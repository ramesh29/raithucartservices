package com.qb.exception;

import org.apache.log4j.Logger;

public class SessionTimeOutException  extends SurfBIException {
	
	private static final long serialVersionUID = 147672832568520794L;

	public SessionTimeOutException(ErrorCode errorCode) {
		super(errorCode);
	}

	public SessionTimeOutException(Throwable cause) {
		super(ErrorCode.ERR_DEFAULT, cause);
	}

	public SessionTimeOutException(ErrorCode errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	public SessionTimeOutException(ErrorCode errorCode, Throwable cause, Logger log) {
		super(errorCode, cause, log);
	}

}
