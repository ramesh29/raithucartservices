package com.qb.exception;

import org.apache.log4j.Logger;

public class SurfBIException extends Exception {

	private static final long serialVersionUID = 754791306519923474L;
	private ErrorCode errorCode = null;

	public SurfBIException() {
		super();
	}

	public SurfBIException(ErrorCode errorCode) {
		this(errorCode, null, null);
	}

	public SurfBIException(ErrorCode errorCode, Throwable cause) {
		this(errorCode, cause, null);
	}

	public SurfBIException(ErrorCode errorCode, Throwable cause, Logger log) {
		super(cause);
		this.setErrorCode(errorCode);
		if (log != null) {
			log.error(errorCode.getNumericCode() + " - " + errorCode.getDefaultMessage(), cause);
		}
	}

	/**
	 * @param message
	 */
	public SurfBIException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SurfBIException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public SurfBIException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param exc
	 * @param log
	 * @param isTranslationRequired
	 * @param exceptionLevel
	 * @param moduleName
	 */
	public SurfBIException(Throwable exc, Logger log,
			boolean isTranslationRequired, String exceptionLevel,
			String moduleName) {
		super(exc);
		log.error("Error in " + moduleName + " and Exception Level is " + exceptionLevel, exc);

	}

	/**
	 * @param exc
	 * @param log
	 * @param errorMessage
	 */
	public SurfBIException(Throwable exc, Logger log, String errorMessage) {
		super(exc);
		log.error(errorMessage + exc.getMessage());

	}

	public static void showTrace(Throwable e) {
		e.printStackTrace();
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

}
