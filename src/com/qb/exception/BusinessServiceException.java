package com.qb.exception;

import org.apache.log4j.Logger;

public class BusinessServiceException extends SurfBIException {

	private static final long serialVersionUID = -7435921952883884290L;

	public BusinessServiceException(ErrorCode errorCode) {
		super(errorCode);
	}

	public BusinessServiceException(Throwable cause) {
		super(ErrorCode.ERR_DEFAULT, cause);
	}

	public BusinessServiceException(ErrorCode errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	public BusinessServiceException(ErrorCode errorCode, Throwable cause,
			Logger log) {
		super(errorCode, cause, log);
	}
}
