package com.qb.exception;

import org.apache.log4j.Logger;

public class DataAccessException extends SurfBIException {

	private static final long serialVersionUID = 147672832568520794L;

	public DataAccessException(ErrorCode errorCode) {
		super(errorCode);
	}

	public DataAccessException(Throwable cause) {
		super(ErrorCode.ERR_DEFAULT, cause);
	}

	public DataAccessException(ErrorCode errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	public DataAccessException(ErrorCode errorCode, Throwable cause, Logger log) {
		super(errorCode, cause, log);
	}

}
