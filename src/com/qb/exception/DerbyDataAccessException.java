package com.qb.exception;

import org.apache.log4j.Logger;

public class DerbyDataAccessException extends SurfBIException {
	
	private static final long serialVersionUID = 8516448785431728770L;
	
	public DerbyDataAccessException(ErrorCode errorCode) {
		super(errorCode);
	}

	public DerbyDataAccessException(Throwable cause) {
		super(ErrorCode.ERR_DEFAULT, cause);
	}

	public DerbyDataAccessException(ErrorCode errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	public DerbyDataAccessException(ErrorCode errorCode, Throwable cause, Logger log) {
		super(errorCode, cause, log);
	}

}
