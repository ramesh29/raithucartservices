package com.qb.exception;

import org.apache.log4j.Logger;

public class WebCatParseException extends SurfBIException {

	private static final long serialVersionUID = 3215768180446455151L;

	public WebCatParseException(ErrorCode errorCode) {
		super(errorCode);
	}

	public WebCatParseException(Throwable cause) {
		super(ErrorCode.ERR_DEFAULT, cause);
	}

	public WebCatParseException(ErrorCode errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	public WebCatParseException(ErrorCode errorCode, Throwable cause, Logger log) {
		super(errorCode, cause, log);
	}
}
