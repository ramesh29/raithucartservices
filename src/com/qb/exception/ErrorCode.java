package com.qb.exception;

import java.io.Serializable;

import com.qb.util.IExceptionConstants;



public class ErrorCode implements Serializable {

	private static final long serialVersionUID = -688431835623492526L;
	private Integer numericCode;
	private String defaultMessage = null;

	protected ErrorCode(int numericCode) {
		this.setNumericCode(Integer.valueOf(numericCode));
	}

	protected ErrorCode(int numericCode, String defaultMessage) {
		this.setNumericCode(Integer.valueOf(numericCode));
		this.setDefaultMessage(defaultMessage);
	}

	// 100 - 499 are for Generic error codes
	// 1000 - 1999 are for Data Access Layer
	// 2000 - 2999 are for Business Service Layer
	//9001-Session Time Out Exception
	public static final ErrorCode ERR_DEFAULT = new ErrorCode(0);

	// 1000 - 1999 are for Data Access Layer
	public static final ErrorCode ERR_GET_DATASOURCE = new ErrorCode(1000, IExceptionConstants.ERR_GET_DATASOURCE_MSG);
	public static final ErrorCode ERR_CLOSE_CONNECTION = new ErrorCode(1001, IExceptionConstants.ERR_ClOSE_CONNECTION_MSG);
	public static final ErrorCode ERR_GET_CONNECTION = new ErrorCode(1002, IExceptionConstants.ERR_GET_CONNECTION_MSG);
	public static final ErrorCode ERR_EXECUTE_QUERY = new ErrorCode(1003, IExceptionConstants.ERR_EXECUTE_QUERY_MSG);
	public static final ErrorCode ERR_NON_SQL_DATASOURCE_QUERY = new ErrorCode(1004, IExceptionConstants.ERR_NON_SQL_DATASOURCE_MSG);
	public static final ErrorCode ERR_BOX_ACCOUNT_INFO = new ErrorCode(1005, IExceptionConstants.ERR_BOX_ACCOUNT_INFO);
	public static final ErrorCode ERR_BOX_COLLABORATION = new ErrorCode(1005, IExceptionConstants.ERR_BOX_COLLABORATION);
	public static final ErrorCode ERR_SERVER_SESSION_TIMEOUT = new ErrorCode(9001, IExceptionConstants.ERR_SERVER_SESSION_TIMEOUT);

	// 2000 - 2999 are for Business Service Layer
	public static final ErrorCode ERR_UNABLE_TO_PROCESS_WEBSERVICE = new ErrorCode( 1000, IExceptionConstants.ERR_UNABLE_TO_PROCESS_WEBSERVICE_MSG);

	// 3000 - 3999 are for webcat and metadata
	public static final ErrorCode ERR_UNABLE_TO_PARSE_WEBCAT = new ErrorCode( 3000, IExceptionConstants.ERR_UNABLE_TO_PARSE_WEBCAT_MSG);

	// 9000 - 10000 are for Derby Exceptions
	public static final ErrorCode ERR_LOAD_DERBY_CLASS_DRIVER = new ErrorCode(9000, IExceptionConstants.ERR_UNABLE_TO_LOAD_DERBY_CLASS);
		
		
	public void setNumericCode(Integer numericCode) {
		this.numericCode = numericCode;
	}

	public Integer getNumericCode() {
		return numericCode;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public String toString() {
		return numericCode.toString();
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof ErrorCode))
			return false;

		return this.numericCode.equals(((ErrorCode) obj).numericCode);
	}

	public int hashCode() {
		return numericCode.hashCode();
	}

	public int toNumericCode() {
		return this.numericCode.intValue();
	}
}
