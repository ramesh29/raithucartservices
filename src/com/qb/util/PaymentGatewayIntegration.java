package com.qb.util;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qb.dto.AtomPaymentDTO;
import com.qb.dto.ResponseDTO;

public class PaymentGatewayIntegration
{
	
	public AtomPaymentDTO firstPaymentRequest(AtomPaymentDTO atom)
	{
		String requestParams="";
		
		ResponseDTO<String> response = new ResponseDTO<String>();
		
		requestParams = "login=" +atom.getLogin()+
				"&pass="+atom.getPassword()+
				"&ttype="+atom.getTtype()+
				"&prodid="+atom.getProdId()+
				"&amt="+atom.getAmount()+
				//"&amt=3000.0"+
				"&txncurr="+atom.getCurrency()+
				"&txnscamt=" +atom.getTransactionServiceCharge()+
				"&clientcode="+atom.getClientCode()+
				"&txnid="+atom.getTransactionId()+
				"&date="+atom.getDate()+
				"&custacc="+atom.getCustAcc()+
				"&udf1="+atom.getUdf1()+
				"&udf2="+atom.getUdf2()+
				"&udf3="+atom.getUdf3()+
				"&udf4="+atom.getUdf4()+
				"&ru="+atom.getReturnUrl()+
				"&mprod="+atom.getContentXML()+
				"&signature="+SignatureGenerate.getEncodedValueWithSha2(atom.getRequestHashKey(), atom.getLogin()+"",atom.getPassword(),atom.getTtype(),atom.getProdId(),atom.getTransactionId()+"",atom.getAmount(),atom.getCurrency());
		atom.setRequestParameter(requestParams);
		atom.setPaymentGatewayURL(IPaymentGatewayURLS.paymentGatewayURL);
		atom.setTransactionStatus("SUCCESS");
		response = httpRequestProcessor(IPaymentGatewayURLS.paymentGatewayURL, requestParams);
		if(response!=null && response.getResponseCode() == 200)
		{
			return parseFirstPayment(atom,response.getResponseMessage());
		}
		else
		{
			atom.setTransactionStatus("FAILED");
			atom.setTransactionMsg(response.getResponseMessage());
		}
		
		return atom;
	}
	
	
	private AtomPaymentDTO parseFirstPayment(AtomPaymentDTO atom, String response)
	{
		
		try{
			
			
			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

			Document doc = dBuilder.parse( new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8)));
			
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			
			if (doc.hasChildNodes()) {
			
				printNote(atom, doc.getChildNodes());
				
			} 
	  }catch (Exception e) {
			e.printStackTrace();
	    }
		
		return atom;
	}
	
	
	private static AtomPaymentDTO printNote(AtomPaymentDTO atom, NodeList nodeList) {

	    for (int count = 0; count < nodeList.getLength(); count++) {

		Node tempNode = nodeList.item(count);

		// make sure it's element node.
		if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

			// get node name and value
			System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
			System.out.println("Node Value =" + tempNode.getTextContent());

			if("param".equalsIgnoreCase(tempNode.getNodeName()))
			{
				if (tempNode.hasAttributes()) {

					// get attributes names and values
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i = 0; i < nodeMap.getLength(); i++) {

						Node node = nodeMap.item(i);
						
						if("tempTxnId".equalsIgnoreCase(node.getNodeValue()))
						{
							atom.setTempTxnId(tempNode.getTextContent());
						}else if("token".equalsIgnoreCase(node.getNodeValue()))
						{
							atom.setToken(tempNode.getTextContent());
						}else if("txnStage".equalsIgnoreCase(node.getNodeValue()))
						{
							atom.setTxnStage(Integer.parseInt(tempNode.getTextContent()));
						}
						
						System.out.println("attr name : " + node.getNodeName());
						System.out.println("attr value : " + node.getNodeValue());

					}

				}	
			}
			
			if("VerifyOutput".equalsIgnoreCase(tempNode.getNodeName()))
			{
				if (tempNode.hasAttributes()) {

					// get attributes names and values
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i = 0; i < nodeMap.getLength(); i++) {

						Node node = nodeMap.item(i);
						
						if("atomtxnId".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setMmp_txn(node.getNodeValue());
						}else if("MerchantTxnID".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setMer_txn(node.getNodeValue());
						}else if("surcharge".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setSurcharge(node.getNodeValue());
						}else if("TxnDate".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setRespTxndate(node.getNodeValue());
						}else if("BID".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setBankTransactionId(node.getNodeValue());
						}else if("VERIFIED".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setTransactionStatus(node.getNodeValue());
						}else if("bankname".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setBankName(node.getNodeValue());
						}else if("discriminator".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setDiscriminator(node.getNodeValue());
						}else if("CardNumber".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setCardNumber(node.getNodeValue());
						}else if("desc".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setTransactionStatus(node.getNodeValue());
						}else if("MerchantTxnID".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setTransactionId(Integer.parseInt(node.getNodeValue()));
						}else if("AMT".equalsIgnoreCase(node.getNodeName()))
						{
							atom.setRespAmount(node.getNodeValue());
						}
						
						System.out.println("attr name : " + node.getNodeName());
						System.out.println("attr value : " + node.getNodeValue());

					}

				}	
			}
			
			

			if (tempNode.hasChildNodes()) {

				// loop again if has child nodes
				printNote(atom, tempNode.getChildNodes());

			}

			System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");

		}

	    }
	    
	    return atom;

	  }
	
	public AtomPaymentDTO trackingPaymentRequest(AtomPaymentDTO atom)
	{
		String requestParams="";
		
		ResponseDTO<String> response = new ResponseDTO<String>();
		
		requestParams = "merchantid="+atom.getLogin()+"&merchanttxnid="+atom.getTransactionId()+"&amt="+atom.getAmount()+"&tdate="+atom.getTrackingDate();
		//requestParams = "merchantid="+atom.getLogin()+"&merchanttxnid="+atom.getTransactionId()+"&amt=3000.0&tdate="+atom.getTrackingDate();
		
		response = httpRequestProcessor(IPaymentGatewayURLS.paymentTrackingGatewayURL, requestParams);
		if(response!=null && response.getResponseCode() == 200)
		{
			return parseFirstPayment(atom,response.getResponseMessage());
		}
		else
		{
			atom.setTransactionStatus("FAILED");
			atom.setTransactionMsg(response.getResponseMessage());
		}
		
		return atom;
	}
	
	
	public static ResponseDTO<String> httpRequestProcessor(String urlString, String requestParams)
	{
		ResponseDTO<String> response = new ResponseDTO<String>();
		String sResponse = "";

        try{
                     
			Object obj;
            final javax.net.ssl.TrustManager[] trustAll = new javax.net.ssl.TrustManager[]{new javax.net.ssl.X509TrustManager(){
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return null;
                        } 
        
                        public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                              throws java.security.cert.CertificateException {
                        }
        
                        public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                                                                          throws java.security.cert.CertificateException {
                        }
        
            }};

        
            final javax.net.ssl.SSLContext sslcontext = javax.net.ssl.SSLContext.getInstance("TLS");
            sslcontext.init(null, trustAll, new java.security.SecureRandom());

			final javax.net.ssl.SSLSocketFactory scoFactory = sslcontext.getSocketFactory();

            javax.net.ssl.HostnameVerifier allverify = new javax.net.ssl.HostnameVerifier() {
        
				public boolean verify(String arg0, javax.net.ssl.SSLSession arg1) {
                                  return true;
                }
            };

            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(allverify);
            System.setProperty("https.protocols","TLSv1");
            java.net.URL url = new java.net.URL(urlString);
                          
            obj = (javax.net.ssl.HttpsURLConnection)url.openConnection(); 
            ((javax.net.ssl.HttpsURLConnection)obj).setSSLSocketFactory(scoFactory);
            ((javax.net.ssl.HttpsURLConnection)obj).setRequestMethod("POST");
            ((javax.net.ssl.HttpsURLConnection)obj).setDoInput(true);
            ((javax.net.ssl.HttpsURLConnection)obj).setDoOutput(true);
            ((javax.net.ssl.HttpsURLConnection)obj).setUseCaches(false);
            ((javax.net.ssl.HttpsURLConnection)obj).setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                          
             java.io.DataOutputStream dataoutputstream = new java.io.DataOutputStream(((javax.net.ssl.HttpsURLConnection)obj).getOutputStream()); 
             dataoutputstream.writeBytes(requestParams);
             dataoutputstream.flush(); 
             dataoutputstream.close();

                          
            int responseCode = ((javax.net.ssl.HttpsURLConnection)obj).getResponseCode();
            String respMssg = ((javax.net.ssl.HttpsURLConnection)obj).getResponseMessage();
                          
            System.out.println("Response Code :: "+ responseCode);
                          
            if(responseCode == 200){
                java.io.BufferedReader bf = new java.io.BufferedReader(new java.io.InputStreamReader(((java.net.URLConnection)obj).getInputStream()));
                for(String line; (line = bf.readLine()) != null;){
                    sResponse += line ;
                }
                response.setResponseCode(200);
                response.setResponseMessage(sResponse);
                
				 System.out.println("Response xml :: " + sResponse);
			}
            else
            {
            	response.setResponseCode(responseCode);
                response.setResponseMessage(respMssg);
                System.out.println("Error code received " + responseCode + " with message :: " + respMssg );
            }
              
           
            return response;
            
	}catch(Exception e)
        {
			response.setResponseCode(400);
        	response.setResponseMessage(e.getMessage());
        
        }
        
		return response;
	}
        
	
	

	public static void main(String[] args) {
		
		AtomPaymentDTO atom =  new AtomPaymentDTO();
		
		PaymentGatewayIntegration pgi = new PaymentGatewayIntegration();
		
		pgi.parseFirstPayment(atom, "<VerifyOutput MerchantID=\"2\" MerchantTxnID=\"207\" AMT=\"3000.0\" VERIFIED=\"SUCCESS\" BID=\"193214121\" bankname=\"Atom Bank\" atomtxnId=\"19321412\" discriminator=\"NB\" surcharge=\"0.00\" CardNumber=\"\" TxnDate=\"2017-05-20 13:52:39\" UDF9=\"null\"/>");
		
		
		//String urlString = "https://payment.atomtech.in/paynetz/epi/fts?"; // url string
		// parameter details
		//String requestParam = "login=18724&pass=Kskv@123&ttype=NBFundTransfer&prodid=KSKVKU&amt=100.00&txncurr=INR&txnscamt=0&clientcode=RDE2QlcwMDAwMDAy&txnid=1011464110604503&date=24/05/2016%2005:23:24&custacc=1011464110604503&ru=https://ups.mkcl.org/du/ups/gatewayAtom/gatewayResponse";
		
		// contains the xml response
/*		String sResponse = "";

        try{
                     
			Object obj;

            final javax.net.ssl.TrustManager[] trustAll = new javax.net.ssl.TrustManager[]{new javax.net.ssl.X509TrustManager(){
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return null;
                        } 
        
                        public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                              throws java.security.cert.CertificateException {
                        }
        
                        public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                                                                          throws java.security.cert.CertificateException {
                        }
        
            }};

        
            final javax.net.ssl.SSLContext sslcontext = javax.net.ssl.SSLContext.getInstance("TLS");
            sslcontext.init(null, trustAll, new java.security.SecureRandom());

			final javax.net.ssl.SSLSocketFactory scoFactory = sslcontext.getSocketFactory();

            javax.net.ssl.HostnameVerifier allverify = new javax.net.ssl.HostnameVerifier() {
        
				public boolean verify(String arg0, javax.net.ssl.SSLSession arg1) {
                                  return true;
                }
            };

            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(allverify);
            System.setProperty("https.protocols","TLSv1");
            java.net.URL url = new java.net.URL(urlString);
                          
            obj = (javax.net.ssl.HttpsURLConnection)url.openConnection(); 
            ((javax.net.ssl.HttpsURLConnection)obj).setSSLSocketFactory(scoFactory);
            ((javax.net.ssl.HttpsURLConnection)obj).setRequestMethod("POST");
            ((javax.net.ssl.HttpsURLConnection)obj).setDoInput(true);
            ((javax.net.ssl.HttpsURLConnection)obj).setDoOutput(true);
            ((javax.net.ssl.HttpsURLConnection)obj).setUseCaches(false);
            ((javax.net.ssl.HttpsURLConnection)obj).setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                          
             java.io.DataOutputStream dataoutputstream = new java.io.DataOutputStream(((javax.net.ssl.HttpsURLConnection)obj).getOutputStream()); 
             dataoutputstream.writeBytes(requestParam);
             dataoutputstream.flush(); 
             dataoutputstream.close();

                          
            int responseCode = ((javax.net.ssl.HttpsURLConnection)obj).getResponseCode();
            String respMssg = ((javax.net.ssl.HttpsURLConnection)obj).getResponseMessage();
                          
            System.out.println("Response Code :: "+ responseCode);
                          
            if(responseCode == 200){
                java.io.BufferedReader bf = new java.io.BufferedReader(new java.io.InputStreamReader(((java.net.URLConnection)obj).getInputStream()));
                for(String line; (line = bf.readLine()) != null;){
                    sResponse += line ;
                }
				 System.out.println("Response xml :: " + sResponse);
			}
            else
              System.out.println("Error code received " + responseCode + " with message :: " + respMssg );
                                                                          
                          
		}
        catch(Exception e)
        {
            e.printStackTrace();
        }*/

	}
	
}
