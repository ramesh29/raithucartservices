package com.qb.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Base64;
import java.util.EnumMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;

import org.apache.commons.lang.RandomStringUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QuickBookUtil {
	 private static final String ALGORITHM = "AES";
	    private static final byte[] keyValue = 
	        new byte[] { 'T', 'h', 'i', 's', 'I', 's', 'A', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y' };

	     public static String encrypt(String valueToEnc) throws Exception {
	        Key key = generateKey();
	        Cipher c = Cipher.getInstance(ALGORITHM);
	        c.init(Cipher.ENCRYPT_MODE, key);
	        byte[] encValue = c.doFinal(valueToEnc.getBytes());
	        String encryptedValue = QuickBookBase64.encode(encValue);
	        return encryptedValue;
	    }

	    public static String decrypt(String encryptedValue) throws Exception {
	        Key key = generateKey();
	        Cipher c = Cipher.getInstance(ALGORITHM);
	        c.init(Cipher.DECRYPT_MODE, key);
	        byte[] decordedValue = QuickBookBase64.decode(encryptedValue);
	        byte[] decValue = c.doFinal(decordedValue);
	        String decryptedValue = new String(decValue);
	        return decryptedValue;
	    }

	    private static Key generateKey() throws Exception {
	        Key key = new SecretKeySpec(keyValue, ALGORITHM);
	        // SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
	        // key = keyFactory.generateSecret(new DESKeySpec(keyValue));
	        return key;
	    }
	    public static void main(String[] args) throws Exception {
	    	
	    	//	Integer.parseInt("9988899990");
//	    		System.out.println(" DeCRYPT : " + QuickBookUtil.decrypt("3vu0k/h63Hrvm+BoNR+MJQ==")) ;
	    //		System.out.println(" Oracle ENCRYPT : " + QuickBookUtil.encrypt("Tractile@2017")) ;
	    		System.out.println(" Oracle DeCRYPT : " + QuickBookUtil.decrypt("4ODHV5F74Am98qkRsmeqmQ==")) ;
	    }
	    
	 	
		public static boolean stringCheck(String input) {
			Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
			Matcher matcher = pattern.matcher(input);
			boolean isChecked = matcher.find();
			return isChecked;
		}
		public static boolean convertToBoolean(String str) {

			if ("Y".equalsIgnoreCase(str)) {
				return true;
			} else {
				return false;
			}
		}
		
		/**
		 * 
		 * @param str
		 * @return
		 */
		public static boolean isStringNullOrEmpty(String str) {
			if(str == null) {
				return true;
			} else if(str.trim().equals("")){
				return true;
			} else {
				return false;
			}
		}
		
		public static boolean isAValidNumber(String number) 
		{
			if(number != null && number.trim().length() > 0)
			{
				try {
					Integer.parseInt(number);
					return true;
				}catch(NumberFormatException e) {
					return false;
				}
			}
			return false;
		}
		
		public static void copyfile(String srFile, String dtFile)
		{
			try{
				File f1 = new File(srFile);
				File f2 = new File(dtFile);
				InputStream in = new FileInputStream(f1);
				OutputStream out = new FileOutputStream(f2);
				
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0){
					out.write(buf, 0, len);
				}
				in.close();
				out.close();

			} catch(FileNotFoundException ex){
				System.out.println(ex.getMessage() + " in the specified directory.");  	 
			} catch(IOException e){
				System.out.println("IOException Occured while copying the file from work to Main Area");
			}
		}
		
		public static <T, E> T getKeyByValue(Map<T, E> map, E value)
		{
			if(map==null)
			{
				return null;
			}
			for (Entry<T, E> entry : map.entrySet())
			{
				if (value.equals(entry.getValue()))
				{
					return entry.getKey();
				}
			}
			return null;
		}
		
		
		
		
		
		public static String getBookingNumber()
		{
			
			return RandomStringUtils.randomAlphanumeric(8).toUpperCase();
		}
		
		
		public static String generateBase64QR(String bookingNumber)
		{
			System.out.println(bookingNumber);
			String myCodeText = bookingNumber;
			//String filePath = "D:/Office/CrunchifyQR.png";
			int size = 150;
			String fileType = "png";
			//File myFile = new File(filePath);
			try {
				
				Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
				hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
				
				// Now with zxing version 3.2.1 you could change border size (white border size to just 1)
				hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
				hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
	 
				QRCodeWriter qrCodeWriter = new QRCodeWriter();
				BitMatrix byteMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size,
						size, hintMap);
				int CrunchifyWidth = byteMatrix.getWidth();
				BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth,
						BufferedImage.TYPE_INT_RGB);
				image.createGraphics();
	 
				Graphics2D graphics = (Graphics2D) image.getGraphics();
				graphics.setColor(Color.WHITE);
				graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
				graphics.setColor(Color.BLACK);
	 
				for (int i = 0; i < CrunchifyWidth; i++) {
					for (int j = 0; j < CrunchifyWidth; j++) {
						if (byteMatrix.get(i, j)) {
							graphics.fillRect(i, j, 1, 1);
						}
					}
				}
				
				 final ByteArrayOutputStream os = new ByteArrayOutputStream();
				 ImageIO.write(image, fileType, Base64.getEncoder().wrap(os));
				 System.out.println( os.toString(StandardCharsets.ISO_8859_1.name()));
				 return os.toString(StandardCharsets.ISO_8859_1.name());
			} catch (WriterException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("\n\nYou have successfully created QR Code.");
			
				return null;
		}
}
