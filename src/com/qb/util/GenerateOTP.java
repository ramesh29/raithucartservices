package com.qb.util;

import java.util.Random;

public class GenerateOTP {
	
	public static void main(String[] args) 
	  {  
	    System.out.println(generatorOTP(4)); 
	  } 
	
	public static String generatorOTP(int length) 
	  { 
	    System.out.print("Your OTP is : "); 
	              //Creating object of Random class
	    Random obj = new Random(); 
	    char[] otp = new char[length]; 
	    for (int i=0; i<length; i++) 
	    { 
	      otp[i]= (char)(obj.nextInt(10)+48); 
	    } 
	      String str = String.valueOf(otp);

	    return str; 
	  } 

}
