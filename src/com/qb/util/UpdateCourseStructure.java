package com.qb.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.qb.dao.ABaseDAO;
import com.qb.exception.DataAccessException;
import com.qb.exception.ErrorCode;
import com.tt.util.IQueryConstants;

public class UpdateCourseStructure  extends ABaseDAO{

	public static Map<String, Integer> columnIndexMap = new HashMap<String, Integer>();
	
	
	public static void loadColumnIndexes()
    {
		columnIndexMap.put("CourseType",0);
        columnIndexMap.put("Code",1);
        columnIndexMap.put("CourseName",2);
    }
	
	@SuppressWarnings("resource")
	public static void updateCourse() throws DataAccessException
	{

		
		Connection con = null;
		try {

            File inputWorkbook = new File("C:\\Users\\skrishna\\Desktop\\courselistUPDATED.xls");
            Workbook w;
            w = new HSSFWorkbook(new FileInputStream(inputWorkbook));
            FormulaEvaluator evaluator = w.getCreationHelper().createFormulaEvaluator();
            Map<String,List<String>> courses = new HashMap<>();
            Sheet cursheet1=w.getSheet("Sheet1");
            con = getDBConnection();
            for(int j=1;j<cursheet1.getPhysicalNumberOfRows();j++) 
            {
            	Row row1 = cursheet1.getRow(j);
                Row.MissingCellPolicy mcp = cursheet1.getWorkbook().getMissingCellPolicy();
            	if(getCellAsString(row1, columnIndexMap.get("CourseName"), mcp, evaluator) !=null && getCellAsString(row1, columnIndexMap.get("CourseName"), mcp, evaluator).trim().length() > 0)
            	{
            		String courseName = getCellAsString(row1, columnIndexMap.get("CourseName"), mcp, evaluator);
            		String courseType = getCellAsString(row1, columnIndexMap.get("CourseType"), mcp, evaluator);
            		String courseCode = getCellAsString(row1, columnIndexMap.get("Code"), mcp, evaluator);
            		
            		List<String> courseList = null;
            		if(courses.containsKey(courseType))
            		{
            			courseList = courses.get(courseType);
            			courseList.add(courseName+":':"+courseCode);
            		}else
            		{
            			courseList = new ArrayList<>();
            			courseList.add(courseName+":':"+courseCode);
            		}
            		
            		courses.put(courseType,courseList);
                }
	      	}
            createCourses(con,courses);
            
        }catch (IOException e) {
            System.out.println("Cannot read file");
            e.printStackTrace();
        } catch (Exception e) {
            cleanUp(null, null, con);
        	System.out.println("Exception occurred while running");
            e.printStackTrace();
        }finally {
			System.out.println("Completed ..........");
		}

	
	}
	
	
	public static void createCourses(Connection con, Map<String,List<String>>  courses) throws DataAccessException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "INSERT INTO e_courses (course_sub_category_id, org_id, course_name, course_description, created_by, creation_on, updated_by, updation_on,native_code) VALUES ((select course_sub_category_id from e_course_sub_category where course_sub_cateogry_name=?), 1, ?, ?, -1, now(), -1, now(),?)";
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			for(Map.Entry<String,List<String>> entry : courses.entrySet())
			{
				List<String> courseList = entry.getValue();
				for(String course:courseList)
				{
					pstmt.setString(1, entry.getKey().trim());
					pstmt.setString(2, course.split(":':")[0].trim());
					pstmt.setString(3, course.split(":':")[0].trim());
					pstmt.setString(4, course.split(":':")[1].trim());
					pstmt.addBatch();	
				}
			}
			pstmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch(Exception ex){
			ex.printStackTrace();
		}finally {
			cleanUp(rs, pstmt, null);		
		}
	}
	
	
	public static void main(String[] args) throws DataAccessException {
		loadColumnIndexes();
		updateCourse();
		
	}
	
	private static String getCellAsString(Row row,int index,Row.MissingCellPolicy mcp,FormulaEvaluator evaluator){
        return getCellAsString(row, index, mcp, evaluator, false);
    }

    private static String getCellAsString(Row row,int index,Row.MissingCellPolicy mcp,FormulaEvaluator evaluator, Boolean isNullToBeReturned)
    {
        String retValue = null;
        //index cannot be less than zero
        Cell cell = row.getCell(index);
        if(cell!=null)
        {
            String result = row.getCell(index, mcp).toString();
            if(row.getCell(index, mcp).getCellType() == Cell.CELL_TYPE_FORMULA)
            {
                int cellValue = evaluator.evaluateFormulaCell(cell);
                result = cell.getStringCellValue();
                if(cellValue == Cell.CELL_TYPE_STRING)
                {
                    result = cell.getStringCellValue();
                }
                else if(cellValue == Cell.CELL_TYPE_NUMERIC)
                {
                    Double numericValue = cell.getNumericCellValue();
                    if (numericValue % 1 == 0)
                    {
                        result = numericValue.intValue() + "";
                    }
                    else
                    {
                        result = numericValue+"";
                    }
                }
            }
            retValue =  result;
        }else{
            retValue = null;
        }
        if(!isNullToBeReturned && retValue == null)
            return "";
        else
            return retValue.trim();


    }
    
    private static Connection getDBConnection() {

		Connection dbConnection = null;

		try {

			Class.forName(IQueryConstants.DB_DRIVER);

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(IQueryConstants.URL,IQueryConstants.USERNAME,IQueryConstants.PASSWORD);
			return dbConnection;

		} catch (SQLException e) {

			System.out.println("Exception Occured in getDBConnection method "+e.getMessage());
		}
		return dbConnection;
	}
    
    public static void cleanUp(ResultSet rs, Statement stmt, Connection con) throws DataAccessException {
		try {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			throw new DataAccessException(ErrorCode.ERR_CLOSE_CONNECTION, e, null);
		}
	}
	

}
