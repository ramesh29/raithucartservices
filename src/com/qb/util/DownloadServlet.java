package com.qb.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.IBookingDAO;
import com.qb.dto.HTMlAttributesDTO;
import com.qb.dto.OrgBookingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.exception.DataAccessException;

/**
 * Servlet implementation class DownloadServlet
 */
@WebServlet("/DownloadServlet")
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	IBookingDAO bookingDAO = DataAccessFactory.instance().getBookingDAO();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String bookingId= request.getParameter("bookingId");
		String userId = request.getParameter("userId");
		SearchParametersDTO search = new SearchParametersDTO();
		search.setBookingId(Integer.parseInt(bookingId));
		search.setUserId(Integer.parseInt(userId));
		ResponseDTO<OrgBookingDTO> r = new ResponseDTO<OrgBookingDTO>();
		
		
		try {
			bookingDAO.getUserBookingInfo(search, r);
			List<OrgBookingDTO> bookings = r.getData();
			if(bookings != null && !bookings.isEmpty())
			{
				OrgBookingDTO bookingInfo = bookings.get(0);
				String bookedNumber = bookingInfo.getBookingNumber();
				String qrBase64QRText = QuickBookUtil.generateBase64QR(bookedNumber);
				String filePath = getServletContext().getRealPath("EmailTemplates/Reciept.html");
				
				
				HTMlAttributesDTO html = new HTMlAttributesDTO();
				html.setHtmlFileName(filePath);
				HashMap<String, String> replaceValues = new HashMap<>();
				replaceValues.put(":replaceText", qrBase64QRText);
				replaceValues.put(":courseName", bookingInfo.getCourseName());
				replaceValues.put(":student", bookingInfo.getFullName());
				replaceValues.put(":orgName", bookingInfo.getOrgName());
				replaceValues.put(":startTime", bookingInfo.getTimings());
				replaceValues.put(":coursePrice", bookingInfo.getAmountPaid());
				replaceValues.put(":startDate", bookingInfo.getStartDate() +" to "+bookingInfo.getEndDate());
				replaceValues.put(":bookingNumber", bookedNumber);
				Double totalPrice = Double.parseDouble(bookingInfo.getAmountPaid());
				DecimalFormat df = new DecimalFormat("#.##");     
				/*Double serviceTax = (IConstants.serviceTax/100)*totalPrice;
				serviceTax = Double.valueOf(df.format(serviceTax));*/
				Double convienceCharge = (IPaymentGatewayURLS.convienceCost/100)*totalPrice;
				convienceCharge = Double.valueOf(df.format(convienceCharge));
				/*Double paymentGateway = (IConstants.paymentGateway/100)*totalPrice;
				paymentGateway = Double.valueOf(df.format(paymentGateway));*/
				//totalPrice = totalPrice+serviceTax+convienceCharge+paymentGateway;
				totalPrice = totalPrice+convienceCharge;
				totalPrice = Double.valueOf(df.format(totalPrice));
				replaceValues.put(":internetHandlingCharges", convienceCharge+"");
				//replaceValues.put(":totalPaymentGatewayCharges", paymentGateway+"");
				replaceValues.put(":totalCost", totalPrice+"");
				replaceValues.put(":bookingDate", bookingInfo.getPaymentDateTime()+"");
				
				html.setReplaceValues(replaceValues);
				response.setContentType("text/html");
				response.setHeader("Content-Disposition","attachment;filename=Receipt.html");
				InputStream is = new ByteArrayInputStream(HTMLGenerator.getModifiedHTML(html).getBytes(StandardCharsets.UTF_8));

				int read=0;
				byte[] bytes = new byte[1024];
				OutputStream os = response.getOutputStream();

				while((read = is.read(bytes))!= -1){
					os.write(bytes, 0, read);
				}
				os.flush();
				os.close();
			   }
			
			
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
