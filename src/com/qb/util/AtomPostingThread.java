package com.qb.util;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.IBookingDAO;
import com.qb.dto.AtomPaymentDTO;
import com.qb.dto.TransactionDTO;
import com.qb.exception.DataAccessException;


public class AtomPostingThread extends TimerTask  
{
	IBookingDAO bookingDAO = DataAccessFactory.instance().getBookingDAO();
	AtomPaymentDTO atom;
	 
	public AtomPostingThread(AtomPaymentDTO atom)
	{
		super();
		this.atom = atom;
		
	}

	@Override
	public void run()
	{		
		System.out.println("Run Method Started ");
		
		try{
			//Thread.sleep(60000);
			new Thread(new Runnable() {
                @Override
                public void run() {

                	PaymentGatewayIntegration pgi = new PaymentGatewayIntegration();
        			AtomPaymentDTO finalAtomResponse = pgi.trackingPaymentRequest(atom);
        			try {
						bookingDAO.updateAtomTransaction(finalAtomResponse);
						if(!"SUCCESS".equalsIgnoreCase(finalAtomResponse.getTransactionStatus()))
						{
							List<Integer> scheduleIds = new ArrayList<Integer>();
							try {
								Map<Integer, List<Integer>>  map = bookingDAO.getSchedulesForTransaction(finalAtomResponse.getTransactionId());
								
								for(Map.Entry<Integer, List<Integer>> entry : map.entrySet())
								{
									atom.setUdf2(entry.getKey()+"");
									scheduleIds = entry.getValue();
								}
								
							} catch (DataAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							if(bookingDAO.isTransactionUpdated(finalAtomResponse.getTransactionId()))
							{
								int failBooking = bookingDAO.bookingFailed(scheduleIds);
								if(failBooking > 0)
								{
									TransactionDTO transactionDTO = new TransactionDTO();
									transactionDTO.setUserId(Integer.parseInt(atom.getUdf2()));
									transactionDTO.setTransactionStatus("ERROR");
									transactionDTO.setTransactionId(finalAtomResponse.getTransactionId());
									int updateTransaction = bookingDAO.updateTransaction(transactionDTO);
											
									if(updateTransaction > 0)
									{
										bookingDAO.updateBooking(transactionDTO);
										
									}
								}	
							}
							
						
						}
						
					} catch (DataAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        			
                }
            }).start();
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally
		{
			System.out.println("Run Method Completed");
		}
		
		
	}
}
