package com.qb.util;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.qb.dao.DataAccessFactory;



/**
 * Servlet implementation class InitEnvServlet
 */
public class InitEnvServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitEnvServlet() {
        super();        
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		// setting of the servlet context path
		DataAccessFactory.instance().setContextPath(config.getServletContext().getRealPath("/WEB-INF"));
		
		System.out.println("Server context path : "+ DataAccessFactory.instance().getContextPath());
		
		// call initPropeties method
		DataAccessFactory.instance().initProperties();	
	}
	
}
