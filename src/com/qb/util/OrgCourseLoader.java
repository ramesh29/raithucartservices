package com.qb.util;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.qb.dao.ABaseDAO;
import com.qb.dao.DataAccessFactory;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.OrgCourseDTO;
import com.qb.exception.DataAccessException;
import com.qb.exception.ErrorCode;
import com.tt.util.IQueryConstants;

public class OrgCourseLoader extends ABaseDAO{

	public static Map<String, Integer> columnIndexMap = new HashMap<String, Integer>();
	
	private static final String propsFolderPath = "D:/Office/Personal/MyOrg/BitBucketRepository/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/MyMarineCourseServices/WEB-INF";
	
	public static final int orgId = 7;
	
	public static void loadColumnIndexes()
    {
		columnIndexMap.put("Course Category",0);
        columnIndexMap.put("Course Sub Category",1);
        columnIndexMap.put("Course",2);
        columnIndexMap.put("Campus",3);
        columnIndexMap.put("Duration",4);
        columnIndexMap.put("Fees",5);
        columnIndexMap.put("Repeat",6);
        columnIndexMap.put("Repeat Every",7);
        columnIndexMap.put("Repeat Detail",8);
        columnIndexMap.put("Repeat On",9);
    }
	
	public static void main(String[] args) throws ServletException {
		try {
			//init(propsFolderPath);
			loadColumnIndexes();
			loadData();
		
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void init(String path) throws ServletException {
		
		// setting of the servlet context path
		DataAccessFactory.instance().setContextPath(path);
		
		System.out.println("Server context path : "+ DataAccessFactory.instance().getContextPath());
		
		// call initPropeties method
		DataAccessFactory.instance().initProperties();	
	}

	
	@SuppressWarnings("resource")
	public static void loadData() throws DataAccessException
	{
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

            File inputWorkbook = new File("C:/Users/skrishna/Desktop/courselisttemplatesrsa.xls");
            Workbook w;
            w = new HSSFWorkbook(new FileInputStream(inputWorkbook));
            FormulaEvaluator evaluator = w.getCreationHelper().createFormulaEvaluator();
            Map<String,CourseBookingDTO> courses = new HashMap<>();
            List<OrgCourseDTO> orgCourses = new ArrayList<>();
            Sheet cursheet1=w.getSheet("RS MARINE");
            con = getDBConnection();
            int i =0;
            for(int j=1;j<cursheet1.getPhysicalNumberOfRows();j++) 
            {
            	
            	Row row1 = cursheet1.getRow(j);
                Row.MissingCellPolicy mcp = cursheet1.getWorkbook().getMissingCellPolicy();
            	if(getCellAsString(row1, columnIndexMap.get("Course"), mcp, evaluator) !=null && getCellAsString(row1, columnIndexMap.get("Course"), mcp, evaluator).trim().length() > 0)
            	{
            		
            		System.out.println("Row "+i++);
            		CourseBookingDTO courseDTO = new CourseBookingDTO();
                	OrgCourseDTO orgCourseDTO = new OrgCourseDTO();
                    try {
                    	
                        
                        courseDTO.setCategoryName(getCellAsString(row1, columnIndexMap.get("Course Category"), mcp, evaluator));
                        courseDTO.setSubCatName(getCellAsString(row1, columnIndexMap.get("Course Sub Category"), mcp, evaluator));
                        courseDTO.setCourseName(getCellAsString(row1, columnIndexMap.get("Course"), mcp, evaluator).toUpperCase());
                        courses.put(getCellAsString(row1, columnIndexMap.get("Course"), mcp, evaluator).toUpperCase(),courseDTO);
                        
                        orgCourseDTO.setCourseName(getCellAsString(row1, columnIndexMap.get("Course"), mcp, evaluator).toUpperCase());
                        
                        orgCourseDTO.setDuration((getCellAsString(row1, columnIndexMap.get("Duration"), mcp, evaluator) == "" ? 0 : Integer.parseInt(getCellAsString(row1, columnIndexMap.get("Duration"), mcp, evaluator).substring(0, getCellAsString(row1, columnIndexMap.get("Duration"), mcp, evaluator).length()-2))));
                        orgCourseDTO.setCampus(getCellAsString(row1, columnIndexMap.get("Campus"), mcp, evaluator));
                        orgCourseDTO.setPrice(getCellAsString(row1, columnIndexMap.get("Fees"), mcp, evaluator));
                        orgCourseDTO.setRepeat(getCellAsString(row1, columnIndexMap.get("Repeat"), mcp, evaluator));
                        orgCourseDTO.setRepeatEvery(getCellAsString(row1, columnIndexMap.get("Repeat Every"), mcp, evaluator));
                        orgCourseDTO.setRepeatDetail(getCellAsString(row1, columnIndexMap.get("Repeat Detail"), mcp, evaluator));
                        orgCourseDTO.setRepeatOn(getCellAsString(row1, columnIndexMap.get("Repeat On"), mcp, evaluator));
                        orgCourses.add(orgCourseDTO);
                        

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
	
            	}
            	            
            
            createOrgCourses(con,loadCourses(courses ,con),orgCourses);
            
        }catch (IOException e) {
            System.out.println("Cannot read file");
            e.printStackTrace();
        } catch (Exception e) {
            cleanUp(rs, pstmt, con);
        	System.out.println("Exception occurred while running");
            e.printStackTrace();
        }

	}
	
	
	public static Map<String,CourseBookingDTO> loadCourses(Map<String,CourseBookingDTO> courses , Connection con) throws DataAccessException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			
			List<String> courseNames = new ArrayList<>();
			for(Map.Entry<String,CourseBookingDTO> entry : courses.entrySet())
			{
				courseNames.add(entry.getKey().toUpperCase());
			}
			
			
			String sql = "select * from e_courses where course_name in (?)";
			sql = setList(sql, courseNames, 1);
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			Map<String,CourseBookingDTO> matchedCourses = new HashMap<>();
			CourseBookingDTO courseBookingDTO =  null;
			while(rs.next())
			{
				courseBookingDTO = new CourseBookingDTO();
				courseBookingDTO.setCourseName(rs.getString("course_name").toUpperCase());
				courseBookingDTO.setApiCourseId(rs.getInt("course_id"));
				matchedCourses.put(rs.getString("course_name").toUpperCase(), courseBookingDTO);
			}
			
			Map<String,CourseBookingDTO> unAddedCourses = new HashMap<>();
			
			if(!matchedCourses.isEmpty())
			{
				for(Map.Entry<String, CourseBookingDTO> entry : courses.entrySet())
				{
					if(!matchedCourses.containsKey(entry.getKey().toUpperCase())){
						unAddedCourses.put(entry.getKey().toUpperCase(),entry.getValue());
					}
				}
			}else
			{
				unAddedCourses.putAll(courses);	
			}
			
			courses = new HashMap<>();
			if(!unAddedCourses.isEmpty())
			{
				courses = createUnAvailableCourses(con, unAddedCourses);	
				
			}
			courses.putAll(matchedCourses);
			
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			
		}finally {
			cleanUp(rs, pstmt, null);
		}
		return courses;
	}
		
		
		public static Map<String,CourseBookingDTO> createUnAvailableCourses(Connection con, Map<String,CourseBookingDTO> courses) throws DataAccessException
		{
			PreparedStatement pstmt = null;
			ResultSet rs = null;
					
			try {
				  String sql = "INSERT INTO e_courses(course_id,course_sub_category_id, org_id, course_name, course_description, created_by, creation_on, updated_by, updation_on) VALUES (null,(select course_sub_category_id from  e_course_sub_category where course_sub_cateogry_name=	?	), 1,  	?	, 	?	, -1, now(), -1, now())";
				  pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				  for(Map.Entry<String, CourseBookingDTO> entry : courses.entrySet())
				  {
					  CourseBookingDTO course = entry.getValue();
					  pstmt.setString(1, course.getSubCatName());
					  pstmt.setString(2, entry.getKey());
					  pstmt.setString(3, entry.getKey());
					  pstmt.addBatch();
				 }
				  	int[] count =pstmt.executeBatch();
				  	if(count.length > 0)
					  {
				  		List<String> courseNames = new ArrayList<>();
						for(Map.Entry<String,CourseBookingDTO> entry : courses.entrySet())
						{
							courseNames.add(entry.getKey().toUpperCase());
						}
						
						pstmt.close();
						
						sql = "select * from e_courses where course_name in (?)";
						sql = setList(sql, courseNames, 1);
						pstmt = con.prepareStatement(sql);
						rs = pstmt.executeQuery();
						
						while(rs.next())
						{
							if(courses.containsKey(rs.getString("course_name")))
							{
								CourseBookingDTO bookingDTO = courses.get(rs.getString("course_name").toUpperCase());
								bookingDTO.setApiCourseId(rs.getInt("course_id"));
							}
						}
	
					  }
				  
			return courses;
				  
			} catch (SQLException e) {
				e.printStackTrace();
			} catch(Exception ex){
				ex.printStackTrace();
			}finally {
				
				cleanUp(rs, pstmt, null);		
			}
			return courses;
			
		
		}

		
		
		public static void createOrgCourses(Connection con, Map<String,CourseBookingDTO> courses, List<OrgCourseDTO> orgCourses)
		{
			PreparedStatement pstmt = null;
			ResultSet rs = null;
					
			try {
				  String sql = ""
							+ "INSERT INTO e_org_courses( "
							+ "								org_course_id , "
							+ "								org_id, "
							+ "								course_id, "
							+ "								duration, "
							+ "								payment_agreement_type, "
							+ "								payment_value, "
							+ "								created_by , "
							+ "								creation_on, "
							+ "								updated_by , "
							+ "								updation_on,can_repeat,repeat_every,repeat_detail,repeat_on,campus,fees ) "
							+ "							values "
							+ "							(null,?,?,?,'Percent',8,-1,now(),-1,now(),?,?,?,?,?,?)";

				 
				  pstmt = con.prepareStatement(sql);
				  
				  for(OrgCourseDTO orgCourse : orgCourses)
				  {
					  CourseBookingDTO course = courses.get(orgCourse.getCourseName().toUpperCase());
					  pstmt.setInt(1, orgId);
					  pstmt.setInt(2, course.getApiCourseId());
					  pstmt.setInt(3, orgCourse.getDuration());
					  pstmt.setString(4,orgCourse.getRepeat());
					  pstmt.setString(5,orgCourse.getRepeatEvery());
					  pstmt.setString(6,orgCourse.getRepeatDetail());
					  pstmt.setString(7,orgCourse.getRepeatOn());
					  pstmt.setString(8,orgCourse.getCampus());
					  pstmt.setString(9,orgCourse.getPrice());
					  pstmt.addBatch();
					  
				 }
				 pstmt.executeBatch();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch(Exception ex){
				ex.printStackTrace();
			}finally {
				
				if(pstmt != null)
				{
					try {
						pstmt.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(rs != null )
				{
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			
			}
			
		
		}

	
	
	private static String getCellAsString(Row row,int index,Row.MissingCellPolicy mcp,FormulaEvaluator evaluator){
        return getCellAsString(row, index, mcp, evaluator, false);
    }

    private static String getCellAsString(Row row,int index,Row.MissingCellPolicy mcp,FormulaEvaluator evaluator, Boolean isNullToBeReturned)
    {
        String retValue = null;
        //index cannot be less than zero
        Cell cell = row.getCell(index);
        if(cell!=null)
        {
            String result = row.getCell(index, mcp).toString();
            if(row.getCell(index, mcp).getCellType() == Cell.CELL_TYPE_FORMULA)
            {
                int cellValue = evaluator.evaluateFormulaCell(cell);
                result = cell.getStringCellValue();
                if(cellValue == Cell.CELL_TYPE_STRING)
                {
                    result = cell.getStringCellValue();
                }
                else if(cellValue == Cell.CELL_TYPE_NUMERIC)
                {
                    Double numericValue = cell.getNumericCellValue();
                    if (numericValue % 1 == 0)
                    {
                        result = numericValue.intValue() + "";
                    }
                    else
                    {
                        result = numericValue+"";
                    }
                }
            }
            retValue =  result;
        }else{
            retValue = null;
        }
        if(!isNullToBeReturned && retValue == null)
            return "";
        else
            return retValue.trim();


    }
    
    private static Connection getDBConnection() {

		Connection dbConnection = null;

		try {

			Class.forName(IQueryConstants.DB_DRIVER);

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(IQueryConstants.URL,IQueryConstants.USERNAME,IQueryConstants.PASSWORD);
			return dbConnection;

		} catch (SQLException e) {

			System.out.println("Exception Occured in getDBConnection method "+e.getMessage());
		}
		return dbConnection;
	}
    
    public static void cleanUp(ResultSet rs, Statement stmt, Connection con) throws DataAccessException {
		try {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			throw new DataAccessException(ErrorCode.ERR_CLOSE_CONNECTION, e, null);
		}
	}
    

   
	
}
