package com.qb.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;



public class SMSService {
	
	 public String sendSms(String userName, String password, String sender, String sToPhoneNo,String sMessage)
     {
        try 
        {
         // Construct data
        /* String data = "user=" + URLEncoder.encode("sureshkrishna1227", "UTF-8");
         data += "&password=" + URLEncoder.encode("Suresh@1227", "UTF-8");
         data += "&message=" + URLEncoder.encode(sMessage, "UTF-8");
         data += "&sender=" + URLEncoder.encode("INVITE", "UTF-8");
         data += "&mobile=" + URLEncoder.encode(sToPhoneNo, "UTF-8");
         data += "&type=" + URLEncoder.encode("3", "UTF-8");*/
        	
        	 String data = "user=" + URLEncoder.encode(userName, "UTF-8");
             data += "&password=" + URLEncoder.encode(password, "UTF-8");
             data += "&message=" + URLEncoder.encode(sMessage, "UTF-8");
             data += "&sender=" + URLEncoder.encode(sender, "UTF-8");
             data += "&mobile=" + URLEncoder.encode(sToPhoneNo, "UTF-8");
             data += "&type=" + URLEncoder.encode("3", "UTF-8");
        	
         // Send data
         SSLContext sc = SSLContext.getInstance("TLSv1.2");
         sc.init(null, null, new java.security.SecureRandom());

         URL url = new URL(IPaymentGatewayURLS.smsURL+data);
         HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
         conn.setSSLSocketFactory(sc.getSocketFactory());
         conn.setDoOutput(true);
         OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
         wr.write(data);
         wr.flush();
         // Get the response
         BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
         String line;
         String sResult="";
         while ((line = rd.readLine()) != null) 
         {
               // Process line...
               sResult=sResult+line+" ";
         }
         wr.close();
         rd.close();
         return sResult;
         } 
             catch (Exception e) 
             {
            	 e.printStackTrace();
                System.out.println("Error SMS "+e);
                return "Error "+e;
             }
      }
	 
	 public static void main(String[] args) {
		 SMSService sms = new SMSService();
		 System.out.println(sms.sendSms("tractile","Tractile@2020","TSBOOK","9985632111", "Hello! This is an api integration test message"));
	}

}
