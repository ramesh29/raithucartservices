package com.qb.util;

public interface IExceptionConstants {

	// JDBC Layer Messages

	String ERR_GET_DATASOURCE_MSG = "Exception has occured while getting the datasource from JNDI";
	String ERR_GET_CONNECTION_MSG = "Exception has occured while getting the connection";
	String ERR_ClOSE_CONNECTION_MSG = "Exception has occured while closing the connection";
	String ERR_EXECUTE_QUERY_MSG = "Exception has occured while executing the query";
	String ERR_NON_SQL_DATASOURCE_MSG = "Query can not be verified against the non-sql datasource.";
	String ERR_BOX_ACCOUNT_INFO = "Unable to get Box Account info";
	String ERR_BOX_COLLABORATION = "Unable to share folder in Box";
	String ERR_SERVER_SESSION_TIMEOUT = "Session expired please login";

	// Business Service Layer Messages
	String ERR_UNABLE_TO_PROCESS_WEBSERVICE_MSG = "Webservice is unable to process your request";

	// Derby Layer Messages
	String ERR_UNABLE_TO_LOAD_DERBY_CLASS = "Exception has occured while loading the derby class driver";
		
		
	// Webcat / Metadata parsing layer
	String ERR_UNABLE_TO_PARSE_WEBCAT_MSG = "Unable to parse the webcat";
	String NO = "no";
	String YES = "yes";
	String Y = "Y";
}
