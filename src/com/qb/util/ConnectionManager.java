package com.qb.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.qb.exception.DataAccessException;
import com.qb.exception.ErrorCode;


public class ConnectionManager {

	private static final Logger log = Logger.getLogger(ConnectionManager.class.getName());

	private static ConnectionManager cm = null;
	private Context initContext = null;
	private Context context = null;
	private DataSource ds = null;

	private ConnectionManager() throws DataAccessException {
		try {
			initContext = new InitialContext();
			context = (Context) initContext.lookup("java:comp/env");
			ds = (DataSource) context.lookup("QuickBook_DS");
		} catch (NamingException e) {
			throw new DataAccessException(ErrorCode.ERR_GET_DATASOURCE, e, log);
		}

	}

	public static ConnectionManager instance() throws DataAccessException {
		synchronized (ConnectionManager.class) {
			if (cm == null) {
				cm = new ConnectionManager();
			}
			return cm;
		}
	}

	public Connection getConnection() throws DataAccessException {
		try {
			if (ds != null) {
				return ds.getConnection();
			}
		} catch (SQLException e) {
			throw new DataAccessException(ErrorCode.ERR_GET_CONNECTION, e, log);
		}
		return null;
	}

	public void closeConnection(Connection con) throws DataAccessException {
		try {
			if (con != null && !con.isClosed()) {
				con.close();
			}
		} catch (SQLException e) {
			throw new DataAccessException(ErrorCode.ERR_CLOSE_CONNECTION, e,
					log);
		}
	}
	
	public Connection getConnection(String datasource) throws DataAccessException {
		try {
			DataSource customerDS = null;
			if (datasource != null) {
				 customerDS = (DataSource) context.lookup(datasource);
				return customerDS.getConnection();
			}/*else{
				return ds.getConnection();
			}*/
		} catch (SQLException e) {
			throw new DataAccessException(ErrorCode.ERR_GET_CONNECTION, e, log);
		}catch (NamingException e) {
			throw new DataAccessException(ErrorCode.ERR_GET_DATASOURCE, e, log);
		}
		return null;
	}	
		          
	public void cleanUp(ResultSet rs, Statement stmt, Connection con) throws DataAccessException {
		try {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			throw new DataAccessException(ErrorCode.ERR_CLOSE_CONNECTION, e, log);
		}
	}
}
