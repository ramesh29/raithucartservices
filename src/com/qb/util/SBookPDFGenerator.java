package com.qb.util;

import java.awt.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.vandeseer.easytable.TableDrawer;
import org.vandeseer.easytable.settings.HorizontalAlignment;
import org.vandeseer.easytable.structure.Row;
import org.vandeseer.easytable.structure.Table;
import org.vandeseer.easytable.structure.cell.TextCell;

import com.qb.dto.CCTransactionDTO;
import com.qb.dto.CreditCustomerDTO;
import com.qb.dto.ShopTransactionDTO;
import com.qb.dto.ShopUserDTO;

public class SBookPDFGenerator {

	private static final Color DARK_BLUE = new Color(46, 77, 97);
    private static final Color CUSTOM_GRAY = new Color(136, 136, 136);

    private static final float POINTS_PER_INCH = 72;
    private static final float POINTS_PER_MM = 1 / (10 * 2.54f) * POINTS_PER_INCH;
    
    private static final Float gap = 20.0f;
    private static DecimalFormat df = new DecimalFormat("0.00");

  
    public void drawMultipageTable(ShopUserDTO shopDetails, CreditCustomerDTO customerInfo , CCTransactionDTO transactionDetails, String fileName) throws IOException {

        try (final PDDocument document = new PDDocument()) {
        	
            drawMultipageTableOn(shopDetails, customerInfo, transactionDetails, document);
            document.save(fileName);
        }

    }
    
    private void drawMultipageTableOn(ShopUserDTO shopDetails, CreditCustomerDTO customerInfo , CCTransactionDTO transactionDetails, PDDocument document) throws IOException {
    	TableDrawer tableDrawer = TableDrawer.builder()
                .table(appendShopName(shopDetails.getShopName(),shopDetails.getMobileNumber()))
                //.startX(50)
                .startX(50)
                .startY(800F)
                .endY(50F) // note: if not set, table is drawn over the end of the page
                .build();
    	tableDrawer.draw(() -> document, () -> new PDPage(PDRectangle.A4), 50f);
    	
    	tableDrawer = TableDrawer.builder()
                .table(appendBillDetails(customerInfo.getCc_name(),transactionDetails.getInvoiceNumber(),transactionDetails.getTransactionDate()))
                .startX(50)
                .startY(tableDrawer.getFinalY()-gap)
                .endY(50F) // note: if not set, table is drawn over the end of the page
                .build();
    	tableDrawer.draw(() -> document, () -> new PDPage(PDRectangle.A4), 50f);
    	
    	tableDrawer = TableDrawer.builder()
                .table(addItems(transactionDetails))
                .startX(50)
                .startY(tableDrawer.getFinalY()-gap)
                .endY(50F) // note: if not set, table is drawn over the end of the page
                .build();
    	tableDrawer.draw(() -> document, () -> new PDPage(PDRectangle.A4), 50f);
    	
    	
    	
    	
    }
    
    
	private Table appendShopName(String shopName, String mobile) {
        final Table.TableBuilder tableBuilder = Table.builder()
                .addColumnOfWidth(500);

        TextCell dummyHeaderCell = TextCell.builder().text(shopName)
                .fontSize(18)
                .horizontalAlignment(HorizontalAlignment.CENTER)
                   .build();
        TextCell dummyHeaderCell1 = TextCell.builder().text("Cell "+mobile)
                .fontSize(9)
                .horizontalAlignment(HorizontalAlignment.CENTER)
                   .build();

        tableBuilder.addRow(
                Row.builder()
                        .add(dummyHeaderCell)
                        
                        .build());
        tableBuilder.addRow(
                Row.builder()
                        .add(dummyHeaderCell1)
                        
                        .build());
        return tableBuilder.build();
    }
    
    private Table appendBillDetails(String customerName, String billNo, String date) {
        final Table.TableBuilder tableBuilder = Table.builder()
                .addColumnOfWidth(80)
                .addColumnOfWidth(150)
                .addColumnOfWidth(95)
                .addColumnOfWidth(170);

       

        tableBuilder.addRow(
                Row.builder()
                        .add(createHeaderCell("Name",1, HorizontalAlignment.LEFT))
                        .add(createHeaderCell(customerName,3,HorizontalAlignment.LEFT))
                        .build());
        tableBuilder.addRow(
                Row.builder()
                        .add(createHeaderCell("Bill No",1,HorizontalAlignment.LEFT))
                        .add(createHeaderCell(billNo,1, HorizontalAlignment.LEFT))
                        .add(createHeaderCell("Date",1, HorizontalAlignment.RIGHT))
                        .add(createHeaderCell(date,1, HorizontalAlignment.LEFT))
                        .build());
        return tableBuilder.build();
    }
    
    private TextCell createHeaderCell(String text,int colSpan, HorizontalAlignment alignment) {
        return TextCell.builder()
                .font(PDType1Font.HELVETICA_BOLD)
                .text(text.toUpperCase())
                .colSpan(colSpan)
                .horizontalAlignment(alignment)
                //.padding(16f)
                .build();
    }
    
    private Table addItems(CCTransactionDTO transactionDetails) {
    	
        final Table.TableBuilder tableBuilder = Table.builder()
                .addColumnOfWidth(230)
                .addColumnOfWidth(90)
                .addColumnOfWidth(90)
                .addColumnOfWidth(90);

        tableBuilder.addRow(
                Row.builder()
                        .add(createHeaderCell("ITEM NAME",1,HorizontalAlignment.LEFT))
                        .add(createHeaderCell("QTY",1,HorizontalAlignment.RIGHT))
                        .add(createHeaderCell("PRICE",1,HorizontalAlignment.RIGHT))
                        .add(createHeaderCell("ToTAL",1,HorizontalAlignment.RIGHT))
                        .build());

        for (ShopTransactionDTO transaction : transactionDetails.getTransactions() ) {
        	if(transaction.getTransactionQty()>0){
            tableBuilder.addRow(
                    Row.builder()
                            .add(TextCell.builder()
                                    .text(transaction.getItemName())
                                    .build())
                            .add(TextCell.builder()
                                    .text(df.format(transaction.getTransactionQty()))
                                    .horizontalAlignment(HorizontalAlignment.RIGHT)
                                    .build())
                            .add(TextCell.builder()
                                    .text(df.format(transaction.getItemSoldCost())+"")
                                    .horizontalAlignment(HorizontalAlignment.RIGHT)
                                    .build())
                            .add(TextCell.builder()
                                    .text(df.format(transaction.getTransactionQty()*transaction.getItemSoldCost()))
                                    .horizontalAlignment(HorizontalAlignment.RIGHT)
                                    .build())
                            .build());}
        }
        
        
        tableBuilder.addRow(
                Row.builder()
                        .add(TextCell.builder()
                                .text("")
                                .colSpan(4)
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .build());

        tableBuilder.addRow(
                Row.builder()
                        .add(TextCell.builder()
                                .text("Current Purchase")
                                .colSpan(3)
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .font(PDType1Font.HELVETICA_BOLD)
                                .build())
                        .add(TextCell.builder()
                                .text(df.format(transactionDetails.getCc_total_amount()))
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .build());
       
       tableBuilder.addRow(
                Row.builder()
                        .add(TextCell.builder()
                                .text("Previous Credit")
                                .colSpan(3)
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .font(PDType1Font.HELVETICA_BOLD)
                                .build())
                        
                        .add(TextCell.builder()
                                .text((transactionDetails.getCc_prev_credit_amount())+"")
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .build());
        tableBuilder.addRow(
                Row.builder()
                        .add(TextCell.builder()
                                .text("Current Balance")
                                .colSpan(3)
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .font(PDType1Font.HELVETICA_BOLD)
                                .build())
                        .add(TextCell.builder()
                                .text((transactionDetails.getCc_total_amount()+transactionDetails.getCc_prev_credit_amount())+"")
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .build());
        tableBuilder.addRow(
                Row.builder()
                        .add(TextCell.builder()
                                .text("Discount Amount")
                                .font(PDType1Font.HELVETICA_BOLD)
                                .colSpan(3)
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .add(TextCell.builder()
                                .text(df.format(transactionDetails.getCc_discount()))
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .build());
        
        tableBuilder.addRow(
                Row.builder()
                        .add(TextCell.builder()
                                .text("Paid Amount")
                                .colSpan(3)
                                .font(PDType1Font.HELVETICA_BOLD)
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .add(TextCell.builder()
                                .text(df.format(transactionDetails.getCc_paid_amount()))
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .build());
        
        tableBuilder.addRow(
                Row.builder()
                        .add(TextCell.builder()
                                .text("Final Credit")
                                .font(PDType1Font.HELVETICA_BOLD)
                                .colSpan(3)
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .add(TextCell.builder()
                                .text(df.format(transactionDetails.getCc_total_amount()+transactionDetails.getCc_prev_credit_amount()-transactionDetails.getCc_discount()-transactionDetails.getCc_paid_amount()))
                                .horizontalAlignment(HorizontalAlignment.RIGHT)
                                .build())
                        .build());
        
       
    
        

        return tableBuilder.build();
    }
    
    public static void main(String[] args) throws IOException {
    	
    	SBookPDFGenerator pdf = new SBookPDFGenerator();
    	//pdf.drawMultipageTable();
		
	}
	
}
