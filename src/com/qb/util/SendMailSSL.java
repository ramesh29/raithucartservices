package com.qb.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;  
  
public class SendMailSSL {  
	
	
	public static void sendEmail(String to,String subject, String template)
	{
		//Get the session object  
		  Properties props = new Properties();  
		  props.put("mail.smtp.host", "smtp.gmail.com");  
		  props.put("mail.smtp.socketFactory.port", "465");  
		  props.put("mail.smtp.socketFactory.class",  
		            "javax.net.ssl.SSLSocketFactory");  
		  props.put("mail.smtp.auth", "true");  
		  props.put("mail.smtp.port", "465");  
		   
		  Session session = Session.getDefaultInstance(props,  
		   new javax.mail.Authenticator() {  
		   protected PasswordAuthentication getPasswordAuthentication() {  
		   return new PasswordAuthentication("support@mymarinecourse.com","sales@Tractile");//change accordingly  
		   }  
		  });  
		  
		  try {  
			   MimeMessage message = new MimeMessage(session);  
			   message.setFrom(new InternetAddress("support@mymarinecourse.com"));//change accordingly  
			   message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
			   message.setSubject(subject);  
			   //message.setText("Testing.......");  
			   message.setContent(template,"text/html" );
			   //send message  
			   Transport.send(message);  
			  
			   System.out.println("message sent successfully");  
			   
			  } catch (MessagingException e) {throw new RuntimeException(e);}  
		  
		  
		  
	}
  
}  