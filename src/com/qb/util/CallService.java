package com.qb.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


public class CallService {
	
	public static String callService(String wsurl, String requestParams, String requestType)
	{
		
		String responseString = "";
		String outputString = "";
		
		try
		{
			URL url = new URL(wsurl);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection)connection;
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			
			 
			byte[] buffer = new byte[requestParams.length()];
			buffer = requestParams.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty("Content-Length",String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			//httpConn.setRequestProperty("SOAPAction", SOAPAction);
			httpConn.setRequestMethod(requestType);
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			OutputStream out = httpConn.getOutputStream();
			//Write the content of the request to the outputstream of the HTTP Connection.
			out.write(b);
			out.close();
			//Ready with sending the request.
			 
			//Read the response.
			InputStreamReader isr =
			new InputStreamReader(httpConn.getInputStream());
			BufferedReader in = new BufferedReader(isr);
			 
			//Write the SOAP message response to a String.
			while ((responseString = in.readLine()) != null) {
			outputString = outputString + responseString;
			
			}
			return outputString;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return outputString;			
	}

}
