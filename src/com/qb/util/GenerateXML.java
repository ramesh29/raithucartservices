package com.qb.util;

import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.qb.dto.BookingDTO;

public class GenerateXML {
	
	public static String generateXML(List<BookingDTO> bookings,Double convienceCharge, int transactionId)
	{
		String xmlString = null;
		
		 try {

				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

				// root elements
				Document doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("products");
				doc.appendChild(rootElement);
				Double totalCharge = 0.0;
				
				Double orgPrice = 0.0;
				String agreementType = null;
				Double agreementValue = null;
				String atomProductName = null;
				int orgId = 0;
				String bookingNumber = "";
				String scheduleIds = "";
				for(BookingDTO booking : bookings)
				{
					
					orgPrice = orgPrice+Double.parseDouble(booking.getOrgPrice());
					agreementType = booking.getPaymentAgreementType();
					agreementValue = Double.parseDouble(booking.getPaymentAgreementValue());
					atomProductName = booking.getAtomProdName();
					orgId = booking.getOrgId();
					if(bookingNumber.length() > 0)
					{
						bookingNumber = booking.getBookingNumber();
						scheduleIds = booking.getScheduleId()+"";
					}else
					{
						bookingNumber = bookingNumber+","+booking.getBookingNumber();
						scheduleIds = scheduleIds+","+booking.getScheduleId();
					}
					
					
				}
				
				if("FLAT".equalsIgnoreCase(agreementType))
				{
					totalCharge = totalCharge+agreementValue;
					orgPrice = orgPrice - agreementValue;
				}else
				{
					agreementValue = ((agreementValue/100)*orgPrice);
					totalCharge = totalCharge+agreementValue;
					orgPrice = orgPrice - agreementValue;
				}
				
				// product elements
				
				
				Element product = doc.createElement("product");
				rootElement.appendChild(product);

				Element id = doc.createElement("id");
				id.appendChild(doc.createTextNode("1"));
				product.appendChild(id);
				
				Element name = doc.createElement("name");
				name.appendChild(doc.createTextNode(atomProductName));
				product.appendChild(name);
				
				Element amount = doc.createElement("amount");
				amount.appendChild(doc.createTextNode(orgPrice+""));
				product.appendChild(amount);
				
				Element param1 = doc.createElement("param1");
				param1.appendChild(doc.createTextNode("Booking Number "+bookingNumber));
				product.appendChild(param1);
				
				Element param2 = doc.createElement("param2");
				param2.appendChild(doc.createTextNode("Schedule Id "+scheduleIds));
				product.appendChild(param2);
				
				Element param3 = doc.createElement("param3");
				param3.appendChild(doc.createTextNode("College Id "+orgId));
				product.appendChild(param3);
				
				Element param4 = doc.createElement("param4");
				param4.appendChild(doc.createTextNode("College Id "+orgId));
				product.appendChild(param4);
				
				Element param5 = doc.createElement("param5");
				param5.appendChild(doc.createTextNode("College Id "+orgId));
				product.appendChild(param5);
				
				
				//Tractile Percentage
				
				
				product = doc.createElement("product");
				rootElement.appendChild(product);

				id = doc.createElement("id");
				id.appendChild(doc.createTextNode("2"));
				product.appendChild(id);
				
				name = doc.createElement("name");
				name.appendChild(doc.createTextNode(IPaymentGatewayURLS.tractileProductName));
				product.appendChild(name);
				
				amount = doc.createElement("amount");
				amount.appendChild(doc.createTextNode((totalCharge+convienceCharge)+""));
				product.appendChild(amount);
				
				param1 = doc.createElement("param1");
				param1.appendChild(doc.createTextNode("Transaction Id  "+transactionId));
				product.appendChild(param1);

				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				//StreamResult result = new StreamResult(new File("C:\\file.xml"));

				StringWriter outWriter = new StringWriter();
				// Output to console for testing
				 StreamResult result = new StreamResult(outWriter);

				transformer.transform(source, result);
				
				StringBuffer sb = outWriter.getBuffer(); 
				xmlString = sb.toString();
				
				System.out.println("File saved! "+xmlString);

			  } catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			  } catch (TransformerException tfe) {
				tfe.printStackTrace();
			  }
		
		
		return xmlString;
	}
	
	public static String generateXMLForShop(int transactionId)
	{
		String xmlString = null;
		
		 try {

				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

				// root elements
				Document doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("products");
				doc.appendChild(rootElement);
				Double totalCharge = 0.0;
				
				
				Element product = doc.createElement("product");
				rootElement.appendChild(product);

				Element id = doc.createElement("id");
				id.appendChild(doc.createTextNode("2"));
				product.appendChild(id);
				
				Element name = doc.createElement("name");
				name.appendChild(doc.createTextNode(IPaymentGatewayURLS.tractileProductName));
				product.appendChild(name);
				
				Element amount = doc.createElement("amount");
				amount.appendChild(doc.createTextNode((totalCharge)+""));
				product.appendChild(amount);
				
				Element param1 = doc.createElement("param1");
				param1.appendChild(doc.createTextNode("Transaction Id  "+transactionId));
				product.appendChild(param1);

				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				//StreamResult result = new StreamResult(new File("C:\\file.xml"));

				StringWriter outWriter = new StringWriter();
				// Output to console for testing
				 StreamResult result = new StreamResult(outWriter);

				transformer.transform(source, result);
				
				StringBuffer sb = outWriter.getBuffer(); 
				xmlString = sb.toString();
				
				System.out.println("File saved! "+xmlString);

			  } catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			  } catch (TransformerException tfe) {
				tfe.printStackTrace();
			  }
		
		
		return xmlString;
	}
	

}
