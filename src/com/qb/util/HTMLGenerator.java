package com.qb.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.qb.dto.HTMlAttributesDTO;

public class HTMLGenerator {

	private static final Logger log = Logger.getLogger(HTMLGenerator.class.getName());

	public static String getModifiedHTML(HTMlAttributesDTO htmlattributes) throws IOException
	{
		File html= new File(htmlattributes.getHtmlFileName());

		HashMap<String, String> replaceMap = htmlattributes.getReplaceValues();

		String totalStr = "";
		FileReader fr = new FileReader(html);
		String s;
		
		try (BufferedReader br = new BufferedReader(fr)) {

			while ((s = br.readLine()) != null) {
				totalStr += s;
			}
			
			

		} catch (IOException e) {
			log.error(e.getMessage());
		}finally {
			
		}
		
		//file reading
		
		for(Map.Entry<String, String> entry : replaceMap.entrySet())
		{
			String search = entry.getKey();  // <- changed to work with String.replaceAll()
			String replacement =  entry.getValue();

			totalStr = totalStr.replaceAll(search, replacement);

		
		}
		System.out.println(totalStr);
		return totalStr;
	}
}
