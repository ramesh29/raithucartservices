package com.qb.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.qb.dao.ABaseDAO;
import com.qb.dao.DataAccessFactory;
import com.qb.exception.DataAccessException;
import com.qb.exception.ErrorCode;
import com.tt.util.IQueryConstants;

public class OrgCourseMapper extends ABaseDAO{

	public static Map<String, Integer> columnIndexMap = new HashMap<String, Integer>();
	
	public static final int orgId = 5;
	
	public static void loadColumnIndexes()
    {
		columnIndexMap.put("course_id",0);
        columnIndexMap.put("course_code",2);
        columnIndexMap.put("org_id",3);
    }
	
	public static void main(String[] args) throws ServletException {
		try {
			//init(propsFolderPath);
			loadColumnIndexes();
			loadData();
		
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void init(String path) throws ServletException {
		
		// setting of the servlet context path
		DataAccessFactory.instance().setContextPath(path);
		
		System.out.println("Server context path : "+ DataAccessFactory.instance().getContextPath());
		
		// call initPropeties method
		DataAccessFactory.instance().initProperties();	
	}

	
	@SuppressWarnings("resource")
	public static void loadData() throws DataAccessException
	{
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

            File inputWorkbook = new File("D:/Office/Personal/Personal/REST SERVICES/CourseMapping/courseMapper.xls");
            Workbook w;
            w = new HSSFWorkbook(new FileInputStream(inputWorkbook));
            FormulaEvaluator evaluator = w.getCreationHelper().createFormulaEvaluator();
            Sheet cursheet1=w.getSheet("Sheet1");
            con = getDBConnection();
            String sql = "UPDATE e_org_courses set course_code = ? where org_id=? and course_id=? ";
            pstmt = con.prepareStatement(sql);
            System.out.println("Job Started");
            for(int j=1;j<cursheet1.getPhysicalNumberOfRows();j++) 
            {
            	System.out.println("Row # "+j);
            	Row row1 = cursheet1.getRow(j);
                Row.MissingCellPolicy mcp = cursheet1.getWorkbook().getMissingCellPolicy();
            	if(getCellAsString(row1, columnIndexMap.get("course_id"), mcp, evaluator) !=null && getCellAsString(row1, columnIndexMap.get("course_id"), mcp, evaluator).trim().length() > 0)
            	{
            		String code = getCellAsString(row1, columnIndexMap.get("course_code"), mcp, evaluator);
            		String orgId = getCellAsString(row1, columnIndexMap.get("org_id"), mcp, evaluator);
            		String courseId = getCellAsString(row1, columnIndexMap.get("course_id"), mcp, evaluator);
            		
            		pstmt.setInt(1,new Double(code).intValue());
					pstmt.setInt(2,new Double(orgId).intValue());
					pstmt.setInt(3,new Double(courseId).intValue());
					pstmt.addBatch();
                }
	        }
            pstmt.executeBatch();            
            
        }catch (IOException e) {
            System.out.println("Cannot read file");
            e.printStackTrace();
        } catch (Exception e) {
            cleanUp(rs, pstmt, con);
        	System.out.println("Exception occurred while running");
            e.printStackTrace();
        }finally {
        	if(pstmt != null)
			{
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(rs != null )
			{
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			System.out.println("Job Ended");
		}

	}
	
	
		
	
	private static String getCellAsString(Row row,int index,Row.MissingCellPolicy mcp,FormulaEvaluator evaluator){
        return getCellAsString(row, index, mcp, evaluator, false);
    }

    private static String getCellAsString(Row row,int index,Row.MissingCellPolicy mcp,FormulaEvaluator evaluator, Boolean isNullToBeReturned)
    {
        String retValue = null;
        //index cannot be less than zero
        Cell cell = row.getCell(index);
        if(cell!=null)
        {
            String result = row.getCell(index, mcp).toString();
            if(row.getCell(index, mcp).getCellType() == Cell.CELL_TYPE_FORMULA)
            {
                int cellValue = evaluator.evaluateFormulaCell(cell);
                result = cell.getStringCellValue();
                if(cellValue == Cell.CELL_TYPE_STRING)
                {
                    result = cell.getStringCellValue();
                }
                else if(cellValue == Cell.CELL_TYPE_NUMERIC)
                {
                    Double numericValue = cell.getNumericCellValue();
                    if (numericValue % 1 == 0)
                    {
                        result = numericValue.intValue() + "";
                    }
                    else
                    {
                        result = numericValue+"";
                    }
                }
            }
            retValue =  result;
        }else{
            retValue = null;
        }
        if(!isNullToBeReturned && retValue == null)
            return "";
        else
            return retValue.trim();


    }
    
    private static Connection getDBConnection() {

		Connection dbConnection = null;

		try {

			Class.forName(IQueryConstants.DB_DRIVER);

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(IQueryConstants.URL,IQueryConstants.USERNAME,IQueryConstants.PASSWORD);
			return dbConnection;

		} catch (SQLException e) {

			System.out.println("Exception Occured in getDBConnection method "+e.getMessage());
		}
		return dbConnection;
	}
    
    public static void cleanUp(ResultSet rs, Statement stmt, Connection con) throws DataAccessException {
		try {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			throw new DataAccessException(ErrorCode.ERR_CLOSE_CONNECTION, e, null);
		}
	}
    

   
	
}
