package com.qb.filters;


import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class SessionFilter implements Filter {

	private static final Logger log = Logger.getLogger(SessionFilter.class.getName());	
	
	private String timeoutPage = "pages/SessionExpiredLogout.jsp";
	
	public void init(FilterConfig config) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response,FilterChain filterChain) throws IOException, ServletException 
	{
		
		
		if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) 
		{
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			// is session expire control required for this request?

			HttpSession session = httpServletRequest.getSession(false);
			
			if (session != null && session.getAttribute("userName") != null) 
			{
				filterChain.doFilter(request, response);
				return;
				
			}
			
			log.debug("Request URI : " + httpServletRequest.getRequestURI());
	
			String url = httpServletRequest.getRequestURI();
			System.out.println("URL : "+url);
			if(url.contains("/ajax/")){
				httpServletResponse.getOutputStream().print("{\"success\":false,\"code\":1000,\"msg\":\"Your active session is expired. Please login again!!!\"}");
				return;
			}
			if (httpServletRequest.getRequestURI().contains("index") || httpServletRequest.getRequestURI().contains(".css") || url.contains("/images/")) 
			{
				
				filterChain.doFilter(request, response);
			} else if(httpServletRequest.getRequestURI().contains("SessionExpiredLogout")) {
				filterChain.doFilter(request, response);
			}else if(url.contains("LicenseUpload.xhtml"))
			{
				filterChain.doFilter(request, response);
			}else {
				String timeoutUrl = httpServletRequest.getContextPath() + "/" + getTimeoutPage();
				httpServletResponse.sendRedirect(timeoutUrl);
				return;
			}
			
		}
	}

	public void destroy() {

	}

	public String getTimeoutPage() {
		return timeoutPage;
	}

	public void setTimeoutPage(String timeoutPage) {
		this.timeoutPage = timeoutPage;
	}

}
