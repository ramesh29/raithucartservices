package com.qb.filters;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
 

/**
 * This filter verify the access permissions for a user
 * based on username and passowrd provided in request
 * */
@Provider
public class AuthenticationFilter implements ContainerRequestFilter
{
	@Context
    HttpServletRequest httpServletRequest;
    @Context
    private UriInfo info;
     
    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Bearer";
      
 	@Override
	public ContainerRequest filter(ContainerRequest requestContext) {
 		
 		URI url = requestContext.getAbsolutePath();
 		if(url.getPath().contains("index") || url.getPath().contains("loginService") || url.getPath().contains("bookingService")
 				|| url.getPath().contains("courseService")
 				|| url.getPath().contains("dashboardService")
 				|| url.getPath().contains("orgService")
 				|| url.getPath().contains("scheduleService")
 				|| url.getPath().contains("supportService")
 				|| url.getPath().contains("file")
 				|| url.getPath().contains("shop")
 				)
 		{
 			return requestContext;
 		}
		//  //Get request headers
        final MultivaluedMap<String, String> headers = requestContext.getRequestHeaders();
        
        //Fetch authorization header
        final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);
          
        //If no authorization information present; block access
        if(authorization == null || authorization.isEmpty())
        {
        	throw new WebApplicationException(Status.UNAUTHORIZED);
        }
          
        //Get encoded username and password
        final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");
          
        //Decode username and password
        
        
        HttpSession session = httpServletRequest.getSession(false);
		
		if (session != null && session.getAttribute("userId") != null && session.getId().equalsIgnoreCase(encodedUserPassword)) 
		{
			//filterChain.doFilter(request, response);
			return requestContext;
			
		}else{
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}
	}
}