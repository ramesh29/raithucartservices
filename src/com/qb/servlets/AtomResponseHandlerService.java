package com.qb.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.IBookingDAO;
import com.qb.dto.AtomPaymentDTO;
import com.qb.dto.HTMlAttributesDTO;
import com.qb.dto.OrgBookingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.dto.TransactionDTO;
import com.qb.exception.DataAccessException;
import com.qb.util.ConnectionManager;
import com.qb.util.HTMLGenerator;
import com.qb.util.IPaymentGatewayURLS;
import com.qb.util.QuickBookUtil;
import com.qb.util.SendMailSSL;
import com.tt.schedulerServices.YAKDataPuller;

/**
 * Servlet implementation class Sample
 */
@WebServlet("/AtomResponseHandlerService")
public class AtomResponseHandlerService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	IBookingDAO bookingDAO = DataAccessFactory.instance().getBookingDAO();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AtomResponseHandlerService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @return 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		Map<String, String[]> formVal =  request.getParameterMap();
		AtomPaymentDTO atom = new AtomPaymentDTO();
		for(Map.Entry<String, String[]> entry : formVal.entrySet())
		{
			System.out.println(entry.getKey() + " = " +entry.getValue()[0]);
			
			if("mer_txn".equalsIgnoreCase(entry.getKey()))
			{
				atom.setTransactionId(Integer.parseInt(entry.getValue()[0]));
			}else if("mmp_txn".equalsIgnoreCase(entry.getKey()))
			{
				atom.setMmp_txn(entry.getValue()[0]);
			}else if("bank_txn".equalsIgnoreCase(entry.getKey()))
			{
				atom.setBankTransactionId(entry.getValue()[0]);
			}else if("bank_name".equalsIgnoreCase(entry.getKey()))
			{
				atom.setBankName(entry.getValue()[0]);
			}
			else if("f_code".equalsIgnoreCase(entry.getKey()))
			{
				atom.setTransactionStatus(entry.getValue()[0]);
			}else if("discriminator".equalsIgnoreCase(entry.getKey()))
			{
				atom.setDiscriminator(entry.getValue()[0]);
			}else if("CardNumber".equalsIgnoreCase(entry.getKey()))
			{
				atom.setCardNumber(entry.getValue()[0]);
			}else if("desc".equalsIgnoreCase(entry.getKey()))
			{
				atom.setTransactionMsg(entry.getValue()[0]);
			}else if("amt".equalsIgnoreCase(entry.getKey()))
			{
				atom.setAmount(entry.getValue()[0]);
			}else if("surcharge".equalsIgnoreCase(entry.getKey()))
			{
				atom.setSurcharge(entry.getValue()[0]);
			}else if("date".equalsIgnoreCase(entry.getKey()))
			{
				atom.setDate(entry.getValue()[0]);
			}else if("udf2".equalsIgnoreCase(entry.getKey()))
			{
				atom.setUdf2(entry.getValue()[0]);
			}else if("udf3".equalsIgnoreCase(entry.getKey()))
			{
				atom.setUdf3(entry.getValue()[0]);
			}
		}

		List<Integer> scheduleIds = new ArrayList<Integer>();
		
		if(atom.getUdf3() != null)
		{
			List<String> scheduleIdstr = Arrays.asList(atom.getUdf3().split("\\s*,\\s*"));
			
			for(String str : scheduleIdstr)
			{
				scheduleIds.add(Integer.parseInt(str));
			}
	
		}else
		{
			try {
				Map<Integer, List<Integer>>  map = bookingDAO.getSchedulesForTransaction(atom.getTransactionId());
				
				for(Map.Entry<Integer, List<Integer>> entry : map.entrySet())
				{
					atom.setUdf2(entry.getKey()+"");
					scheduleIds = entry.getValue();
				}
				
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
				
		
		try {
			bookingDAO.updateAtomTransaction(atom);
			boolean status = false;
			if("OK".equalsIgnoreCase(atom.getTransactionStatus()))
			{
				int confirmBooking = bookingDAO.bookConfirm(scheduleIds);
				if(confirmBooking > 0)
				{
					TransactionDTO transactionDTO = new TransactionDTO();
					transactionDTO.setUserId(Integer.parseInt(atom.getUdf2()));
					transactionDTO.setTransactionStatus("SUCCESS");
					transactionDTO.setTransactionId(atom.getTransactionId());
					int updateTransaction = bookingDAO.updateTransaction(transactionDTO);
							
					if(updateTransaction > 0)
					{
						int updateBooking = bookingDAO.updateBooking(transactionDTO);
						if(updateBooking > 0)
						{
							Connection con = ConnectionManager.instance().getConnection();
							YAKDataPuller.bookCourse(con, atom.getTransactionId(), scheduleIds);
							
							status = true;
							
							//Send email to user
							
							if(scheduleIds != null && scheduleIds.size() > 0)
							{
								YAKDataPuller.validateCourseSchedules(con,scheduleIds);
							}
						
							
							sendBookingEmails(atom.getTransactionId(), Integer.parseInt(atom.getUdf2()), atom);
							
						}
					}
				}
			}else
			{
				if(bookingDAO.isTransactionUpdated(atom.getTransactionId()))
				{
					int failBooking = bookingDAO.bookingFailed(scheduleIds);
					if(failBooking > 0)
					{
						TransactionDTO transactionDTO = new TransactionDTO();
						transactionDTO.setUserId(Integer.parseInt(atom.getUdf2()));
						transactionDTO.setTransactionStatus("ERROR");
						transactionDTO.setTransactionId(atom.getTransactionId());
						int updateTransaction = bookingDAO.updateTransaction(transactionDTO);
								
						if(updateTransaction > 0)
						{
							int updateBooking = bookingDAO.updateBooking(transactionDTO);
							if(updateBooking > 0)
							{
								status = true;
							}
						}
					}	
				}
				
				
			}
				
			response.sendRedirect(IPaymentGatewayURLS.ServerURL+"MyMarineCourse/pages/home.html?status="+atom.getTransactionStatus().toUpperCase());
			
			
			
		} catch (DataAccessException e) {
			// TODO Auto-generated catch blocks
			System.out.println("hi");
			e.printStackTrace();
		}
		
		
	}
	
	
	public void sendBookingEmails(int transactionId, int userId, AtomPaymentDTO atom )
	{
		SearchParametersDTO search = new SearchParametersDTO();
		search.setTransactionId(transactionId);
		search.setUserId(userId);
		ResponseDTO<OrgBookingDTO> r = new ResponseDTO<OrgBookingDTO>();
		
		try {
			bookingDAO.getUserBookingInfo(search, r);
			List<OrgBookingDTO> bookings = r.getData();
			if(bookings != null && !bookings.isEmpty())
			{
				for(OrgBookingDTO bookingInfo : bookings)
				{
					String bookedNumber = bookingInfo.getBookingNumber();
					String qrBase64QRText = QuickBookUtil.generateBase64QR(bookedNumber);
					String filePath = getServletContext().getRealPath("EmailTemplates/Reciept.html");
					
					
					HTMlAttributesDTO html = new HTMlAttributesDTO();
					html.setHtmlFileName(filePath);
					HashMap<String, String> replaceValues = new HashMap<>();
					replaceValues.put(":replaceText", qrBase64QRText);
					replaceValues.put(":courseName", bookingInfo.getCourseName());
					replaceValues.put(":student", bookingInfo.getFullName());
					replaceValues.put(":orgName", bookingInfo.getOrgName());
					replaceValues.put(":startTime", bookingInfo.getTimings());
					replaceValues.put(":coursePrice", bookingInfo.getAmountPaid());
					replaceValues.put(":startDate", bookingInfo.getStartDate() +" to "+bookingInfo.getEndDate());
					replaceValues.put(":bookingNumber", bookedNumber);
					Double totalPrice = Double.parseDouble(bookingInfo.getAmountPaid());
					DecimalFormat df = new DecimalFormat("#.##");     
					/*Double serviceTax = (IConstants.serviceTax/100)*totalPrice;
					serviceTax = Double.valueOf(df.format(serviceTax));*/
					Double convienceCharge = (IPaymentGatewayURLS.convienceCost/100)*totalPrice;
					convienceCharge = Double.valueOf(df.format(convienceCharge));
					/*Double paymentGateway = (IConstants.paymentGateway/100)*totalPrice;
					paymentGateway = Double.valueOf(df.format(paymentGateway));*/
					//totalPrice = totalPrice+serviceTax+convienceCharge+paymentGateway;
					totalPrice = totalPrice+convienceCharge;
					totalPrice = Double.valueOf(df.format(totalPrice));
					replaceValues.put(":internetHandlingCharges", convienceCharge+"");
					//replaceValues.put(":totalPaymentGatewayCharges", paymentGateway+"");
					replaceValues.put(":totalCost", totalPrice+"");
					replaceValues.put(":bookingDate", bookingInfo.getPaymentDateTime()+"");
					
					html.setReplaceValues(replaceValues);
					try {
						InputStream is = new ByteArrayInputStream(HTMLGenerator.getModifiedHTML(html).getBytes(StandardCharsets.UTF_8));
						
						SendMailSSL.sendEmail(bookingInfo.getUsername(),"Your Receipt", IOUtils.toString(is, "UTF-8"));
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					//Confirmation to College
					
					filePath = getServletContext().getRealPath("EmailTemplates/CollegeReciept.html");
										
					html = new HTMlAttributesDTO();
					html.setHtmlFileName(filePath);
					replaceValues = new HashMap<>();
					replaceValues.put(":replaceText", qrBase64QRText);
					replaceValues.put(":courseName", bookingInfo.getCourseName());
					replaceValues.put(":student", bookingInfo.getFullName());
					replaceValues.put(":orgName", bookingInfo.getOrgName());
					replaceValues.put(":startTime", bookingInfo.getTimings());
					replaceValues.put(":coursePrice", bookingInfo.getAmountPaid());
					replaceValues.put(":startDate", bookingInfo.getStartDate() +" to "+bookingInfo.getEndDate());
					replaceValues.put(":bookingNumber", bookedNumber);
					totalPrice = Double.parseDouble(bookingInfo.getAmountPaid());
					/*df = new DecimalFormat("#.##");     
					convienceCharge = (IPaymentGatewayURLS.convienceCost/100)*totalPrice;
					convienceCharge = Double.valueOf(df.format(convienceCharge));*/
					totalPrice = Double.valueOf(df.format(totalPrice));
					replaceValues.put(":totalCost", totalPrice+"");
					replaceValues.put(":bookingDate", bookingInfo.getPaymentDateTime()+"");
					replaceValues.put(":college", bookingInfo.getOrgName());
					replaceValues.put(":bankName", atom.getBankName());
					replaceValues.put(":fundTransferStatus", atom.getTransactionStatus());
					replaceValues.put(":atomTransactionId", atom.getBankTransactionId());
					replaceValues.put(":cardNumber", atom.getCardNumber());
					replaceValues.put(":cardIssuingBank", atom.getBankName());
					replaceValues.put(":clientName", bookingInfo.getFullName());
					replaceValues.put(":clientEmail", bookingInfo.getUsername());
					replaceValues.put(":clientMobile", bookingInfo.getPhone());
					
					
					
					
					html.setReplaceValues(replaceValues);
					try {
						InputStream is = new ByteArrayInputStream(HTMLGenerator.getModifiedHTML(html).getBytes(StandardCharsets.UTF_8));
						
						SendMailSSL.sendEmail(bookingInfo.getOrgEmail(),"New Course Booked at www.mymarinecourse.com", IOUtils.toString(is, "UTF-8"));
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					
				}
				
				
				
				
			   }
			
			
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
