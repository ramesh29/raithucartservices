/* Project  : Quick Booking
 * File     : ABaseDAO.java
 * version	: 1.0
 * Date     : 24-Nov-2016
 * Author   : Suresh Krishna
 *
 * Revision History
 * Date		      Release 	   Who		   Description
 * ----           -------      ----        ------------- 
 * 24-Nov-2016    1.0          Suresh    Initial Version	
 * 
 * Copyright 2016 Innovative Technologies, Inc. All rights reserved.
 */
package com.qb.dao;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.qb.dao.query.Query;




public abstract class ABaseDAO {

	private static final Logger log = Logger.getLogger(ABaseDAO.class.getName());
	private Map<String, Query> queries = null;
	
	public ABaseDAO() {
		
	}
	
	public Map<String, Query> getQueries() {
		return queries;
	}

	public void setQueries(Map<String, Query> queries) {
		this.queries = queries;
	}
	
	public static String setIntegerList(String query, List<Integer> array, int index)
	{
		if(query != null && array != null && array.size() >0)
		{
			String strArray = "";
			for (int i = 0; i < array.size(); i++) {					
				strArray = (strArray.length()>0) ? strArray+", "+array.get(i) : strArray+array.get(i);							
			}
			
			int replaceIndex = -1;
			int i=0;
			for(i=0; i<index; i++)
			{
				if((replaceIndex = query.indexOf('?', replaceIndex+1))>0) 
					continue;
				else 
					log.error("Index not found...");
					throw new ArrayIndexOutOfBoundsException();					
			}
			if(replaceIndex != -1 && i==index)
			query = query.substring(0, replaceIndex)+strArray+query.substring(replaceIndex+1);
		}		
		return query;
	}
	
	public static String setArray(String query, int[] array, int index)
	{
		if(query != null && array != null && array.length >0)
		{
			String strArray = "";
			for (int i = 0; i < array.length; i++) {					
				strArray = (strArray.length()>0) ? strArray+", "+array[i] : strArray+array[i];							
			}
			
			int replaceIndex = -1;
			int i=0;
			for(i=0; i<index; i++)
			{
				if((replaceIndex = query.indexOf('?', replaceIndex+1))>0) 
					continue;
				else 
					log.error("Index not found...");
					throw new ArrayIndexOutOfBoundsException();					
			}
			if(replaceIndex != -1 && i==index)
			query = query.substring(0, replaceIndex)+strArray+query.substring(replaceIndex+1);
		}		
		return query;
	}
	
	public static String setArray(String query, Integer[] array, int index)
	{
		if(query != null && array != null && array.length >0)
		{
			String strArray = "";
			for (int i = 0; i < array.length; i++) {					
				strArray = (strArray.length()>0) ? strArray+", "+array[i] : strArray+array[i];							
			}
			
			int replaceIndex = -1;
			int i=0;
			for(i=0; i<index; i++)
			{
				if((replaceIndex = query.indexOf('?', replaceIndex+1))>0) 
					continue;
				else 
					log.error("Index not found...");
					throw new ArrayIndexOutOfBoundsException();					
			}
			if(replaceIndex != -1 && i==index)
			query = query.substring(0, replaceIndex)+strArray+query.substring(replaceIndex+1);
		}		
		return query;
	}
	
	protected String getDateAsString(Date date, String dateFormat) {
		if(date != null)
		{
			if(dateFormat == null){
				dateFormat = "MM-dd-yyyy";
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			return formatter.format(date);
		}		
		return null;
	}
	
	public static String getDateAsString(java.util.Date date, String dateFormat) {
		if(date != null)
		{
			if(dateFormat == null){
				dateFormat = "MM-dd-yyyy";
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			return formatter.format(date);
		}		
		return null;
	}
	
	protected String getDate(long milliSeconds, String dateFormat)
	{
	    // Create a DateFormatter object for displaying date in specified format.
		if(dateFormat == null){
		   dateFormat = "MM-dd-yyyy";
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);

	    // Create a calendar object that will convert the date and time value in milliseconds to date. 
	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milliSeconds);
	     return formatter.format(calendar.getTime());
	}
	

	public static String replaceAllOccurences(String base, String strTobeReplaced, String strToReplace){
		while(base.contains(strTobeReplaced)){
			base = base.replace(strTobeReplaced, strToReplace);
		}
		return base;
	}
	
	protected java.util.Date getStringAsDate(String date, String dateFormat) {
		if(date != null)
		{
			if(dateFormat == null){
				   dateFormat = "MM-dd-yyyy";
				}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			try{
				return formatter.parse(date);
			}catch (ParseException e) {
				log.error("Exception occurred while converting the string to date : "+e.getMessage());
			}
		}		
		return null;
	}
	
	protected boolean containsIgnoreCase(String paramStr, List<String> list) {
        if(list != null && list.size() > 0 && paramStr != null){
			for (String s : list) {
	            if (paramStr.equalsIgnoreCase(s)) 
	            	return true;
	        }
        }
        return false;
    }
	
	protected static String setList(String query, List<String> stringList, int index) 
	{	
		if (query != null) 
		{
			String result = "%";
			if(stringList.size() > 0)
			{
				String separator = ", ";
				int total = stringList.size() * separator.length();
				for (String s : stringList) {
				    total += s.length();
				}

				StringBuilder sb = new StringBuilder(total);
				for (String s : stringList) {
				    sb.append(separator).append("'"+s+"'");
				}

				result = sb.substring(separator.length());	
			}

			int replaceIndex = -1;
			int i = 0;
			for (i = 0; i < index; i++) {
				if ((replaceIndex = query.indexOf('?', replaceIndex + 1)) > 0)
					continue;
				else
					//log.error("Index not found...");
				throw new ArrayIndexOutOfBoundsException();
			}
			if (replaceIndex != -1 && i == index)
				query = query.substring(0, replaceIndex) + result
						+ query.substring(replaceIndex + 1);
		}
		return query;
	}
}
