package com.qb.dao;

import com.qb.dto.CourseBookingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ScheduleDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.exception.DataAccessException;

public interface IScheduleDAO {
	
	public ResponseDTO<ScheduleDTO> getSchedules(ResponseDTO<ScheduleDTO> response) throws DataAccessException;
	
	public ResponseDTO<ScheduleDTO> addSchdule(ResponseDTO<ScheduleDTO> response,ScheduleDTO schduleDTO) throws DataAccessException;
	
	public ResponseDTO<ScheduleDTO> updateSchdule(ResponseDTO<ScheduleDTO> response,ScheduleDTO schduleDTO) throws DataAccessException;
	
	public ResponseDTO<ScheduleDTO> deleteSchdule(ResponseDTO<ScheduleDTO> response,ScheduleDTO schduleDTO) throws DataAccessException;
	
	public ResponseDTO<CourseBookingDTO> getOrgCourseSchdules( SearchParametersDTO search, ResponseDTO<CourseBookingDTO> response) throws DataAccessException;

}
