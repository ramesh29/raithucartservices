package com.qb.dao;

import java.util.List;

import com.qb.dto.DashboardDTO;
import com.qb.exception.DataAccessException;

public interface IDashboardDAO {
	
	public List<DashboardDTO> getBookingInfo(int orgId, String startDate,String endDate, String bookingType) throws DataAccessException;
	
	public List<DashboardDTO> getScheduleInfo(int orgId,  String bookingType) throws DataAccessException;

}
