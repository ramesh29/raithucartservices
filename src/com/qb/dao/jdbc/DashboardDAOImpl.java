package com.qb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.qb.dao.ABaseDAO;
import com.qb.dao.IDashboardDAO;
import com.qb.dao.query.Query;
import com.qb.dto.DashboardDTO;
import com.qb.exception.DataAccessException;
import com.qb.util.ConnectionManager;

public class DashboardDAOImpl extends ABaseDAO implements IDashboardDAO{
	
	private static final Logger log = Logger.getLogger(DashboardDAOImpl.class.getName());

	@Override
	public List<DashboardDTO> getBookingInfo(int orgId, String startDate, String endDate, String bookingType)
			throws DataAccessException {

		List<DashboardDTO>  dashboardDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("getBookingInfo");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, orgId);
			pstmt.setString(2, bookingType);
			//pstmt.setString(3, startDate);
			//pstmt.setString(4, endDate);   /*   AND date_format(b.creation_on, '%d-%b-%Y') between ? and ? */
			rs = pstmt.executeQuery();
			
			DashboardDTO dashboardDTO = null;
			while(rs.next()) {
				dashboardDTO =  new DashboardDTO();
				dashboardDTO.setCourseName(rs.getString("courseName"));
				dashboardDTO.setUsername(rs.getString("userName"));
				dashboardDTO.setTrasactionDate(rs.getString("transactionDate"));
				dashboardDTO.setPrice(rs.getString("price"));
				dashboardDTOs.add(dashboardDTO);
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getScheduleDropList method : "+e.getMessage());
		
			
		} catch(Exception e){
			log.error("Error occurred in getSchedules method : "+e.getMessage());
			
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return dashboardDTOs;
	}

	@Override
	public List<DashboardDTO> getScheduleInfo(int orgId, String bookingType) throws DataAccessException {

		List<DashboardDTO>  dashboardDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get(bookingType);
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, orgId);
			rs = pstmt.executeQuery();
			
			DashboardDTO dashboardDTO = null;
			while(rs.next()) {
				dashboardDTO =  new DashboardDTO();
				dashboardDTO.setCourseName(rs.getString("courseName"));
				dashboardDTO.setStartDate(rs.getString("startDate"));
				dashboardDTO.setEndDate(rs.getString("endDate"));
				dashboardDTO.setPrice(rs.getString("price"));
				dashboardDTO.setStartTime(rs.getString("startTime"));
				dashboardDTO.setEndTime(rs.getString("endTime"));
				
				
				dashboardDTOs.add(dashboardDTO);
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getScheduleInfo method : "+e.getMessage());
		
			
		} catch(Exception e){
			log.error("Error occurred in getScheduleInfo method : "+e.getMessage());
			
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return dashboardDTOs;
	}

}
