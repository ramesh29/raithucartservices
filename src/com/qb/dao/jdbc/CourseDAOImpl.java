package com.qb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.qb.dao.ABaseDAO;
import com.qb.dao.ICourseDAO;
import com.qb.dao.query.Query;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.CourseCategoryDTO;
import com.qb.dto.CourseDTO;
import com.qb.dto.CourseMappingDTO;
import com.qb.dto.CourseSubCategoryDTO;
import com.qb.dto.OrgDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.SearchDropListsDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.exception.DataAccessException;
import com.qb.util.ConnectionManager;
import com.tt.schedulerServices.YAKDataPuller;

public class CourseDAOImpl extends ABaseDAO implements ICourseDAO{
	
	private static final Logger log = Logger.getLogger(CourseDAOImpl.class.getName());
	
	@Override
	public ResponseDTO<SearchDropListsDTO> getSearchDropDowns(ResponseDTO<SearchDropListsDTO> response)
			throws DataAccessException {

		List<SearchDropListsDTO> courseCategories = new ArrayList<>();
		
		try {

			SearchDropListsDTO courseCategory = new SearchDropListsDTO();
			courseCategory.setCourseCategoryDTO(getCouseCategories());
			courseCategory.setCities(getCities());
			courseCategory.setInstitutes(getInstitues());
			courseCategories.add(courseCategory);
		}catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to Search DropDowns.Please try again");
			return response;
		} finally{
			
		}		
		response.setData(courseCategories);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}
	

	
	public List<CourseCategoryDTO> getCouseCategories() throws DataAccessException {

		List<CourseCategoryDTO> courseCategories = new ArrayList<>();
		Query query = (Query)getQueries().get("getCourseSuperCategory");
		String sql = query.getQuery();
		CourseCategoryDTO courseCategory = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				courseCategory = new CourseCategoryDTO();
				courseCategory.setCouseCategoryId(rs.getInt(1));
				courseCategory.setCouseCategoryName(rs.getString(2).replaceAll("&", "and").replaceAll("\n", "\\n"));
				courseCategory.setCouseSubCategoryId(getCourseSubCategory(rs.getInt(1)));
				courseCategories.add(courseCategory);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getCourseSuperCategory", e);
			
		} catch(Exception e){
			log.error("Error occurred in getCourseSuperCategory method : "+e.getMessage());
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return courseCategories;
	}
	
	private List<CourseSubCategoryDTO> getCourseSubCategory(int categoryId) throws DataAccessException{

		List<CourseSubCategoryDTO> courseCategories = new ArrayList<>();
		Query query = (Query)getQueries().get("getCourseSubCategory");
		String sql = query.getQuery();
		CourseSubCategoryDTO courseCategory = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, categoryId);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				courseCategory = new CourseSubCategoryDTO();
				courseCategory.setCouseCategoryId(categoryId);
				courseCategory.setCouseSubCategoryId(rs.getInt(1));
				courseCategory.setCourseSubCategoryName(rs.getString(2).replaceAll("&", "and").replaceAll("\n", "\\n"));
				courseCategory.setCourses(getCourses(categoryId,rs.getInt(1)));
				courseCategories.add(courseCategory);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getCourseSubCategory", e);
			
		} catch(Exception e){
			log.error("Error occurred in getCourseSubCategory method : "+e.getMessage());
			
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return courseCategories;
	}
	
	private List<CourseDTO> getCourses(int categoryId,int subCategoryId) throws DataAccessException{

		List<CourseDTO> courseCategories = new ArrayList<>();
		Query query = (Query)getQueries().get("getCourses");
		String sql = query.getQuery();
		CourseDTO courseCategory = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, subCategoryId);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				courseCategory = new CourseDTO();
				courseCategory.setCourseId(rs.getInt(1));
				courseCategory.setCourseName(rs.getString(2).replaceAll("&", "\\&").replaceAll("\n", "\\n"));
				courseCategory.setCouseCategoryId(categoryId);
				courseCategory.setCouseSubCategoryId(subCategoryId);
				courseCategories.add(courseCategory);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getCourses", e);
			
		} catch(Exception e){
			log.error("Error occurred in getCourses method : "+e.getMessage());
			
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return courseCategories;
	}
	
	public List<String> getCities() throws DataAccessException {

		List<String> cities = new ArrayList<>();
		Query query = (Query)getQueries().get("getCities");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				cities.add(rs.getString(1));
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getCities", e);
			
		} catch(Exception e){
			log.error("Error occurred in getCities method : "+e.getMessage());
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return cities;
	}
	
	public  List<OrgDTO>  getInstitues() throws DataAccessException {

		List<OrgDTO>  institutes = new ArrayList<>();
		Query query = (Query)getQueries().get("getInstitutes");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();	
			OrgDTO orgDTO = null;
			while(rs.next()) {
				orgDTO = new OrgDTO();
				orgDTO.setOrgId(rs.getInt(1));
				orgDTO.setOrgName(rs.getString(2));
				institutes.add(orgDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getCities", e);
			
		} catch(Exception e){
			log.error("Error occurred in getCities method : "+e.getMessage());
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return institutes;
	}



	@Override
	public ResponseDTO<CourseBookingDTO> getCourseSchdules(SearchParametersDTO search,int categoryId, int subCategoryId, List<Integer> courseId,
			List<Integer> instituteId, List<String> cities,List<Integer> schdules, String startDate, String endDate,String availablility, List<Integer> courseCode,
			ResponseDTO<CourseBookingDTO> response) throws DataAccessException {

		List<CourseBookingDTO>  courseBookingDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("getCourseSchedules");
		String sql = query.getQuery();
		
		if(startDate != null && startDate.trim().length() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND s.start_date between str_to_date(':startDate','%d-%b-%Y') and str_to_date(':endDate','%d-%b-%Y') &filter_condition");
			sql = sql.replaceAll(":startDate", startDate);
			sql = sql.replaceAll(":endDate", endDate);
		}
		
		if(categoryId > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND cc.course_category_id = :categoryId &filter_condition");
			sql = sql.replaceAll(":categoryId", categoryId+"");
		}
		
		if(subCategoryId > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND sc.course_sub_category_id = :subCategoryId &filter_condition");
			sql = sql.replaceAll(":subCategoryId", subCategoryId+"");
		}
		
		if(courseId != null && courseId.size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND c.course_id in (?) &filter_condition");
			sql = setArray(sql, courseId.toArray(new Integer[courseId.size()]), 1);
		}
		
		if(instituteId != null && instituteId.size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND o.org_id in (?) &filter_condition");
			sql = setArray(sql, instituteId.toArray(new Integer[instituteId.size()]), 1);
		}
		
		if(cities != null && cities.size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND o.city in (?) &filter_condition");
			sql = setList(sql, cities, 1);
			
		}
		
		if(schdules != null && schdules.size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND s.schedule_id in (?) &filter_condition");
			sql = setArray(sql, schdules.toArray(new Integer[schdules.size()]), 1);
		}
		
		if(availablility != null && availablility.trim().length() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND case when ifnull(s.api_batch_id,0)> 0 then s.seats > 0 else ifnull(s.booked_seats,0)+ifnull(s.reserved_seats,0) < s.seats  end &filter_condition");
			sql = sql.replaceAll(":subCategoryId", subCategoryId+"");
		}
		
		if(courseCode != null && courseCode.size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND oc.course_code in (?) &filter_condition");
			sql = setArray(sql, courseCode.toArray(new Integer[courseCode.size()]), 1);
		}
		
		sql = sql.replaceAll("&filter_condition", " ");
		
		
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			if(schdules != null && schdules.size() > 0)
			{
				YAKDataPuller.validateCourseSchedules(con,schdules);
			}
		
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			CourseBookingDTO courseBookingDTO = null;
			while(rs.next()) {
				courseBookingDTO =  new CourseBookingDTO();
				courseBookingDTO.setCollege(rs.getString("college"));
				courseBookingDTO.setCategoryName(rs.getString("catName"));
				courseBookingDTO.setSubCatName(rs.getString("subCatName"));
				courseBookingDTO.setCourseName(rs.getString("courseName"));
				courseBookingDTO.setScheduleId(rs.getInt("scheduleId"));
				courseBookingDTO.setStartDate(rs.getString("startDate"));
				courseBookingDTO.setEndDate(rs.getString("endDate"));
				courseBookingDTO.setSeats(rs.getString("seats"));
				courseBookingDTO.setPrice(rs.getString("price"));
				courseBookingDTO.setTimings(rs.getString("timings"));
				courseBookingDTO.setAvailable(rs.getString("availability"));
				courseBookingDTO.setBookedSeats(rs.getString("bookedSeats"));
				courseBookingDTO.setTotalSeats(rs.getString("totalSeats"));
				courseBookingDTO.setStartTime(rs.getString("startTime"));
				courseBookingDTO.setEndTime(rs.getString("endTime"));
				courseBookingDTO.setDisplay(rs.getString("confirm"));
				courseBookingDTO.setOrgId(rs.getInt("orgId"));
				courseBookingDTO.setAtomProdId(rs.getString("prodId"));
				courseBookingDTO.setAtomProdName(rs.getString("prodName"));
				courseBookingDTO.setApiBatchId(rs.getInt("api_batch_id"));
				courseBookingDTO.setApiCourseId(rs.getInt("api_course_id"));
				courseBookingDTO.setPaymentAgreementType(rs.getString("payment_agreement_type"));
				courseBookingDTO.setPaymentAgreementValue(rs.getString("payment_value"));
				courseBookingDTO.setAtomUsername(rs.getString("atomUserId"));
				courseBookingDTO.setAtomPassword(rs.getString("atomPassword"));
				courseBookingDTO.setRequestHashKey(rs.getString("request_hash_key"));
				courseBookingDTO.setResponseHashKey(rs.getString("response_hash_key"));
				courseBookingDTOs.add(courseBookingDTO);
			}
			
			
			
			
		}catch (SQLException e) {
			log.error("Error occurred in getCourseSchdules method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get course scheules.Please try again");
			return response;
			
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get course scheules.Please try again");
			return response;
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		response.setData(courseBookingDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}



	@Override
	public ResponseDTO<CourseMappingDTO> getNewSearchDropDowns(ResponseDTO<CourseMappingDTO> response)
			throws DataAccessException {

		List<CourseMappingDTO> courseCategories =null; 
		
		try {
			courseCategories = getCourseMapper();
		}catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to Search DropDowns.Please try again");
			return response;
		} finally{
			
		}		
		response.setData(courseCategories);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}
	
	
	public List<CourseMappingDTO> getCourseMapper() throws DataAccessException {

		List<CourseMappingDTO> courseCategories = new ArrayList<>();
		Query query = (Query)getQueries().get("getCourseMapping");
		String sql = query.getQuery();
		CourseMappingDTO courseCategory = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				courseCategory = new CourseMappingDTO();
				courseCategory.setCategory(rs.getString(1).replaceAll("&", "and").replaceAll("\n", "\\n"));
				courseCategory.setSubCategory(rs.getString(2).replaceAll("&", "and").replaceAll("\n", "\\n"));
				courseCategory.setCourseName(rs.getString(3).replaceAll("&", "and").replaceAll("\n", "\\n"));
				courseCategory.setCourseCode(rs.getInt(4));
				courseCategory.setOrgId(rs.getInt(5));
				courseCategory.setOrgName(rs.getString(6));
				courseCategory.setCity(rs.getString(7));
				courseCategories.add(courseCategory);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getCourseSuperCategory", e);
			
		} catch(Exception e){
			log.error("Error occurred in getCourseSuperCategory method : "+e.getMessage());
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return courseCategories;
	}

}
