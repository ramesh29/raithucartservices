package com.qb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.qb.dao.ABaseDAO;
import com.qb.dao.ILoginDAO;
import com.qb.dao.query.Query;
import com.qb.dto.PasswordDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ShopUserDTO;
import com.qb.dto.UserDTO;
import com.qb.exception.DataAccessException;
import com.qb.exception.ErrorCode;
import com.qb.util.ConnectionManager;
import com.qb.util.IPaymentGatewayURLS;
import com.qb.util.QuickBookUtil;
import com.tt.util.DateUtils;

public class LoginDAOImpl extends ABaseDAO implements ILoginDAO {
	
	private static final Logger log = Logger.getLogger(LoginDAOImpl.class.getName());
	
	@Override
	public UserDTO checkUserNameExists(String userName, int orgId) throws DataAccessException {

		Query query = (Query)getQueries().get("checkUserExistence");
		String sql = query.getQuery();
		UserDTO userDTO = new UserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userName);
			//	pstmt.setString(2, orgName);

			rs = pstmt.executeQuery();				
			if(rs.next()) {
				userDTO.setUserId(rs.getInt("user_id"));
				userDTO.setSuccess(false);
				userDTO.setMessege("User already exists.Please register with new username");
				
			}else
			{
				userDTO.setSuccess(true);
				
			}
			return userDTO;

		}catch (SQLException e) {
			log.error("Unable to execute the query - getUserDetails", e);
			throw new DataAccessException(ErrorCode.ERR_EXECUTE_QUERY, e);
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			e.printStackTrace();
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			

	}

	@Override
	public UserDTO registerUser(UserDTO userdto, String userType) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("register");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, userdto.getUserName());
			pstmt.setObject(2, (userdto.getPassword() == null ? null : QuickBookUtil.encrypt(userdto.getPassword())));
			pstmt.setObject(3, userdto.getRoleName());
			pstmt.setObject(4, userdto.getOrgId());
			if(userdto.getDateOfBirth() != null && userdto.getDateOfBirth().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getDateOfBirth(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(5, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(5, null);
			}
			pstmt.setObject(6, userdto.getRank());
			pstmt.setObject(7, userdto.getIndosNo());
			pstmt.setObject(8, userdto.getCdcNo());
			if(userdto.getCdcIssueDate() != null && userdto.getCdcIssueDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getCdcIssueDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(9, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(9, null);
			}
			pstmt.setObject(10, userdto.getCocNo());
			pstmt.setObject(11, userdto.getGrade());
			pstmt.setObject(12, userdto.getCocIssuingAuthority());
			if(userdto.getCocIssuingDate() != null && userdto.getCocIssuingDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getCocIssuingDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(13, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(13, null);
			}
			pstmt.setObject(14, userdto.getPassportNo());
			pstmt.setObject(15, userdto.getPassportPlaceOfIssue());
			if(userdto.getPassportIssueDate() != null && userdto.getPassportIssueDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getPassportIssueDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(16, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(16, null);
			}
			pstmt.setObject(17, userdto.getAddress());
			pstmt.setObject(18, userdto.getCity());
			pstmt.setObject(19, userdto.getState());
			pstmt.setObject(20, userdto.getZip());
			pstmt.setObject(21, userdto.getCountry());
			pstmt.setObject(22, userdto.getNationality());
			pstmt.setObject(23, userdto.getEmail());
			pstmt.setObject(24, userdto.getPhone());
			pstmt.setObject(25, "N");
			pstmt.setObject(26, userdto.getFirstName());
			pstmt.setObject(27, userdto.getLastName());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					userdto.setUserId(rs.getInt(1));
					userdto.setSuccess(true);
					userdto.setMessege("User Registered successfully.Please reset password");
					return userdto;
				}
			}else
			{
				userdto.setSuccess(false);
				userdto.setMessege("User Registered failed.Please try again");
				return userdto;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			userdto.setSuccess(false);
			userdto.setMessege("User Registered failed.Please try again");
			return userdto;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return null;
	}

	@Override
	public UserDTO CreateorUpdatePassword(PasswordDTO passwordDTO) throws DataAccessException {

		Query query = (Query)getQueries().get("updatePassword");
		String sql = query.getQuery();
		UserDTO userDTO = new UserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, QuickBookUtil.encrypt(passwordDTO.getUserPassword()));
			pstmt.setInt(2, passwordDTO.getUserId());
			int count =  pstmt.executeUpdate();
			if(count > 0)
			{
				userDTO.setSuccess(true);
				userDTO.setMessege("Password reset successful.Please Login");
			}else
			{
				userDTO.setSuccess(false);
				userDTO.setMessege("Password reset failed.");
			}
			
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getUserDetails", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("Password reset failed.");
			
			
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("Password reset failed.");
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return userDTO;
	}

	@Override
	public UserDTO authenticate(String userName, String password) throws DataAccessException 
	{

		Query query = (Query)getQueries().get("authenticate");
		String sql = query.getQuery();
		UserDTO userDTO = new UserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userName);
			pstmt.setString(2, QuickBookUtil.encrypt(password));

			rs = pstmt.executeQuery();				
			if(rs.next()) {
				userDTO.setUserId(rs.getInt("userId"));
				userDTO.setUserName(rs.getString("userName"));
				userDTO.setRoleId(rs.getInt("roleId"));
				userDTO.setRoleName(rs.getString("roleName"));
				userDTO.setSuccess(true);
				userDTO.setOrgId(rs.getInt("orgId"));
				userDTO.setMessege("Login Successful.");
				
			}else
			{
				userDTO.setSuccess(false);
				userDTO.setMessege("Invalid username/password.Please try again");
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - getUserDetails", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("Invalid username/password.Please try again");
			throw new DataAccessException(ErrorCode.ERR_EXECUTE_QUERY, e);
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("Invalid username/password.Please try again");
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	
		
		return userDTO;

	}

	@Override
	public UserDTO updateUser(UserDTO userdto, String userType) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateUser");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			
			if(userdto.getDateOfBirth() != null && userdto.getDateOfBirth().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getDateOfBirth(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(1, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(1, userdto.getOrgId());
			}
			pstmt.setObject(2, userdto.getRank());
			pstmt.setObject(3, userdto.getIndosNo());
			pstmt.setObject(4, userdto.getCdcNo());
			if(userdto.getCdcIssueDate() != null && userdto.getCdcIssueDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getCdcIssueDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(5, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(5, null);
			}
			pstmt.setObject(6, userdto.getCocNo());
			pstmt.setObject(7, userdto.getGrade());
			pstmt.setObject(8, userdto.getCocIssuingAuthority());
			if(userdto.getCocIssuingDate() != null && userdto.getCocIssuingDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getCocIssuingDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(9, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(9, null);
			}
			pstmt.setObject(10, userdto.getPassportNo());
			pstmt.setObject(11, userdto.getPassportPlaceOfIssue());
			if(userdto.getPassportIssueDate() != null && userdto.getPassportIssueDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(userdto.getPassportIssueDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(12, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(12, null);
			}
			pstmt.setObject(13, userdto.getAddress());
			pstmt.setObject(14, userdto.getCity());
			pstmt.setObject(15, userdto.getState());
			pstmt.setObject(16, userdto.getZip());
			pstmt.setObject(17, userdto.getCountry());
			pstmt.setObject(18, userdto.getNationality());
			pstmt.setObject(19, userdto.getEmail());
			pstmt.setObject(20, userdto.getPhone());
			pstmt.setObject(21, userdto.getFirstName());
			pstmt.setObject(22, userdto.getLastName());
			pstmt.setObject(23, userdto.getUserId());
			
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				
				userdto.setSuccess(true);
				userdto.setMessege("User details updated successfully");
				return userdto;
			}else
			{
				userdto.setSuccess(false);
				userdto.setMessege("User details update failed.Please try again");
				return userdto;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			userdto.setSuccess(false);
			userdto.setMessege("User details update failed.Please try again");
			return userdto;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		
	}

	@Override
	public UserDTO getUser(int userId,String indosNo) throws DataAccessException {

		Query query = (Query)getQueries().get("getUser");
		String sql = query.getQuery();
		if(userId > 0 )
		{
			sql = sql.replace("&filterCond", " user_id = ?");
		}else
		{
			sql = sql.replace("&filterCond", " indos_no = ?");
		}
		UserDTO userDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, (userId > 0 ? userId : indosNo));
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				userDTO = new UserDTO();
				userDTO.setUserName(rs.getString("username"));
				userDTO.setFirstName(rs.getString("first_name"));
				userDTO.setLastName(rs.getString("last_name"));
				userDTO.setDateOfBirth(rs.getString("date_of_birth"));
				userDTO.setRank(rs.getString("rank"));
				userDTO.setIndosNo(rs.getString("indos_no"));
				userDTO.setCdcNo(rs.getString("cdc_no"));
				userDTO.setCdcIssueDate(rs.getString("cdc_issue_date"));
				userDTO.setCocNo(rs.getString("coc_no"));
				userDTO.setGrade(rs.getString("grade"));
				userDTO.setCocIssuingAuthority(rs.getString("coc_issuing_authority"));
				userDTO.setCocIssuingDate(rs.getString("coc_issue_date"));
				userDTO.setPassportNo(rs.getString("passport_no"));
				userDTO.setPassportPlaceOfIssue(rs.getString("passport_place_of_issue"));
				userDTO.setPassportIssueDate(rs.getString("passport_issue_date"));
				userDTO.setAddress(rs.getString("address"));
				userDTO.setState(rs.getString("state"));
				userDTO.setCity(rs.getString("city"));
				userDTO.setZip(rs.getString("zip"));
				userDTO.setCountry(rs.getString("country"));
				userDTO.setNationality(rs.getString("nationality"));
				userDTO.setEmail(rs.getString("email"));
				userDTO.setPhone(rs.getString("phone"));
				userDTO.setUserId(rs.getInt("user_id"));
				userDTO.setPassword(QuickBookUtil.decrypt(rs.getString("password")));
			}
			

		}catch (SQLException e) {
			log.error("Unable to execute the query - getUser", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("Unable to fetch user details.Please try again");
			return userDTO;
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("Unable to fetch user details.Please try again");
			return userDTO;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		userDTO.setSuccess(true);
		return userDTO;
	}

	@Override
	public UserDTO getAdminUser(int orgId) throws DataAccessException {

		Query query = (Query)getQueries().get("getAdminUser");
		String sql = query.getQuery();
		UserDTO userDTO = new UserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, orgId);
			rs = pstmt.executeQuery();				
			if(rs.next()) {
				
				userDTO.setUserName(rs.getString("username"));
				userDTO.setUserId(rs.getInt("user_id"));
				userDTO.setPassword(QuickBookUtil.decrypt(rs.getString("password")));
			}
			

		}catch (SQLException e) {
			log.error("Unable to execute the query - getUser", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("Unable to fetch user details.Please try again");
			return userDTO;
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("Unable to fetch user details.Please try again");
			return userDTO;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		userDTO.setSuccess(true);
		return userDTO;
	}

	@Override
	public UserDTO updateAdmin(UserDTO userdto) throws DataAccessException {

		Query query = (Query)getQueries().get("updateAdmin");
		String sql = query.getQuery();
		UserDTO userDTO = new UserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, userdto.getUserName());
			pstmt.setString(2, QuickBookUtil.encrypt(userdto.getPassword()));
			pstmt.setInt(3, userdto.getUserId());
			int count =  pstmt.executeUpdate();
			if(count > 0)
			{
				userDTO.setSuccess(true);
				userDTO.setMessege("User updated successfully");
			}else
			{
				userDTO.setSuccess(false);
				userDTO.setMessege("User update failed");
			}
			
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - updateAdmin", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("User update failed");
			
			
		} catch(Exception e){
			log.error("Error occurred in updateAdmin method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("User update failed");
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return userDTO;
	}

	@Override
	public ResponseDTO<UserDTO> getUsers(ResponseDTO<UserDTO> response) throws DataAccessException {

		List<UserDTO> users = new ArrayList<>();
		Query query = (Query)getQueries().get("getUsers");
		String sql = query.getQuery();
		UserDTO userDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				userDTO = new UserDTO();
				userDTO.setUserName(rs.getString("username"));
				userDTO.setFirstName(rs.getString("first_name"));
				userDTO.setLastName(rs.getString("last_name"));
				userDTO.setDateOfBirth(rs.getString("date_of_birth"));
				userDTO.setRank(rs.getString("rank"));
				userDTO.setIndosNo(rs.getString("indos_no"));
				userDTO.setCdcNo(rs.getString("cdc_no"));
				userDTO.setCdcIssueDate(rs.getString("cdc_issue_date"));
				userDTO.setCocNo(rs.getString("coc_no"));
				userDTO.setGrade(rs.getString("grade"));
				userDTO.setCocIssuingAuthority(rs.getString("coc_issuing_authority"));
				userDTO.setCocIssuingDate(rs.getString("coc_issue_date"));
				userDTO.setPassportNo(rs.getString("passport_no"));
				userDTO.setPassportPlaceOfIssue(rs.getString("passport_place_of_issue"));
				userDTO.setPassportIssueDate(rs.getString("passport_issue_date"));
				userDTO.setAddress(rs.getString("address"));
				userDTO.setState(rs.getString("state"));
				userDTO.setCity(rs.getString("city"));
				userDTO.setZip(rs.getString("zip"));
				userDTO.setCountry(rs.getString("country"));
				userDTO.setNationality(rs.getString("nationality"));
				userDTO.setEmail(rs.getString("email"));
				userDTO.setPhone(rs.getString("phone"));
				userDTO.setUserId(rs.getInt("user_id"));
				userDTO.setBookings(rs.getInt("bookings"));
				userDTO.setLastBookingDate(rs.getString("lastBookingDate"));
				users.add(userDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getUsers", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getUsers.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getUsers method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getUsers.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(users);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ShopUserDTO checkShopUserExists(String mobileNumber) throws DataAccessException {

		Query query = (Query)getQueries().get("checkShopUserExistence");
		String sql = query.getQuery();
		ShopUserDTO userDTO = new ShopUserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, mobileNumber);
			//	pstmt.setString(2, orgName);

			rs = pstmt.executeQuery();				
			if(rs.next()) {
				userDTO.setUserId(rs.getInt("user_id"));
				userDTO.setSuccess(false);
				userDTO.setMessege("User already registered with this Mobile Number.Please register with new Mobile Number");
				
			}else
			{
				userDTO.setSuccess(true);
				
			}
			return userDTO;

		}catch (SQLException e) {
			log.error("Unable to execute the query - getUserDetails", e);
			throw new DataAccessException(ErrorCode.ERR_EXECUTE_QUERY, e);
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			e.printStackTrace();
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			

	}

	@Override
	public ShopUserDTO registerShopUser(ShopUserDTO userdto) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("registerShop");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, userdto.getMobileNumber());
			pstmt.setObject(2, (userdto.getPassword() == null ? null : QuickBookUtil.encrypt(userdto.getPassword())));
			pstmt.setObject(3, userdto.getShopName());
			if(userdto.getPlan().contains("Month"))
			{
				pstmt.setObject(4, 1);
			}else if(userdto.getPlan().contains("Annual"))
			{
				pstmt.setObject(4, 12);
			}
			
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					int userId = rs.getInt(1);
					userdto.setUserId(userId);
					userdto.setSuccess(true);
					userdto.setMessege("Shop Registered successfully.Please Login");
					//Copy existing items into shop
					//copyItems(userId,con);
					return userdto;
				}
			}else
			{
				userdto.setSuccess(false);
				userdto.setMessege("Shop Registered failed.Please contact support@mymarinecourse.com");
				return userdto;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			userdto.setSuccess(false);
			userdto.setMessege("User Registered failed.Please contact support@mymarinecourse.com");
			return userdto;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return null;
	}
	
	public void copyItems(int userId,Connection con)
	{

		
		PreparedStatement pstmt = null;			
		Query query = (Query)getQueries().get("copyItems");
		String sql = query.getQuery();
		try{
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, userId);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} 
	}

	@Override
	public ShopUserDTO shopLogin(String mobileNumber, String password) throws DataAccessException {

		Query query = (Query)getQueries().get("shopLogin");
		String sql = query.getQuery();
		ShopUserDTO userDTO = new ShopUserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, mobileNumber);
			pstmt.setString(2, QuickBookUtil.encrypt(password));

			rs = pstmt.executeQuery();				
			if(rs.next()) {
				userDTO.setUserId(rs.getInt("userId"));
				userDTO.setMobileNumber(rs.getString("mobileNumber"));
				userDTO.setShopName(rs.getString("shopName"));
				userDTO.setAddress(rs.getString("address"));
				userDTO.setCity(rs.getString("city"));
				userDTO.setState(rs.getString("state"));
				userDTO.setZip(rs.getString("zip"));
				userDTO.setCountry(rs.getString("country"));
				userDTO.setDiscount(rs.getString("discount"));
				userDTO.setShopLogo(rs.getString("shop_logo"));
				userDTO.setShowDetails(rs.getString("show_details"));
				userDTO.setImageName(rs.getString("image_name"));
				userDTO.setGst(rs.getString("gst"));
				userDTO.setCurrentStockValue(rs.getString("currentStockValue"));
				userDTO.setCurrentSoldValue(rs.getString("currentSoldValue"));
				userDTO.setTotalSoldValue(rs.getString("totalSoldValue"));
				userDTO.setTotalCreditValue(rs.getString("totalCredit"));
				userDTO.setProfit(rs.getString("profitForecast"));
				userDTO.setTodaySoldValue(rs.getString("todaySoldValue"));
				userDTO.setSuccess(true);
				userDTO.setMessege("Login Successful.");
				
			}else
			{
				userDTO.setSuccess(false);
				userDTO.setMessege("Invalid Mobile Number/password.Please try again");
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - shopLogin", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("Invalid Mobile Number/password.Please try again");
			throw new DataAccessException(ErrorCode.ERR_EXECUTE_QUERY, e);
		} catch(Exception e){
			log.error("Error occurred in shopLogin method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("Invalid Mobile Number/password.Please try again");
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	
		return userDTO;

	}

	@Override
	public ShopUserDTO updateShop(ShopUserDTO userdto) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateShop");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, userdto.getShopName());
			pstmt.setObject(2, userdto.getAddress());
			pstmt.setObject(3, userdto.getCity());
			pstmt.setObject(4, userdto.getState());
			pstmt.setObject(5, userdto.getZip());
			pstmt.setObject(6, userdto.getCountry());
			pstmt.setObject(7, userdto.getDiscount());
			
			pstmt.setObject(8, userdto.getShopLogo());
			pstmt.setObject(9, userdto.getShowDetails());
			pstmt.setObject(10, userdto.getImageName());
			pstmt.setObject(11, userdto.getGst());
			
			
			
			pstmt.setObject(12, userdto.getUserId());
			pstmt.setObject(13, userdto.getUserId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				userdto.setUserId(userdto.getUserId());
				userdto.setSuccess(true);
				userdto.setMessege("Shop details updated successfully.");
				
				return userdto;
			}else
			{
				userdto.setSuccess(false);
				userdto.setMessege("Shop details update failed.Please try again");
				return userdto;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			userdto.setSuccess(false);
			userdto.setMessege("User Registered failed.Please try again");
			return userdto;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		
	}

	
	
	@Override
	public ShopUserDTO updateShopDeviceUUID(ShopUserDTO userdto) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateUUID");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, userdto.getUuid());
			pstmt.setObject(2, userdto.getUserId());
			pstmt.setObject(3, userdto.getUserId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				userdto.setUserId(userdto.getUserId());
				userdto.setSuccess(true);
				userdto.setMessege("Device uuid updated");
				//System.out.println("updated uuid");
				return userdto;
			}else
			{
				userdto.setSuccess(false);
				userdto.setMessege("Device uuid update failed");
				return userdto;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			userdto.setSuccess(false);
			userdto.setMessege("Device uuid update failed");
			return userdto;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public ShopUserDTO renewShopUser(ShopUserDTO userdto) throws DataAccessException {		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("renewAccess");
		/*Query query = (Query)getQueries().get("updateShop");*/
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			if(userdto.getPlan().contains("Month"))
			{
				pstmt.setObject(1, 1);
			}else if(userdto.getPlan().contains("Annual"))
			{
				pstmt.setObject(1, 12);
			}
			pstmt.setObject(2, userdto.getUserId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				userdto.setUserId(userdto.getUserId());
				userdto.setSuccess(true);
				userdto.setMessege("Shop access renewal is successful.Please Login");
				return userdto;
			}else
			{
				userdto.setSuccess(false);
				userdto.setMessege("Shop access renewal failed.Please contact support@mymarinecourse.com");
				return userdto;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			userdto.setSuccess(false);
			userdto.setMessege("Shop access renewal failed.Please contact support@mymarinecourse.com");
			return userdto;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		
	
	
	
	}
	
	@Override
	public ShopUserDTO updatePassword(PasswordDTO passwordDTO) throws DataAccessException {

		Query query = (Query)getQueries().get("updateShopPassword");
		String sql = query.getQuery();
		ShopUserDTO userDTO = new ShopUserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, QuickBookUtil.encrypt(passwordDTO.getPassword()));
			/* pstmt.setInt(2, passwordDTO.getUserId()); */
			pstmt.setString(2, passwordDTO.getMobileNumber());
			int count =  pstmt.executeUpdate();
			if(count > 0)
			{
				userDTO.setSuccess(true);
				userDTO.setMessege("Password reset successful.Please Login");
			}else
			{
				userDTO.setSuccess(false);
				userDTO.setMessege("Password reset failed.");
			}
			
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getUserDetails", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("Password reset failed.");
			
			
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("Password reset failed.");
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return userDTO;
	}

	@Override
	public ShopUserDTO confirmPassword(PasswordDTO passwordDTO) throws DataAccessException {
		
		Query query = (Query)getQueries().get("confirmShopPassword");
		String sql = query.getQuery();
		ShopUserDTO userDTO = new ShopUserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, passwordDTO.getMobileNumber());
			rs = pstmt.executeQuery();				
			if(rs.next()) {
				userDTO.setUserId(rs.getInt("user_id"));
				userDTO.setSuccess(false);
				System.out.println(QuickBookUtil.decrypt(rs.getString("password")));
				if(QuickBookUtil.decrypt(rs.getString("password")).equals(passwordDTO.getExistingPassword())){
					System.out.println("true");
					userDTO.setSuccess(true);
				}
				else 
				{
					userDTO.setSuccess(false);
					userDTO.setMessege("Existing Password is wrong");
				}
			}else
			{
				userDTO.setSuccess(false);
				userDTO.setMessege("Existing Password is wrong");
			}
			return userDTO;

		}catch (SQLException e) {
			log.error("Unable to execute the query - getUserDetails", e);
			throw new DataAccessException(ErrorCode.ERR_EXECUTE_QUERY, e);
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			e.printStackTrace();
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		
		
	}

}
