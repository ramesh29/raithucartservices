package com.qb.dao.jdbc;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.qb.dao.ABaseDAO;
import com.qb.dao.IShopDAO;
import com.qb.dao.query.Query;
import com.qb.dto.CCTransactionDTO;
import com.qb.dto.CreditCustomerDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ShopItemDTO;
import com.qb.dto.ShopTransactionDTO;
import com.qb.dto.ShopUserDTO;
import com.qb.exception.DataAccessException;
import com.qb.exception.ErrorCode;
import com.qb.util.ConnectionManager;
import com.qb.util.IPaymentGatewayURLS;
import com.qb.util.SBookPDFGenerator;

public class ShopDAOImpl extends ABaseDAO implements IShopDAO{

	private static final Logger log = Logger.getLogger(ShopDAOImpl.class.getName());
	
	@Override
	public ResponseDTO<ShopItemDTO>  getShopItems(int userId,ResponseDTO<ShopItemDTO> response) throws DataAccessException {

		List<ShopItemDTO> shopItems = new ArrayList<>();
		Query query = (Query)getQueries().get("getShopItems");
		String sql = query.getQuery();
		ShopItemDTO shopItemDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, userId);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				shopItemDTO = new ShopItemDTO();
				shopItemDTO.setItemId(rs.getInt("item_id"));
				shopItemDTO.setItemName(rs.getString("item_name"));
				shopItemDTO.setItemDesc(rs.getString("item_description"));
				shopItemDTO.setItemUOM(rs.getString("uom"));
				shopItemDTO.setItemPurchaseCost(rs.getDouble("item_standard_cost"));
				shopItemDTO.setItemSoldCost(rs.getDouble("item_sold_cost"));
				shopItemDTO.setTotalQuantity(rs.getDouble("total_qty"));
				shopItemDTO.setTotalAmount(rs.getDouble("total_amount"));
				shopItemDTO.setUserId(userId);
				shopItemDTO.setNewPurchaseCost(rs.getDouble("item_standard_cost"));
				shopItemDTO.setNewSoldCost(rs.getDouble("item_sold_cost"));
				shopItemDTO.setLowQuantityValue(rs.getInt("low_quantity_value"));
				shopItemDTO.setImageName(StringUtils.isNotBlank(rs.getString("image_name")) ? rs.getString("image_name") : "no-image-available.jpg");
				shopItemDTO.setUpcCode(rs.getString("upc_code"));
				shopItems.add(shopItemDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getShopItems", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getShopItems.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getShopItems method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getShopItems.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(shopItems);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<ShopItemDTO> addNewItem(ResponseDTO<ShopItemDTO> response, ShopItemDTO shopItemDTO) throws  DataAccessException {
		
		Query query = (Query)getQueries().get("addNewItem");
		String sql = query.getQuery();
		List<ShopItemDTO> shopItemDTOs = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, shopItemDTO.getItemName());
			pstmt.setObject(2, shopItemDTO.getItemDesc());
			pstmt.setObject(3, shopItemDTO.getItemUOM());
			pstmt.setObject(4, shopItemDTO.getItemPurchaseCost());
			pstmt.setObject(5, shopItemDTO.getItemSoldCost());
			pstmt.setObject(6, shopItemDTO.getTotalQuantity());
			pstmt.setObject(7, shopItemDTO.getImageName());
			pstmt.setObject(8, shopItemDTO.getLowQuantityValue());
			pstmt.setObject(9, shopItemDTO.getGstValue());
			pstmt.setObject(10,shopItemDTO.getUpcCode());
			pstmt.setObject(11,shopItemDTO.getUserId());
			
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					shopItemDTO.setItemId(rs.getInt(1));
					shopItemDTOs.add(shopItemDTO);
				}
			}
			else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to add Item.Please try again");
				return response;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to add item.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(shopItemDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}
	
	@Override
	public int updateShopItemCosts(List<ShopItemDTO> shopItemDTOs) throws DataAccessException {	
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateShopItemCosts");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			for(ShopItemDTO shopItem : shopItemDTOs)
			{
				pstmt.setObject(1, shopItem.getItemName());
				pstmt.setObject(2, shopItem.getItemDesc());
				pstmt.setObject(3, shopItem.getItemUOM());
				pstmt.setObject(4, shopItem.getNewPurchaseCost());
				pstmt.setObject(5, shopItem.getNewSoldCost());
				pstmt.setObject(6, shopItem.getNewPurchaseCost());
				pstmt.setObject(7, shopItem.getLowQuantityValue());
				pstmt.setObject(8, shopItem.getUpcCode());
				pstmt.setObject(9, shopItem.getItemId());
				pstmt.addBatch();
			}
			
			int count[] = pstmt.executeBatch();
			
			if(count.length > 0)
			{
				query = (Query)getQueries().get("insertCostUpdates");
				sql = query.getQuery();
				pstmt.close();
				pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				for(ShopItemDTO shopItem : shopItemDTOs)
				{
					pstmt.setObject(1, shopItem.getItemId());
					pstmt.setObject(2, shopItem.getUserId());
					pstmt.setObject(3, shopItem.getItemPurchaseCost());
					pstmt.setObject(4, shopItem.getItemSoldCost());
					pstmt.setObject(5, shopItem.getNewPurchaseCost());
					pstmt.setObject(6, shopItem.getNewSoldCost());
					pstmt.addBatch();
				}
				count = pstmt.executeBatch();
				return count.length;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
	
		
	}

	
	@Override
	public int updateShopItemCostsWithImage(ShopItemDTO shopItemDTOs) throws DataAccessException {	
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateShopItemCostsWithImage");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			/*for(ShopItemDTO shopItem : shopItemDTOs)
			{*/
				pstmt.setObject(1, shopItemDTOs.getItemName());
				pstmt.setObject(2, shopItemDTOs.getItemDesc());
				pstmt.setObject(3, shopItemDTOs.getItemUOM());
				pstmt.setObject(4, shopItemDTOs.getNewPurchaseCost());
				pstmt.setObject(5, shopItemDTOs.getNewSoldCost());
				pstmt.setObject(6, shopItemDTOs.getNewPurchaseCost());
				pstmt.setObject(7, shopItemDTOs.getLowQuantityValue());
				pstmt.setObject(8, shopItemDTOs.getUpcCode());
				pstmt.setObject(9, shopItemDTOs.getImageName());
				pstmt.setObject(10, shopItemDTOs.getItemId());
				
				pstmt.addBatch();
			//}
			
			int count[] = pstmt.executeBatch();
			
			if(count.length > 0)
			{
				query = (Query)getQueries().get("insertCostUpdates");
				sql = query.getQuery();
				pstmt.close();
				pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				/*for(ShopItemDTO shopItem : shopItemDTOs)
				{*/
					pstmt.setObject(1, shopItemDTOs.getItemId());
					pstmt.setObject(2, shopItemDTOs.getUserId());
					pstmt.setObject(3, shopItemDTOs.getItemPurchaseCost());
					pstmt.setObject(4, shopItemDTOs.getItemSoldCost());
					pstmt.setObject(5, shopItemDTOs.getNewPurchaseCost());
					pstmt.setObject(6, shopItemDTOs.getNewSoldCost());
					pstmt.addBatch();
				//}
				count = pstmt.executeBatch();
				return count.length;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
	}
	
	
	@Override
	public int updateTransaction(List<ShopTransactionDTO> shopItemDTOs, int customerTransactionId) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("insertInventoryTransaction");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			for(ShopTransactionDTO shopItem : shopItemDTOs)
			{
				if(shopItem.getId() == 0)
				//if(shopItem.getCustomerTransactionId() > 0)
				{
					pstmt.setObject(1, shopItem.getTransactionType());
					pstmt.setObject(2, shopItem.getItemId());
					pstmt.setObject(3, shopItem.getUserId());
					pstmt.setObject(4, shopItem.getExistingQty());
					pstmt.setObject(5, shopItem.getTransactionQty());
					if("Buy".equalsIgnoreCase(shopItem.getTransactionType()))
					{
						pstmt.setObject(6, shopItem.getExistingQty()+shopItem.getTransactionQty());
					}else if("Sold".equalsIgnoreCase(shopItem.getTransactionType()))
					{
						pstmt.setObject(6, shopItem.getExistingQty()-shopItem.getTransactionQty());
						
						
					}else if("Adjustment".equalsIgnoreCase(shopItem.getTransactionType()))
					{
						pstmt.setObject(6, -1* shopItem.getTotalAmount());
					}
					pstmt.setObject(7, shopItem.getNewPurchaseCost());
					pstmt.setObject(8, shopItem.getNewSoldCost());
					pstmt.setObject(9, shopItem.getUserId());
					pstmt.setObject(10, customerTransactionId);
					pstmt.addBatch();	
				}
			}
			
			int count[] = pstmt.executeBatch();
			
			
			if(count.length > 0)
			{
				query = (Query)getQueries().get("updateShopItemQuantity");
				sql = query.getQuery();
				pstmt.close();
				pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				for(ShopTransactionDTO shopItem : shopItemDTOs)
				{
					if("Buy".equalsIgnoreCase(shopItem.getTransactionType()))
					{
						pstmt.setObject(1, shopItem.getExistingQty()+shopItem.getTransactionQty());
						pstmt.setObject(2, shopItem.getNewPurchaseCost()*shopItem.getTransactionQty());
					}else if("Sold".equalsIgnoreCase(shopItem.getTransactionType()))
					{
						double transactionQty = 0.0;
						if(shopItem.getTransactionQty() > shopItem.getPreviousQuantity()){
							transactionQty = shopItem.getExistingQty()-(shopItem.getTransactionQty()-shopItem.getPreviousQuantity());
							//pstmt.setObject(1, shopItem.getExistingQty()-(shopItem.getTransactionQty()-shopItem.getPreviousQuantity()));
						}else if(shopItem.getTransactionQty() < shopItem.getPreviousQuantity()){
							//pstmt.setObject(1, shopItem.getExistingQty()-(shopItem.getPreviousQuantity()-shopItem.getTransactionQty()));
							transactionQty = shopItem.getExistingQty()+(shopItem.getPreviousQuantity()-shopItem.getTransactionQty());
						}else if(shopItem.getTransactionQty() == shopItem.getPreviousQuantity()){
							transactionQty=shopItem.getExistingQty();
						}else{
							transactionQty=shopItem.getExistingQty()-shopItem.getTransactionQty();
						}
						pstmt.setObject(1, transactionQty);
						pstmt.setObject(2, -1*(shopItem.getNewPurchaseCost()*transactionQty));
					}else if("Adjustment".equalsIgnoreCase(shopItem.getTransactionType()))
					{
						pstmt.setObject(1, shopItem.getTransactionQty());
						if(shopItem.getExistingQty() > shopItem.getTransactionQty())
						{
							pstmt.setObject(2, shopItem.getNewPurchaseCost()*shopItem.getTransactionQty());
						}else if(shopItem.getExistingQty() < shopItem.getTransactionQty()){
							pstmt.setObject(2, -1*shopItem.getNewPurchaseCost()*shopItem.getTransactionQty());	
						}else
						{
							pstmt.setObject(2, 0);
						}
						
					}
					
					pstmt.setObject(3, shopItem.getItemId());
					pstmt.addBatch();
				}
				count = pstmt.executeBatch();
				
				
				
				
				
				
				return count.length;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
	
		
	}
	
	@Override
	public ShopUserDTO shopLogin(int userId) throws DataAccessException {

		Query query = (Query)getQueries().get("shopLogin");
		String sql = query.getQuery();
		ShopUserDTO userDTO = new ShopUserDTO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, userId);
			
			rs = pstmt.executeQuery();				
			if(rs.next()) {
				userDTO.setUserId(rs.getInt("userId"));
				userDTO.setMobileNumber(rs.getString("mobileNumber"));
				userDTO.setShopName(rs.getString("shopName"));
				userDTO.setDiscount(rs.getString("discount"));
				userDTO.setAddress(rs.getString("address"));
				userDTO.setCity(rs.getString("city"));
				userDTO.setState(rs.getString("state"));
				userDTO.setZip(rs.getString("zip"));
				userDTO.setCountry(rs.getString("country"));
				userDTO.setImageName(rs.getString("image_name"));
				userDTO.setCurrentStockValue(rs.getString("currentStockValue"));
				userDTO.setCurrentSoldValue(rs.getString("currentSoldValue"));
				userDTO.setTotalSoldValue(rs.getString("totalSoldValue"));
				userDTO.setTotalCreditValue(rs.getString("totalCredit"));
				userDTO.setProfit(rs.getString("profitForecast"));
				userDTO.setUuid(rs.getString("uuid"));
				userDTO.setTodaySoldValue(rs.getString("todaySoldValue"));
				userDTO.setSuccess(true);
				userDTO.setMessege("Login Successful.");
				
			}else
			{
				userDTO.setSuccess(false);
				userDTO.setMessege("Invalid Mobile Number/password.Please try again");
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - shopLogin", e);
			userDTO.setSuccess(false);
			userDTO.setMessege("Invalid Mobile Number/password.Please try again");
			throw new DataAccessException(ErrorCode.ERR_EXECUTE_QUERY, e);
		} catch(Exception e){
			log.error("Error occurred in shopLogin method : "+e.getMessage());
			userDTO.setSuccess(false);
			userDTO.setMessege("Invalid Mobile Number/password.Please try again");
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	
		
		return userDTO;

	}

	@Override
	public ResponseDTO<ShopTransactionDTO> getInventoryTransactions(int userId, String startDate, String endDate, List<String> transactionType,
			List<Integer> itemIds, ResponseDTO<ShopTransactionDTO> response) throws DataAccessException {

		List<ShopTransactionDTO> transactions = new ArrayList<>();
		Query query = (Query)getQueries().get("inventoryTransactions");
		String sql = query.getQuery();
		
		
		if(startDate != null && startDate.trim().length() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND sub.date1 between ':startDate' and ':endDate' &filter_condition");
			sql = sql.replaceAll(":startDate", startDate);
			sql = sql.replaceAll(":endDate", endDate);
		}
		
		if(itemIds != null && itemIds.size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND sub.item_id in (?) &filter_condition");
			sql = setArray(sql, itemIds.toArray(new Integer[itemIds.size()]), 2);
		}
		
		if(transactionType != null && transactionType.size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND sub.a in (?) &filter_condition");
			sql = setList(sql, transactionType, 2);
		}
		sql = sql.replaceAll("&filter_condition", " ");
		
		ShopTransactionDTO transactionDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, userId);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				transactionDTO = new ShopTransactionDTO();
				transactionDTO.setItemId(rs.getInt("item_id"));
				transactionDTO.setItemName(rs.getString("ItemName"));
				transactionDTO.setTransactionDate(rs.getString("Time"));
				transactionDTO.setTransactionType(rs.getString("Type"));
				transactionDTO.setTransactionQty(rs.getDouble("Quantity"));
				transactionDTO.setItemPurchaseCost(rs.getDouble("oldBuy"));
				transactionDTO.setItemSoldCost(rs.getDouble("oldSell"));
				transactionDTO.setNewPurchaseCost(rs.getDouble("newBuy"));
				transactionDTO.setUserId(userId);
				transactionDTO.setNewSoldCost(rs.getDouble("newSell"));
				transactionDTO.setUom(rs.getString("uom"));
				transactionDTO.setCustomerName(rs.getString("customer"));
				transactions.add(transactionDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getShopItems", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getShopItems.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getShopItems method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getShopItems.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(transactions);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<CreditCustomerDTO> getCCDetails(int userId, ResponseDTO<CreditCustomerDTO> response)
			throws DataAccessException {

		Query query = (Query)getQueries().get("getCreditCustomers");
		String sql = query.getQuery();
		List<CreditCustomerDTO> ccDetails = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, userId);
			CreditCustomerDTO ccDTO = null;
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				ccDTO =  new CreditCustomerDTO();
				ccDTO.setCc_id(rs.getInt("cc_id"));
				ccDTO.setCc_mobile(rs.getString("cc_mobile"));
				ccDTO.setCc_name(rs.getString("cc_name"));
				ccDTO.setCc_paid_amount(rs.getDouble("cc_paid_amount"));
				ccDTO.setCc_credit_amount(rs.getDouble("ct_credit_amount"));
				//ccDTO.setCc_pending_amount(rs.getDouble("pending_amount"));
				ccDTO.setStatus(rs.getString("credit_status"));
				ccDetails.add(ccDTO);
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - getCreditCustomers", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getCreditCustomers.Please try again");
			return response;
		} catch(Exception e){
			log.error("Unable to execute the query - getCreditCustomers", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getCreditCustomers.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	
		
		response.setData(ccDetails);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;

	}

	@Override
	public int createCreditCustomer(CreditCustomerDTO creditCustomerDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("createCreditCustomer");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql,  PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, creditCustomerDTO.getCc_mobile());
			pstmt.setObject(2,creditCustomerDTO.getCc_name());
			pstmt.setObject(3, creditCustomerDTO.getUserId());
			pstmt.setObject(4, creditCustomerDTO.getUserId());
			pstmt.setObject(5, creditCustomerDTO.getUserId());
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
            if(rs != null && rs.next()){
                return rs.getInt(1);
            }
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}

	@Override
	public ResponseDTO<CCTransactionDTO> getCCTransactions(int ccId, int transactionId, ResponseDTO<CCTransactionDTO> response)
			throws DataAccessException {

		Query query = (Query)getQueries().get("getCreditCustomersTransactions");
		String sql = query.getQuery();
		List<CCTransactionDTO> ccDetails = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			if(StringUtils.isNotEmpty(sql))
			{
				if(transactionId > 0)
				{
					sql = sql.replaceAll("&filter", " AND id = "+transactionId+" ");
				}else{
					sql = sql.replaceAll("&filter", "");
				}
			}
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, ccId);
			CCTransactionDTO ccDTO = null;
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				ccDTO =  new CCTransactionDTO();
				ccDTO.setCc_id(rs.getInt("id"));
				ccDTO.setCustomer_id(rs.getInt("credit_customer_id"));
				ccDTO.setTransactionDate(rs.getString("transaction_date"));
				ccDTO.setCc_total_amount(rs.getDouble("total_amount"));
				ccDTO.setCc_paid_amount(rs.getDouble("paid_amount"));
				ccDTO.setCc_credit_amount(rs.getDouble("credit_amount"));
				ccDTO.setCc_discount(rs.getDouble("discount"));
				ccDTO.setInvoiceNumber(rs.getString("invoice_number"));
				ccDTO.setCc_prev_credit_amount(rs.getDouble("previous_credit"));
				ccDTO.setPdf(rs.getString("pdf"));
				
				if(rs.getDouble("total_amount") > 0)
				{
					ccDTO.setTransactionDate(rs.getString("transaction_date"));
					List<ShopTransactionDTO> transactions = new ArrayList<>();
					transactions.addAll(getTransactions(rs.getInt("id")));
					
					if(rs.getDouble("credit_amount")<0)
					{
						ShopTransactionDTO ccDTO1 = new ShopTransactionDTO();
						ccDTO1.setItemName("Credit Clear");
						ccDTO1.setTransactionQty(1);
						ccDTO1.setUom("NA");
						ccDTO1.setItemSoldCost(0.0);
						ccDTO1.setTotalAmount(Math.abs(rs.getDouble("credit_amount")));
						transactions.add(ccDTO1);
						
					}
					
					ccDTO.setTransactions(transactions);
					
					
					
				}else{
						List<ShopTransactionDTO> transactions = new ArrayList<>();
						ShopTransactionDTO ccDTO1 = new ShopTransactionDTO();
						ccDTO1.setItemName("Credit Clear");
						ccDTO1.setTransactionQty(1);
						ccDTO1.setUom("NA");
						ccDTO1.setItemSoldCost(0.0);
						ccDTO1.setTotalAmount(rs.getDouble("paid_amount"));
						transactions.add(ccDTO1);
						ccDTO.setTransactions(transactions);
				}
				
				ccDetails.add(ccDTO);
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - getCreditCustomers", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getCreditCustomers.Please try again");
			return response;
		} catch(Exception e){
			log.error("Unable to execute the query - getCreditCustomers", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getCreditCustomers.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	
		
		response.setData(ccDetails);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;

	}
	
	
	@Override
	public ResponseDTO<CCTransactionDTO> getCCTransactionsWithFilters(int ccId, int transactionId, String startDate, String endDate, ResponseDTO<CCTransactionDTO> response)
			throws DataAccessException {

		Query query = (Query)getQueries().get("getCreditCustomersTransactions");
		String sql = query.getQuery();
		List<CCTransactionDTO> ccDetails = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			if(StringUtils.isNotEmpty(sql))
			{
				if(startDate != null && startDate.trim().length() > 0)
				{
					
					sql = sql.replaceAll("&filter", " AND transaction_date between ':startDate' and ':endDate' ");
					sql = sql.replaceAll(":startDate", startDate);
					sql = sql.replaceAll(":endDate", endDate);
				}
				
			/*	if(transactionId > 0)
				{
					sql = sql.replaceAll("&filter", " AND id = "+transactionId+" ");
				}else{
					sql = sql.replaceAll("&filter", "");
				}*/
			}
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, ccId);
			CCTransactionDTO ccDTO = null;
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				ccDTO =  new CCTransactionDTO();
				ccDTO.setCc_id(rs.getInt("id"));
				ccDTO.setCustomer_id(rs.getInt("credit_customer_id"));
				ccDTO.setTransactionDate(rs.getString("transaction_date"));
				ccDTO.setCc_total_amount(rs.getDouble("total_amount"));
				ccDTO.setCc_paid_amount(rs.getDouble("paid_amount"));
				ccDTO.setCc_credit_amount(rs.getDouble("credit_amount"));
				ccDTO.setCc_discount(rs.getDouble("discount"));
				ccDTO.setInvoiceNumber(rs.getString("invoice_number"));
				ccDTO.setCc_prev_credit_amount(rs.getDouble("previous_credit"));
				ccDTO.setPdf(rs.getString("pdf"));
				
				if(rs.getDouble("total_amount") > 0)
				{
					ccDTO.setTransactionDate(rs.getString("transaction_date"));
					List<ShopTransactionDTO> transactions = new ArrayList<>();
					transactions.addAll(getTransactions(rs.getInt("id")));
					
					if(rs.getDouble("credit_amount")<0)
					{
						ShopTransactionDTO ccDTO1 = new ShopTransactionDTO();
						ccDTO1.setItemName("Credit Clear");
						ccDTO1.setTransactionQty(1);
						ccDTO1.setUom("NA");
						ccDTO1.setItemSoldCost(0.0);
						ccDTO1.setTotalAmount(Math.abs(rs.getDouble("credit_amount")));
						transactions.add(ccDTO1);
						
					}
					
					ccDTO.setTransactions(transactions);
					
					
					
				}else{
						List<ShopTransactionDTO> transactions = new ArrayList<>();
						ShopTransactionDTO ccDTO1 = new ShopTransactionDTO();
						ccDTO1.setItemName("Credit Clear");
						ccDTO1.setTransactionQty(1);
						ccDTO1.setUom("NA");
						ccDTO1.setItemSoldCost(0.0);
						ccDTO1.setTotalAmount(rs.getDouble("paid_amount"));
						transactions.add(ccDTO1);
						ccDTO.setTransactions(transactions);
				}
				
				ccDetails.add(ccDTO);
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - getCreditCustomers", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getCreditCustomers.Please try again");
			return response;
		} catch(Exception e){
			log.error("Unable to execute the query - getCreditCustomers", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getCreditCustomers.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	
		
		response.setData(ccDetails);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;

	}
	
	
	
	
	
	
	
	

	private List<ShopTransactionDTO> getTransactions(int ccTransactionId) throws DataAccessException
	{


		Query query = (Query)getQueries().get("getCreditCustomerTransactionDetails");
		String sql = query.getQuery();
		List<ShopTransactionDTO> transactions = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, ccTransactionId);
			ShopTransactionDTO ccDTO = null;
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				ccDTO =  new ShopTransactionDTO();
				ccDTO.setItemId(rs.getInt("item_id"));
				ccDTO.setItemName(rs.getString("item_name"));
				ccDTO.setTransactionQty(rs.getDouble("qty"));
				ccDTO.setUom(rs.getString("uom"));
				ccDTO.setItemSoldCost(rs.getDouble("unit_cost"));
				ccDTO.setTotalAmount(rs.getDouble("txn_amount"));
				transactions.add(ccDTO);
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - getCreditCustomers", e);
			
		} catch(Exception e){
			log.error("Unable to execute the query - getCreditCustomers", e);
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	
		
		return transactions;

	
	}

	@Override
	public int insertCustomerTransaction(CreditCustomerDTO creditCustomerDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("createCustomerTransaction");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql,  PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, creditCustomerDTO.getCc_id());
			pstmt.setObject(2, creditCustomerDTO.getStatus());
			pstmt.setObject(3, creditCustomerDTO.getCc_total_amount());
			pstmt.setObject(4, creditCustomerDTO.getCc_paid_amount());
			pstmt.setObject(5, creditCustomerDTO.getCc_credit_amount());
			pstmt.setObject(6, creditCustomerDTO.getCc_discount());
			pstmt.setObject(7, creditCustomerDTO.getUserId());
			pstmt.setObject(8, creditCustomerDTO.getCc_prev_credit_amount());
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
            if(rs != null && rs.next()){
                return rs.getInt(1);
            }
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}
	
	@Override
	public int updateCustomerTransaction(CreditCustomerDTO creditCustomerDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateCustomerTransaction");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, creditCustomerDTO.getStatus());
			pstmt.setObject(2, creditCustomerDTO.getCc_total_amount());
			pstmt.setObject(3, creditCustomerDTO.getCc_paid_amount());
			pstmt.setObject(4, creditCustomerDTO.getCc_credit_amount());
			pstmt.setObject(5, creditCustomerDTO.getCc_discount());
			pstmt.setObject(6, creditCustomerDTO.getCc_prev_credit_amount());
			pstmt.setObject(7, creditCustomerDTO.getCustomerTransactionId());
			
			//pstmt.setObject(7, creditCustomerDTO.getId());
			
			return pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}

	@Override
	public int updateCreditCustomerBalance(int customerId, Double amount) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateCreditCustomerBalance");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, amount);
			pstmt.setObject(2,customerId);
			return pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}

	@Override
	public int clearCreditCustomerBalance(int customerId, double credit) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("clearCreditCustomerBalance");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, credit);
			pstmt.setObject(2, customerId);
			return pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}

	@Override
	public CreditCustomerDTO getCCDetail(int customerId) throws DataAccessException {

		Query query = (Query)getQueries().get("getCreditCustomerDetails");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		CreditCustomerDTO ccDTO = new CreditCustomerDTO();
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, customerId);
			
			rs = pstmt.executeQuery();				
			if(rs.next()) {
				ccDTO.setCc_mobile(rs.getString("mobile"));
				ccDTO.setCc_name(rs.getString("name"));
			}

		}catch (SQLException e) {
			log.error("Unable to execute the query - getCreditCustomers", e);
			
		} catch(Exception e){
			log.error("Unable to execute the query - getCreditCustomers", e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}	

		return ccDTO;

	}
	
	public ResponseDTO<CCTransactionDTO> generatePDF(int userId, int customerId, int transactionId, ResponseDTO<CCTransactionDTO> response)
	{
		String fileName = IPaymentGatewayURLS.extDir +userId+"/PDF/"+customerId+"/"+transactionId+".pdf";
		
		File directory = new File(IPaymentGatewayURLS.extDir +userId);
		if (! directory.exists()){
			directory.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
			
				
		}

		directory = new File(IPaymentGatewayURLS.extDir +userId+"/PDF");
		if (! directory.exists()){
			directory.mkdir();
		}
		
		directory = new File(IPaymentGatewayURLS.extDir +userId+"/PDF/"+customerId);
		if (! directory.exists()){
			directory.mkdir();
		}
		try {
			ResponseDTO<CCTransactionDTO> response1 = getCCTransactions(customerId, transactionId, response);
			List<CCTransactionDTO> transactionDetails = response1.getData();
			
			CreditCustomerDTO customerInfo = getCCDetail(customerId);
			ShopUserDTO shopDetails = shopLogin(userId);
			SBookPDFGenerator pdf =  new SBookPDFGenerator();
			//transactionDetails.get(0).setPdf(fileName);
			try {
				pdf.drawMultipageTable(shopDetails, customerInfo, transactionDetails.get(0), fileName);
				updatePDF(fileName,transactionId);
			} catch (IOException e) {
				fileName = "PDF Does Not Exists";
			}
			
			
			
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
	
	
	
	public int updatePDF(String fileName, int userId) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updatePDF");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, fileName);
			pstmt.setObject(2, userId);
			return pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}

	@Override
	public ResponseDTO<ShopItemDTO> deleteItem(ResponseDTO<ShopItemDTO> response, ShopItemDTO shopItemDTO) throws DataAccessException  {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("deleteItem");
		String sql = query.getQuery();
		List<ShopItemDTO> shopItemDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, shopItemDTO.getItemId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				shopItemDTOs.add(shopItemDTO);
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to delete Item.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to delete Item.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setResponseMessage("Item deleted Successfully");
		response.setData(shopItemDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<CreditCustomerDTO> deleteCustomer(ResponseDTO<CreditCustomerDTO> response,
			CreditCustomerDTO creditCustomerDTO) throws DataAccessException  {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("deleteCreditCustomer");
		String sql = query.getQuery();
		List<CreditCustomerDTO> CreditCustomerDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, creditCustomerDTO.getCc_id());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				CreditCustomerDTOs.add(creditCustomerDTO);
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to delete Credit Customer.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to delete Credit Customer.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setResponseMessage("Credit Customer deleted Successfully");
		response.setData(CreditCustomerDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<CreditCustomerDTO> updateCreditCustomer(ResponseDTO<CreditCustomerDTO> response,CreditCustomerDTO creditCustomerDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateCreditCustomer");
		String sql = query.getQuery();
		List<CreditCustomerDTO> creditCustomerDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, creditCustomerDTO.getCc_mobile());
			pstmt.setObject(2, creditCustomerDTO.getCc_name());
			pstmt.setObject(3, creditCustomerDTO.getUserId());
			pstmt.setObject(4, creditCustomerDTO.getCc_id());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				creditCustomerDTOs.add(creditCustomerDTO);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to update the customer");
			return response;
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setResponseMessage("Customer updated successfully");
		response.setData(creditCustomerDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	public int deleteInventoryTransaction(int customer_transaction_id) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("deleteInventoryTransaction");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, customer_transaction_id);
			return pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}
	
	public int deleteCustomerTransaction(int customer_transaction_id) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("deleteCustomerTransaction");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, customer_transaction_id);
			return pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return 0;
		
	}
	

}
