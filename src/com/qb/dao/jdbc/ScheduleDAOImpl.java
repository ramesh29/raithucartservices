package com.qb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.qb.dao.ABaseDAO;
import com.qb.dao.IScheduleDAO;
import com.qb.dao.query.Query;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ScheduleDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.exception.DataAccessException;
import com.qb.util.ConnectionManager;
import com.qb.util.IPaymentGatewayURLS;

public class ScheduleDAOImpl extends ABaseDAO implements IScheduleDAO {
	
	private static final Logger log = Logger.getLogger(ScheduleDAOImpl.class.getName());

	@Override
	public ResponseDTO<ScheduleDTO> getSchedules(ResponseDTO<ScheduleDTO> response) throws DataAccessException {

		List<ScheduleDTO> scheduleDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("getSchedules");
		String sql = query.getQuery();
		sql = sql.replace("&filterCondition", "AND date(s.start_date) > now()");
		ScheduleDTO scheduleDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				scheduleDTO = new ScheduleDTO();
				scheduleDTO.setScheduleId(rs.getInt("schedule_id"));
				scheduleDTO.setCourseId(rs.getInt("course_id"));
				scheduleDTO.setCourseName(rs.getString("course_name"));
				scheduleDTO.setOrgId(rs.getInt("org_id"));
				scheduleDTO.setSeats(rs.getInt("seats"));
				scheduleDTO.setStartDate(rs.getString("startDate"));
				scheduleDTO.setEndDate(rs.getString("endDate"));
				scheduleDTO.setBookedSeats(rs.getInt("booked_seats"));
				scheduleDTO.setDays(rs.getInt("days"));
				scheduleDTO.setEditable(rs.getString("is_editable"));
				scheduleDTO.setConfirm(rs.getString("confirm"));
				scheduleDTOs.add(scheduleDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getSchedules", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getSchedules.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getSchedules method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getSchedules.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(scheduleDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<ScheduleDTO> addSchdule(ResponseDTO<ScheduleDTO> response, ScheduleDTO schduleDTO)
			throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("insertSchedules");
		String sql = query.getQuery();
		List<ScheduleDTO> schduleDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, schduleDTO.getCourseId());
			pstmt.setObject(2, schduleDTO.getOrgId());
			if(schduleDTO.getStartDate() != null && schduleDTO.getStartDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(schduleDTO.getStartDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(3, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(3, null);
			}
			if(schduleDTO.getEndDate() != null && schduleDTO.getEndDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(schduleDTO.getEndDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(4, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(4, null);
			}
			pstmt.setObject(5, schduleDTO.getDays());
			pstmt.setObject(6, schduleDTO.getSeats());
			pstmt.setObject(7, schduleDTO.getPrice());
			pstmt.setObject(8, schduleDTO.getStartTime());
			pstmt.setObject(9, schduleDTO.getEndTime());
			pstmt.setObject(10, schduleDTO.getConfirm());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					
					schduleDTO.setOrgId(rs.getInt(1));
					schduleDTOs.add(schduleDTO);
					}
				
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to add schedule.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to add schedule.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(schduleDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<ScheduleDTO> updateSchdule(ResponseDTO<ScheduleDTO> response, ScheduleDTO schduleDTO)
			throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateSchedules");
		String sql = query.getQuery();
		List<ScheduleDTO> schduleDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			if(schduleDTO.getStartDate() != null && schduleDTO.getStartDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(schduleDTO.getStartDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(1, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(1, null);
			}
			if(schduleDTO.getEndDate() != null && schduleDTO.getEndDate().trim().length() > 0)
			{
				Long milliSecs = getStringAsDate(schduleDTO.getEndDate(), IPaymentGatewayURLS.dateFormat).getTime();
				pstmt.setTimestamp(2, new Timestamp(milliSecs));
			}else
			{
				pstmt.setObject(2, null);
			}
			pstmt.setObject(3, schduleDTO.getDays());
			pstmt.setObject(4, schduleDTO.getSeats());
			pstmt.setObject(5, schduleDTO.getPrice());
			pstmt.setObject(6, schduleDTO.getStartTime());
			pstmt.setObject(7, schduleDTO.getEndTime());
			pstmt.setObject(8, schduleDTO.getConfirm());
			pstmt.setObject(9, schduleDTO.getScheduleId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				schduleDTOs.add(schduleDTO);
				
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to update schedule.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to update schedule.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(schduleDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<ScheduleDTO> deleteSchdule(ResponseDTO<ScheduleDTO> response, ScheduleDTO schduleDTO)
			throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("deleteSchedule");
		String sql = query.getQuery();
		List<ScheduleDTO> schduleDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			
			pstmt.setObject(1, schduleDTO.getScheduleId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				
				response.setResponseMessage("Schedule deleted successfully");
				
				
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to delete schedule.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to delete schedule.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(schduleDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<CourseBookingDTO> getOrgCourseSchdules(SearchParametersDTO search, ResponseDTO<CourseBookingDTO> response) throws DataAccessException {

		List<CourseBookingDTO>  courseBookingDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("getOrgCourseSchedules");
		String sql = query.getQuery();
		
		if(search.getStartDate() != null && search.getStartDate().trim().length() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND date(s.start_date) between ':startDate' and ':endDate' &filter_condition");
			sql = sql.replaceAll(":startDate", search.getStartDate());
			sql = sql.replaceAll(":endDate", search.getEndDate());
		}
		
		if(search.getCategoryId() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND cc.course_category_id = :categoryId &filter_condition");
			sql = sql.replaceAll(":categoryId", search.getCategoryId()+"");
		}
		
		if( search.getSubCategoryId() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND sc.course_sub_category_id = :subCategoryId &filter_condition");
			sql = sql.replaceAll(":subCategoryId", search.getSubCategoryId() +"");
		}
		
		if(search.getCourseId() != null && search.getCourseId().size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND c.course_id in (?) &filter_condition");
			sql = setArray(sql, search.getCourseId().toArray(new Integer[search.getCourseId().size()]), 2);
		}
		
		if(search.getStatus() != null && search.getStatus().size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND confirm in (?) &filter_condition");
			sql = setList(sql, search.getStatus(), 2);
		}
		
		/*if(search.getSchdulestatus() != null && search.getSchdulestatus().size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND confirm in (?) &filter_condition");
			sql = setArray(sql, search.getStatus().toArray(new Integer[search.getStatus().size()]), 2);
		}*/
		
		if(search.getScheduleId()  > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND s.schedule_id = :schedule_id &filter_condition");
			sql = sql.replaceAll(":schedule_id", search.getScheduleId()+"");
		}
		
		sql = sql.replaceAll("&filter_condition", " ");
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, search.getOrgId());
			rs = pstmt.executeQuery();
			
			CourseBookingDTO courseBookingDTO = null;
			while(rs.next()) {
				courseBookingDTO =  new CourseBookingDTO();
				courseBookingDTO.setCollege(rs.getString("college"));
				courseBookingDTO.setCategoryName(rs.getString("catName"));
				courseBookingDTO.setSubCatName(rs.getString("subCatName"));
				courseBookingDTO.setCourseName(rs.getString("courseName"));
				courseBookingDTO.setScheduleId(rs.getInt("scheduleId"));
				courseBookingDTO.setStartDate(rs.getString("startDate"));
				courseBookingDTO.setEndDate(rs.getString("endDate"));
				courseBookingDTO.setSeats(rs.getString("seats"));
				courseBookingDTO.setPrice(rs.getString("price"));
				courseBookingDTO.setTimings(rs.getString("timings"));
				courseBookingDTO.setAvailable(rs.getString("availability"));
				courseBookingDTO.setBookedSeats(rs.getString("bookedSeats"));
				courseBookingDTO.setTotalSeats(rs.getString("totalSeats"));
				courseBookingDTO.setStartTime(rs.getString("startTime"));
				courseBookingDTO.setEndTime(rs.getString("endTime"));
				courseBookingDTO.setDisplay(rs.getString("confirm"));
				courseBookingDTOs.add(courseBookingDTO);
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getCourseSchdules method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get course scheules.Please try again");
			return response;
			
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get course scheules.Please try again");
			return response;
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		response.setData(courseBookingDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

}
