package com.qb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.qb.dao.ABaseDAO;
import com.qb.dao.IOrganizationDAO;
import com.qb.dao.query.Query;
import com.qb.dto.OrgAccountDTO;
import com.qb.dto.OrgCourseDTO;
import com.qb.dto.OrgDTO;
import com.qb.dto.ResponseDTO;
import com.qb.exception.DataAccessException;
import com.qb.util.ConnectionManager;

public class OrganizationDAOImpl extends ABaseDAO implements IOrganizationDAO {
	
	private static final Logger log = Logger.getLogger(OrganizationDAOImpl.class.getName());

	@Override
	public ResponseDTO<OrgDTO> getOrgs(ResponseDTO<OrgDTO> response) throws DataAccessException {

		List<OrgDTO> orgs = new ArrayList<>();
		Query query = (Query)getQueries().get("getOrgs");
		String sql = query.getQuery();
		OrgDTO orgDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				orgDTO = new OrgDTO();
				orgDTO.setOrgId(rs.getInt("org_id"));
				orgDTO.setOrgName(rs.getString("org_name"));
				orgDTO.setPhone(rs.getString("phone"));
				orgDTO.setEmail(rs.getString("email"));
				orgDTO.setAddress1(rs.getString("address_1"));
				orgDTO.setAddress2(rs.getString("address_2"));
				orgDTO.setCity(rs.getString("city"));
				orgDTO.setState(rs.getString("state"));
				orgDTO.setZip(rs.getString("zip"));
				orgDTO.setCountry(rs.getString("country"));
				orgDTO.setActive(rs.getString("is_enable"));
				orgDTO.setCreationDate(rs.getString("creation_on"));
				orgs.add(orgDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getUser", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(orgs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<OrgAccountDTO> getOrgAccounts(ResponseDTO<OrgAccountDTO> response, int orgId) throws DataAccessException {

		List<OrgAccountDTO> orgAccounts = new ArrayList<>();
		Query query = (Query)getQueries().get("getOrgAccounts");
		String sql = query.getQuery();
		OrgAccountDTO orgAccountDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, orgId);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				orgAccountDTO = new OrgAccountDTO();
				orgAccountDTO.setAccountId(rs.getInt("account_id"));
				orgAccountDTO.setAccountHolderName(rs.getString("account_holder_name"));
				orgAccountDTO.setAccountNumber(rs.getString("account_number"));
				orgAccountDTO.setBankName(rs.getString("bank_name"));
				orgAccountDTO.setBranchName(rs.getString("branch_name"));
				orgAccountDTO.setIfscCode(rs.getString("ifsc_code"));
				orgAccountDTO.setCity(rs.getString("city"));
				orgAccountDTO.setMmid(rs.getString("mmid"));
				orgAccounts.add(orgAccountDTO);
			}
			

		}catch (SQLException e) {
			log.error("Unable to execute the query - getUser", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(orgAccounts);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<OrgDTO> addOrg(ResponseDTO<OrgDTO> response,OrgDTO orgDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("addOrg");
		String sql = query.getQuery();
		List<OrgDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, orgDTO.getOrgName());
			pstmt.setObject(2, orgDTO.getPhone());
			pstmt.setObject(3, orgDTO.getEmail());
			pstmt.setObject(4, orgDTO.getAddress1());
			pstmt.setObject(5, orgDTO.getAddress2());
			pstmt.setObject(6, orgDTO.getCity());
			pstmt.setObject(7, orgDTO.getState());
			pstmt.setObject(8, orgDTO.getZip());
			pstmt.setObject(9, orgDTO.getCountry());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					
					orgDTO.setOrgId(rs.getInt(1));
					orgDTOs.add(orgDTO);
					}
				
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to get Orgs.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<OrgAccountDTO> addOrgAccount(ResponseDTO<OrgAccountDTO> response, OrgAccountDTO orgDTO)
			throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("addOrgAccount");
		String sql = query.getQuery();
		List<OrgAccountDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, orgDTO.getOrgId());
			pstmt.setObject(2, orgDTO.getAccountNumber());
			pstmt.setObject(3, orgDTO.getAccountHolderName());
			pstmt.setObject(4, orgDTO.getBankName());
			pstmt.setObject(5, orgDTO.getBranchName());
			pstmt.setObject(6, orgDTO.getCity());
			pstmt.setObject(7, orgDTO.getIfscCode());
			pstmt.setObject(8, orgDTO.getMmid());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) 
				{
					orgDTO.setAccountId(rs.getInt(1));
					orgDTOs.add(orgDTO);
				}
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to get Orgs.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<OrgDTO> updateOrg(ResponseDTO<OrgDTO> response, OrgDTO orgDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateOrg");
		String sql = query.getQuery();
		List<OrgDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, orgDTO.getOrgName());
			pstmt.setObject(2, orgDTO.getPhone());
			pstmt.setObject(3, orgDTO.getEmail());
			pstmt.setObject(4, orgDTO.getAddress1());
			pstmt.setObject(5, orgDTO.getAddress2());
			pstmt.setObject(6, orgDTO.getCity());
			pstmt.setObject(7, orgDTO.getState());
			pstmt.setObject(8, orgDTO.getZip());
			pstmt.setObject(9, orgDTO.getCountry());
			pstmt.setObject(10, orgDTO.getOrgId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				orgDTOs.add(orgDTO);
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to get Orgs.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setResponseMessage("Org updated Successfully");
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<OrgAccountDTO> updateOrgAccount(ResponseDTO<OrgAccountDTO> response, OrgAccountDTO orgDTO)
			throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateOrgAccount");
		String sql = query.getQuery();
		List<OrgAccountDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, orgDTO.getAccountNumber());
			pstmt.setObject(2, orgDTO.getAccountHolderName());
			pstmt.setObject(3, orgDTO.getBankName());
			pstmt.setObject(4, orgDTO.getBranchName());
			pstmt.setObject(5, orgDTO.getCity());
			pstmt.setObject(6, orgDTO.getIfscCode());
			pstmt.setObject(7, orgDTO.getMmid());
			pstmt.setObject(8, orgDTO.getAccountId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				orgDTOs.add(orgDTO);
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to get Orgs.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setResponseMessage("Org Account updated Successfully");
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<OrgDTO> inactivateOrg(ResponseDTO<OrgDTO> response, OrgDTO orgDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("UpdateOrgAccess");
		String sql = query.getQuery();
		List<OrgDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, orgDTO.getActive());
			pstmt.setObject(2, orgDTO.getOrgId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				orgDTOs.add(orgDTO);
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to get Orgs.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setResponseMessage("Org activated Successfully");
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<OrgDTO> getOrg(ResponseDTO<OrgDTO> response, int orgId) throws DataAccessException {

		List<OrgDTO> orgs = new ArrayList<>();
		Query query = (Query)getQueries().get("getOrg");
		String sql = query.getQuery();
		OrgDTO orgDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, orgId);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				orgDTO = new OrgDTO();
				orgDTO.setOrgId(rs.getInt("org_id"));
				orgDTO.setOrgName(rs.getString("org_name"));
				orgDTO.setPhone(rs.getString("phone"));
				orgDTO.setEmail(rs.getString("email"));
				orgDTO.setAddress1(rs.getString("address_1"));
				orgDTO.setAddress2(rs.getString("address_2"));
				orgDTO.setCity(rs.getString("city"));
				orgDTO.setState(rs.getString("state"));
				orgDTO.setZip(rs.getString("zip"));
				orgDTO.setCountry(rs.getString("country"));
				orgDTO.setActive("is_enable");
				orgDTO.setCreationDate(rs.getString("creation_on"));
				orgs.add(orgDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getUser", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(orgs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<OrgDTO> deleteOrg(ResponseDTO<OrgDTO> response, OrgDTO orgDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("deleteOrg");
		String sql = query.getQuery();
		List<OrgDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, orgDTO.getOrgId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				orgDTOs.add(orgDTO);
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to delete Org.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to delete Org.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setResponseMessage("Org deleted Successfully");
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<OrgCourseDTO> getOrgCourses(ResponseDTO<OrgCourseDTO> response,  int orgId) throws DataAccessException {

		List<OrgCourseDTO> orgs = new ArrayList<>();
		Query query = (Query)getQueries().get("getOrgCourses");
		String sql = query.getQuery();
		OrgCourseDTO orgDTO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, orgId);
			rs = pstmt.executeQuery();				
			while(rs.next()) {
				orgDTO = new OrgCourseDTO();
				orgDTO.setOrgCourseId(rs.getInt("org_course_id"));
				orgDTO.setCourseName(rs.getString("course_name"));
				orgDTO.setOrgId(rs.getInt("org_id"));
				orgDTO.setCourseId(rs.getInt("course_id"));
				orgDTO.setSeats(rs.getInt("seats"));
				orgDTO.setDuration(rs.getInt("duration"));
				orgDTO.setPaymentAgreementType(rs.getString("payment_agreement_type"));
				orgDTO.setPaymentAgreementValue(rs.getString("payment_value"));
				orgs.add(orgDTO);
			}
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - getOrgCourses", e);
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getOrgCourses.Please try again");
			return response;
		} catch(Exception e){
			log.error("Error occurred in getOrgCourses method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		response.setData(orgs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<OrgCourseDTO> addOrgCourse(ResponseDTO<OrgCourseDTO> response, OrgCourseDTO orgDTO)
			throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("addOrgCourse");
		String sql = query.getQuery();
		List<OrgCourseDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, orgDTO.getOrgId());
			pstmt.setObject(2, orgDTO.getCourseId());
			pstmt.setObject(3, orgDTO.getDuration());
			pstmt.setObject(4, orgDTO.getSeats());
			pstmt.setObject(5, orgDTO.getPaymentAgreementType());
			pstmt.setObject(6, orgDTO.getPaymentAgreementValue());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) 
				{
					orgDTO.setOrgCourseId(rs.getInt(1));
					orgDTOs.add(orgDTO);
				}
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to get addOrgCourse.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get addOrgCourse.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}

	@Override
	public ResponseDTO<OrgCourseDTO> updateOrgCourse(ResponseDTO<OrgCourseDTO> response, OrgCourseDTO orgDTO)
			throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("updateOrgAccount");
		String sql = query.getQuery();
		List<OrgCourseDTO> orgDTOs = new ArrayList<>(); 
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, orgDTO.getDuration());
			pstmt.setObject(2, orgDTO.getSeats());
			pstmt.setObject(3, orgDTO.getPaymentAgreementType());
			pstmt.setObject(4, orgDTO.getPaymentAgreementValue());
			pstmt.setObject(5, orgDTO.getOrgCourseId());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				orgDTOs.add(orgDTO);
			}else
			{
				response.setResponseCode(405);
				response.setSuccess(false);	
				response.setResponseMessage("Unable to get Orgs.Please try again");
				return response;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get Orgs.Please try again");
			return response;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		response.setData(orgDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
		
	}


	



	


}
