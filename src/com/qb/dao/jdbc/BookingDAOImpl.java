package com.qb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.qb.dao.ABaseDAO;
import com.qb.dao.IBookingDAO;
import com.qb.dao.query.Query;
import com.qb.dto.AtomPaymentDTO;
import com.qb.dto.BookingDTO;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.OrgBookingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ScheduleDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.dto.TransactionDTO;
import com.qb.exception.DataAccessException;
import com.qb.exception.ErrorCode;
import com.qb.util.ConnectionManager;

public class BookingDAOImpl extends ABaseDAO implements IBookingDAO
{
	private static final Logger log = Logger.getLogger(BookingDAOImpl.class.getName());
	
	@Override
	public int reserveQuantity(List<Integer> scheduleIds) throws DataAccessException {

		Query query = (Query)getQueries().get("reserveSeat");
		String sql = query.getQuery();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		sql = setIntegerList(sql, scheduleIds, 1);
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			return  pstmt.executeUpdate();
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - reserveQuantity", e);
		} catch(Exception e){
			log.error("Error occurred in reserveQuantity method : "+e.getMessage());
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return 0;
	}

	@Override
	public int createTransaction(TransactionDTO transactionDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("addTransaction");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, transactionDTO.getUserId());
			pstmt.setObject(2, transactionDTO.getTotalPrice());
			pstmt.setObject(3, transactionDTO.getServiceTax());
			pstmt.setObject(4, transactionDTO.getInternetHandlingFees());
			pstmt.setObject(5, transactionDTO.getPaymentGateway());
			pstmt.setObject(6, transactionDTO.getTransactionStatus());
			pstmt.setObject(7, transactionDTO.getUserId());
			pstmt.setObject(8, transactionDTO.getUserId());
			pstmt.setObject(9, transactionDTO.getTransactionSource());
			pstmt.setObject(10, transactionDTO.getMobileNumber());
			pstmt.setObject(11, transactionDTO.getPlan());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					
					return rs.getInt(1);
					}
				
			}else
			{
				return -1;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return -1;
		
	}

	@Override
	public int createBooking(int transactionId , List<BookingDTO> bookingDTOs) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("addBooking");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			for(BookingDTO booking : bookingDTOs)
			{
				pstmt.setObject(1, transactionId);
				pstmt.setObject(2, booking.getOrgId());
				pstmt.setObject(3, booking.getScheduleId());
				pstmt.setObject(4, booking.getUserId());
				pstmt.setObject(5, booking.getOrgPrice());
				pstmt.setObject(6, booking.getBookingStatus());
				pstmt.setObject(7, booking.getUserId());
				pstmt.setObject(8, booking.getUserId());
				pstmt.setObject(9, booking.getBookingSource());
				pstmt.setObject(10, booking.getBookingNumber());
				/*pstmt.setObject(11, booking.getApiBatchId());
				pstmt.setObject(12, booking.getApiCourseId());*/
				pstmt.addBatch();
			}
			
			int count[] = pstmt.executeBatch();
			return count.length;
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
	
		
	}

	@Override
	public int bookConfirm(List<Integer> scheduleIds) throws DataAccessException {

		Query query = (Query)getQueries().get("ConfirmBooking");
		String sql = query.getQuery();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		sql = setIntegerList(sql, scheduleIds, 1);
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			return  pstmt.executeUpdate();
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - reserveQuantity", e);
		} catch(Exception e){
			log.error("Error occurred in reserveQuantity method : "+e.getMessage());
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return 0;
	}
	
	@Override
	public int bookingFailed(List<Integer> scheduleIds) throws DataAccessException {

		Query query = (Query)getQueries().get("BookingFailed");
		String sql = query.getQuery();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		sql = setIntegerList(sql, scheduleIds, 1);
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			return  pstmt.executeUpdate();
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - reserveQuantity", e);
		} catch(Exception e){
			log.error("Error occurred in reserveQuantity method : "+e.getMessage());
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return 0;
	}

	@Override
	public int updateTransaction(TransactionDTO transactionDTO) throws DataAccessException {

		Query query = (Query)getQueries().get("UpdateTransaction");
		String sql = query.getQuery();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, transactionDTO.getTransactionStatus());
			pstmt.setObject(2, transactionDTO.getUserId());
			pstmt.setObject(3, transactionDTO.getTransactionId());
			
			return  pstmt.executeUpdate();
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - UpdateTransaction", e);
		} catch(Exception e){
			log.error("Error occurred in UpdateTransaction method : "+e.getMessage());
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return 0;
	}

	@Override
	public int updateBooking(TransactionDTO transactionDTO) throws DataAccessException {

		Query query = (Query)getQueries().get("UpdateBooking");
		String sql = query.getQuery();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, transactionDTO.getTransactionStatus());
			pstmt.setObject(2, transactionDTO.getUserId());
			pstmt.setObject(3, transactionDTO.getTransactionId());
			
			return  pstmt.executeUpdate();
			
		}catch (SQLException e) {
			log.error("Unable to execute the query - UpdateTransaction", e);
		} catch(Exception e){
			log.error("Error occurred in UpdateTransaction method : "+e.getMessage());
			throw new DataAccessException(ErrorCode.ERR_UNABLE_TO_PROCESS_WEBSERVICE, e);
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}			
		return 0;
	}

	@Override
	public ResponseDTO<OrgBookingDTO> getOrgBookingInfo(SearchParametersDTO search, ResponseDTO<OrgBookingDTO> response) throws DataAccessException 
	{

		List<OrgBookingDTO>  orgBookingDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("bookingInfo");
		String sql = query.getQuery();
		
		if(search.getStartDate() != null && search.getStartDate().trim().length() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND date(b.creation_on) between str_to_date(':startDate','%d-%b-%Y') and str_to_date(':endDate','%d-%b-%Y') &filter_condition");
			sql = sql.replaceAll(":startDate", search.getStartDate());
			sql = sql.replaceAll(":endDate", search.getEndDate());
		}
		
		if(search.getCategoryId() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND cc.course_category_id = :categoryId &filter_condition");
			sql = sql.replaceAll(":categoryId", search.getCategoryId()+"");
		}
		
		if(search.getSubCategoryId() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND sc.course_sub_category_id = :subCategoryId &filter_condition");
			sql = sql.replaceAll(":subCategoryId", search.getSubCategoryId()+"");
		}
		
		if(search.getCourseId() != null && search.getCourseId().size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND c.course_id in (?) &filter_condition");
			sql = setArray(sql, search.getCourseId().toArray(new Integer[search.getCourseId().size()]), 2);
		}
		
		if(search.getBookingSource() != null && search.getBookingSource().size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND b.transaction_source in (?) &filter_condition");
			sql = setList(sql, search.getBookingSource(), 2);
		}
		
		sql = sql.replaceAll("&filter_condition", " ");
		
		
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, search.getOrgId());
			rs = pstmt.executeQuery();
			
			OrgBookingDTO orgBookingDTO = null;
			while(rs.next()) {
				orgBookingDTO =  new OrgBookingDTO();
				orgBookingDTO.setBookedId(rs.getInt("bookedId"));
				orgBookingDTO.setUsername(rs.getString("username"));
				orgBookingDTO.setUserId(rs.getInt("userId"));
				orgBookingDTO.setCourseName(rs.getString("courseName"));
				orgBookingDTO.setStartDate(rs.getString("startDate"));
				orgBookingDTO.setEndDate(rs.getString("endDate"));
				orgBookingDTO.setAmountPaid(rs.getString("price"));
				orgBookingDTO.setPaymentDate(rs.getString("paymentDate"));
				orgBookingDTO.setBookingSource(rs.getString("bookingSource"));
				orgBookingDTOs.add(orgBookingDTO);
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getCourseSchdules method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get course scheules.Please try again");
			return response;
			
		} catch(Exception e){
			log.error("Error occurred in getUserDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get course scheules.Please try again");
			return response;
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		response.setData(orgBookingDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<ScheduleDTO> getScheduleDropList(SearchParametersDTO search, ResponseDTO<ScheduleDTO> response)
			throws DataAccessException {

		List<ScheduleDTO>  scheduleDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("getScheduleDropList");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();

			if(search.getOrgId() > 0)
			{
				sql = sql.replace("&filterCondition", " AND org_id = "+search.getOrgId());
			}else
			{
				sql = sql.replace("&filterCondition", "");
			}
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, search.getCourseId().get(0));
			
			
			rs = pstmt.executeQuery();
			
			ScheduleDTO scheduleDTO = null;
			while(rs.next()) {
				scheduleDTO =  new ScheduleDTO();
				scheduleDTO.setScheduleId(rs.getInt(1));
				scheduleDTO.setSchedule(rs.getString(2));
				scheduleDTO.setPrice(rs.getString(3));
				scheduleDTOs.add(scheduleDTO);
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getScheduleDropList method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getScheduleDropList.Please try again");
			return response;
			
		} catch(Exception e){
			log.error("Error occurred in getSchedules method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to get getScheduleDropList.Please try again");
			return response;
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		response.setData(scheduleDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public ResponseDTO<OrgBookingDTO> getUserBookingInfo(SearchParametersDTO search,
			ResponseDTO<OrgBookingDTO> response) throws DataAccessException {

		List<OrgBookingDTO>  orgBookingDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("userBookingInfo");
		String sql = query.getQuery();
		
		if(search.getStartDate() != null && search.getStartDate().trim().length() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND  b.creation_on between str_to_date(':startDate','%d-%b-%Y') and str_to_date(':endDate','%d-%b-%Y') &filter_condition");
			sql = sql.replaceAll(":startDate", search.getStartDate());
			sql = sql.replaceAll(":endDate", search.getEndDate());
		}
		
		if(search.getCategoryId() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND cc.course_category_id = :categoryId &filter_condition");
			sql = sql.replaceAll(":categoryId", search.getCategoryId()+"");
		}
		
		if(search.getSubCategoryId() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND sc.course_sub_category_id = :subCategoryId &filter_condition");
			sql = sql.replaceAll(":subCategoryId", search.getSubCategoryId()+"");
		}
		
		if(search.getCourseId() != null && search.getCourseId().size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND c.course_id in (?) &filter_condition");
			sql = setArray(sql, search.getCourseId().toArray(new Integer[search.getCourseId().size()]), 2);
		}
		
		if(search.getBookingSource() != null && search.getBookingSource().size() > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND b.transaction_source in (?) &filter_condition");
			sql = setList(sql, search.getBookingSource(), 2);
		}
		
		
		if(search.getTransactionId()  > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND b.transaction_id in (:transactionId) &filter_condition");
			sql = sql.replaceAll(":transactionId", search.getTransactionId()+"");
		}
		
		if(search.getBookingId()  > 0)
		{
			sql = sql.replaceAll("&filter_condition", " AND b.booking_id in (:booking_id) &filter_condition");
			sql = sql.replaceAll(":booking_id", search.getBookingId()+"");
		}
		
		sql = sql.replaceAll("&filter_condition", " ");
		
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, search.getUserId());
			rs = pstmt.executeQuery();
			
			OrgBookingDTO orgBookingDTO = null;
			while(rs.next()) {
				orgBookingDTO =  new OrgBookingDTO();
				orgBookingDTO.setBookedId(rs.getInt("bookedId"));
				orgBookingDTO.setBookingNumber(rs.getString("bookingNumber"));
				orgBookingDTO.setOrgName(rs.getString("org_name"));
				orgBookingDTO.setCourseName(rs.getString("courseName"));
				orgBookingDTO.setStartDate(rs.getString("startDate"));
				orgBookingDTO.setEndDate(rs.getString("endDate"));
				orgBookingDTO.setAmountPaid(rs.getString("price"));
				orgBookingDTO.setTimings(rs.getString("timings"));
				orgBookingDTO.setPaymentDate(rs.getString("paymentDate"));
				orgBookingDTO.setBookingSource(rs.getString("bookingSource"));
				orgBookingDTO.setPaymentDateTime(rs.getString("paymentDateNTime"));
				orgBookingDTO.setUsername(rs.getString("username"));
				orgBookingDTO.setFullName(rs.getString("fullName"));
				orgBookingDTO.setPhone(rs.getString("phone"));
				orgBookingDTO.setOrgEmail(rs.getString("orgEmail"));
				orgBookingDTOs.add(orgBookingDTO);
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getUserBookingInfo method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to getUserBookingInfo .Please try again");
			return response;
			
		} catch(Exception e){
			log.error("Error occurred in getUserBookingInfo method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to getUserBookingInfo.Please try again");
			return response;
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		response.setData(orgBookingDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}

	@Override
	public int createAtomTransactionEntry(AtomPaymentDTO atom) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("atomInsert");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setObject(1, atom.getLogin());
			pstmt.setObject(2, atom.getPassword());
			pstmt.setObject(3, atom.getTtype());
			pstmt.setObject(4, atom.getProdId());
			pstmt.setObject(5, atom.getAmount());
			pstmt.setObject(6, atom.getCurrency());
			pstmt.setObject(7, atom.getTransactionServiceCharge());
			pstmt.setObject(8, atom.getClientCode());
			pstmt.setObject(9, atom.getTransactionId());
			pstmt.setObject(10, atom.getDate());
			pstmt.setObject(11, atom.getCustAcc());
			pstmt.setObject(12, atom.getReturnUrl());
			pstmt.setObject(13, atom.getUdf1());
			pstmt.setObject(14, atom.getUdf2());
			pstmt.setObject(15, atom.getUdf3());
			pstmt.setObject(16, atom.getUdf4());
			pstmt.setObject(17, atom.getContentXML());
			pstmt.setObject(18, atom.getTempTxnId());
			pstmt.setObject(19, atom.getToken());
			pstmt.setObject(20, atom.getTxnStage());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					
					return rs.getInt(1);
					}
				
			}else
			{
				return -1;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return -1;
		
	}

	@Override
	public int updateAtomTransaction(AtomPaymentDTO atom) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("atomTransactionUpdate");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql);
			pstmt.setObject(1, atom.getMmp_txn());
			pstmt.setObject(2, atom.getAmount());
			pstmt.setObject(3, atom.getSurcharge());
			pstmt.setObject(4, atom.getDate());
			pstmt.setObject(5, atom.getBankTransactionId());
			pstmt.setObject(6, atom.getTransactionStatus());
			pstmt.setObject(7, atom.getBankName());
			pstmt.setObject(8, atom.getDiscriminator());
			pstmt.setObject(9, atom.getCardNumber());
			pstmt.setObject(10, atom.getTransactionMsg());
			pstmt.setObject(11, atom.getTransactionId());
			return pstmt.executeUpdate();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		
		
	}

	@Override
	public Map<Integer, List<Integer>> getSchedulesForTransaction(int transactionId) throws DataAccessException {

		Map<Integer, List<Integer>>  map = new HashMap<>();
		Query query = (Query)getQueries().get("getTrasnactionScheudleDetailsandUserId");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, transactionId);
			rs = pstmt.executeQuery();
			
			List<Integer> Schedules = new ArrayList<>();
			int userId = 0;
			while(rs.next()) {
				
				Schedules.add(rs.getInt("schedule_id"));
				userId = rs.getInt("user_id");
			}
			map.put(userId, Schedules);
		}catch (SQLException e) {
			log.error("Error occurred in getSchedulesForTransaction method : "+e.getMessage());
			
			
		} catch(Exception e){
			log.error("Error occurred in getSchedulesForTransaction method : "+e.getMessage());
			
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return map;
	}

	@Override
	public boolean isTransactionUpdated(int transactionId) throws DataAccessException {

		
		Query query = (Query)getQueries().get("getTrasnactionStatus");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, transactionId);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				
				return true;
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getSchedulesForTransaction method : "+e.getMessage());
			
			
		} catch(Exception e){
			log.error("Error occurred in getSchedulesForTransaction method : "+e.getMessage());
			
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		return false;
	}


	
	@Override
	public int createShopPaymentTransaction(TransactionDTO transactionDTO) throws DataAccessException {
		
		Connection con = null;
		PreparedStatement pstmt = null;			
		ResultSet rs = null;
		Query query = (Query)getQueries().get("addShopTransaction");
		String sql = query.getQuery();
		try{
			con = ConnectionManager.instance().getConnection();
			pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			/* pstmt.setObject(1, transactionDTO.getUserId()); */
			pstmt.setObject(1, transactionDTO.getMobileNumber());
			pstmt.setObject(2, transactionDTO.getTotalPrice());
			pstmt.setObject(3, transactionDTO.getServiceTax());
			pstmt.setObject(4, transactionDTO.getInternetHandlingFees());
			pstmt.setObject(5, transactionDTO.getPaymentGateway());
			pstmt.setObject(6, transactionDTO.getTransactionStatus());
			
			
			/* pstmt.setObject(7, transactionDTO.getUserId()); */
			/* pstmt.setObject(8, transactionDTO.getMobileNumber()); */
			  
			 
			pstmt.setObject(7, transactionDTO.getTransactionSource());
			int inserted = pstmt.executeUpdate();
			if(inserted > 0) {
				rs = pstmt.getGeneratedKeys();
				if(rs.next()) {
					
					return rs.getInt(1);
					}
				
			}else
			{
				return -1;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return -1;
		} finally {
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}
		return -1;
		
	}

	@Override
	public ResponseDTO<CourseBookingDTO> getAtomDetails(SearchParametersDTO search,
			ResponseDTO<CourseBookingDTO> response) throws DataAccessException {

		List<CourseBookingDTO>  orgBookingDTOs = new ArrayList<>();
		Query query = (Query)getQueries().get("getAtomDetails");
		String sql = query.getQuery();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.instance().getConnection();
			
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			CourseBookingDTO orgBookingDTO = null;
			while(rs.next()) {
				orgBookingDTO =  new CourseBookingDTO();
				orgBookingDTO.setAtomUsername(rs.getString("atom_user_id"));
				orgBookingDTO.setAtomPassword(rs.getString("atom_password"));
				orgBookingDTO.setAtomProdId(rs.getString("atom_product_id"));
				orgBookingDTO.setAtomProdName(rs.getString("atom_product_name"));
				orgBookingDTO.setRequestHashKey(rs.getString("request_hash_key"));
				orgBookingDTO.setResponseHashKey(rs.getString("response_hash_key"));
				orgBookingDTOs.add(orgBookingDTO);
			}
			
		}catch (SQLException e) {
			log.error("Error occurred in getAtomDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to getAtomDetails .Please try again");
			return response;
			
		} catch(Exception e){
			log.error("Error occurred in getAtomDetails method : "+e.getMessage());
			response.setResponseCode(405);
			response.setSuccess(false);	
			response.setResponseMessage("Unable to getUserBookingInfo.Please try again");
			return response;
			
		} finally{
			ConnectionManager.instance().cleanUp(rs, pstmt, con);
		}		
		
		response.setData(orgBookingDTOs);
		response.setResponseCode(200);
		response.setSuccess(true);
		return response;
	}	
	

}
