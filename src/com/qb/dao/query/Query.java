/* Project  : SurfBI
 * File     : Query.java
 * version	: 1.0
 * Date     : 24-Nov-2010
 * Author   : Satheesh Kumar Kashi
 *
 * Revision History
 * Date		      Release 	   Who		   Description
 * ----           -------      ----        ------------- 
 * 24-Nov-2010    1.0          Satheesh    Initial Version	
 * 
 * Copyright 2010 FCS, Inc. All rights reserved.
 */
package com.qb.dao.query;

public class Query {

	private String name;
    
	private String query = ""; //the query/SQL 
    
	public String getName()	{
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public void appendToQuery(String query)	{
		this.query += query;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query)	{
		this.query = query;
	}
}
