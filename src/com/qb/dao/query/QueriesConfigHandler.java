/* Project  : SurfBI
 * File     : QueriesConfigHandler.java
 * version	: 1.0
 * Date     : 24-Nov-2010
 * Author   : Satheesh Kumar Kashi
 *
 * Revision History
 * Date		      Release 	   Who		   Description
 * ----           -------      ----        ------------- 
 * 24-Nov-2010    1.0          Satheesh    Initial Version	
 * 
 * Copyright 2010 FCS, Inc. All rights reserved.
 */
package com.qb.dao.query;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class QueriesConfigHandler extends DefaultHandler {
	
//	private static final Logger log = Logger.getLogger(QueriesConfigHandler.class.getName());
	
	public Map<String, Map<String,Query>> mDAOs = new java.util.HashMap<String, Map<String,Query>>();
	private Map<String, Query> daoQueries = null;     	// list of queries for current DAO element
	private String daoClass = null;     				// the dao class currently being processed
	private Query query = null;       					// the current query used for xml parsing!
	private boolean startQueryString = false; 			// flag to indicate start of queryString;

	public QueriesConfigHandler() {
		
	}

	public void startElement(String uri, String localName, String element, Attributes attributes) throws SAXException {
								
		if (element.equals("dao"))	{
			daoClass = attributes.getValue("class");
			daoQueries = new HashMap<String, Query>();			
		}
		else if (element.equals("query"))	{
			query = new Query();
			query.setName(attributes.getValue("name"));
		}
		else if (element.equals("sql"))	{
			startQueryString = true;
		}
	}

	public void endElement(String uri, String localName, String element) throws SAXException {
		
		if(element.equals("dao")) {
			
			mDAOs.put(daoClass, daoQueries);
			daoClass = null;
			daoQueries = null;
			
		}
		else if (element.equals("query")) {
			
			if (query != null) 	{
				daoQueries.put(query.getName(),query);
			}
			query = null;
			
		}
		else if (element.equals("sql")) {
			startQueryString = false;
		}
	}

	public void characters(char[] ch, int start, int length) {
		
		if (startQueryString) {
			String queryString = new String(ch, start, length);
			query.appendToQuery(queryString);
		}
		
	}

	

}
