package com.qb.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.qb.dao.jdbc.BookingDAOImpl;
import com.qb.dao.jdbc.CourseDAOImpl;
import com.qb.dao.jdbc.DashboardDAOImpl;
import com.qb.dao.jdbc.LoginDAOImpl;
import com.qb.dao.jdbc.OrganizationDAOImpl;
import com.qb.dao.jdbc.ScheduleDAOImpl;
import com.qb.dao.jdbc.ShopDAOImpl;
import com.qb.dao.query.QueriesConfigHandler;
import com.qb.dao.query.Query;


public class DataAccessFactory {

	private static final Logger log = Logger.getLogger(DataAccessFactory.class.getName());
	private static final DataAccessFactory instance = new DataAccessFactory();
	private QueriesConfigHandler queriesConfig;
	
	
	
	private Properties props = new Properties();
	private String contextPath = null;
	//private Properties dateProps = new Properties();
	
	
	
	
	public void initProperties()
	{
		loadLog4jProperties();
		
		loadProperties();
				
		if(props != null && props.size() > 0)
		{
			
			//workbookDir = props.getProperty(ESCLOUD_WORKBOOKS_DIR);
		}	
	
		loadQueries();
	}
	


	

	private void loadLog4jProperties() {
		
		InputStream stream = null;
		
		if(getContextPath() != null)
		{					
			File log4jFile = new File(getContextPath()+"/conf/log4j.properties");
			if(log4jFile.exists()) {				
				try {
					stream = new FileInputStream(log4jFile);
				} catch (FileNotFoundException e) {				
					System.out.println(e.getMessage());
				}	
			}else {
				System.out.println("Unable to locate log4j.properties file");
			}
					
		}
		
		if(stream == null)
		{
			// Old implementation			
			// for linux support
			stream = DataAccessFactory.class.getClassLoader().getResourceAsStream("../conf/log4j.properties");

			if(stream == null)
			{
			  stream = DataAccessFactory.class.getResourceAsStream("../../../../../conf/log4j.properties"); 
			}
			
			// for weblogic support
		    if (stream == null) {	    	
		    	stream = DataAccessFactory.class.getResourceAsStream("../../../../log4j.properties");
		    }
		}
		
		if(stream != null)
		{	
			Properties log4jProps = new Properties();			
			try {
				log4jProps.load(stream);
				PropertyConfigurator.configure(log4jProps);	
			} catch (IOException e) {
				System.out.println("Unable to load the log4j.properties file"+ e.getMessage());
			}
		}else{			
			System.out.println("Unable to read log4j.properties");			
		}
	}
	
	private void loadProperties() {
		
		InputStream stream = null;
		
		if(getContextPath() != null)
		{					
			File propsFile = new File(getContextPath()+"/conf/QuickBook.properties");			
			try {
				stream = new FileInputStream(propsFile);
			} catch (FileNotFoundException e) {				
				System.out.println(e.getMessage());
			}			
		}
		
		if(stream == null)
		{
			// Old implementation
			
			 stream = DataAccessFactory.class.getClassLoader().getResourceAsStream("../conf/QuickBook.properties");
			 
			 if(stream==null)
			 {
			   stream=DataAccessFactory.class.getResourceAsStream("../../../../../conf/QuickBook.properties");	 
			 }
			
			 // for weblogic support
			 if (stream == null) {
			    	stream = DataAccessFactory.class.getResourceAsStream("../../../../QuickBook.properties");
			 }
		}
		
		if(stream != null)
		{
			try {
				props.load(stream);
			} catch (IOException e) {
				log.error("Unable to load the ESCloud.properties file", e);
			} catch(Exception e){
				log.error("Error occurred in loadProperties method : "+e.getMessage());
				e.printStackTrace();
			}
		}		
	}
	
	private void loadQueries() {
		try {
			InputStream stream = null;
			
			stream = DataAccessFactory.class.getResourceAsStream("queries.xml");
			SAXParserFactory parserFactory = SAXParserFactory.newInstance();
					
			SAXParser sp = parserFactory.newSAXParser();

			queriesConfig = new QueriesConfigHandler();

			sp.parse(stream, queriesConfig);			
			
		}catch (Exception e) {
			log.error("Unable to parse the queries.xml file", e);
		}			
	}
	
	
	
	public static DataAccessFactory instance()
	{
		return instance;
	}
	

	public String getPropValue(String propName) {
		
		if(props != null && props.size() > 0)
		{
			return props.getProperty(propName);
		}
		return "";
	}

	
	private void configureDAO (ABaseDAO dao) 
	{		
		List<String> daoSuperClasses = new ArrayList<String>();
		
		Class<?> cls = dao.getClass();
		String daoClass = cls.getName();		
		daoSuperClasses.add(daoClass);
		
		while((cls = cls.getSuperclass()) != null)
		{
			if(!"java.lang.Object".equals(cls.getName()))
			{
				daoSuperClasses.add(cls.getName());
			}	
		}
		
		log.debug(" retrieving queries for "+daoClass);
		
		// Get all super classes of the class and add all the queries to that
		Map<String, Query> queriesAll = new HashMap<String, Query>();
		
		for (String clsName : daoSuperClasses) {
			Map<String, Query> queries = (Map<String, Query>)queriesConfig.mDAOs.get(clsName);
			if(queries != null && queries.size() > 0)
			{
				queriesAll.putAll(queries);
			}			
		}		
		
		log.debug(" got queries = "+ queriesAll.keySet().size());
		
		dao.setQueries(queriesAll);		
	}

	
	
	public String getProperty(String name)
	{
		return props.getProperty(name);
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	
	
	public ILoginDAO getLoginDAO()
	{
		LoginDAOImpl loginDAO = null;
		loginDAO = new LoginDAOImpl();
		configureDAO(loginDAO);		
		return loginDAO;
	}	
	
	public IOrganizationDAO getOrgDAO()
	{
		OrganizationDAOImpl orgDAO = null;
		orgDAO = new OrganizationDAOImpl();
		configureDAO(orgDAO);		
		return orgDAO;
	}	
	
	public IScheduleDAO getScheduleDAO()
	{
		ScheduleDAOImpl scheduleDAO = null;
		scheduleDAO = new ScheduleDAOImpl();
		configureDAO(scheduleDAO);		
		return scheduleDAO;
	}
	
	public ICourseDAO getCourseDAO()
	{
		CourseDAOImpl courseDAO = null;
		courseDAO = new CourseDAOImpl();
		configureDAO(courseDAO);		
		return courseDAO;
	}
	
	public IBookingDAO getBookingDAO()
	{
		BookingDAOImpl bookingDAO = null;
		bookingDAO = new BookingDAOImpl();
		configureDAO(bookingDAO);		
		return bookingDAO;
	}
	
	public IDashboardDAO getDashboardDAO()
	{
		DashboardDAOImpl dashboardDAO = null;
		dashboardDAO = new DashboardDAOImpl();
		configureDAO(dashboardDAO);		
		return dashboardDAO;
	}
	
	public IShopDAO getShopDAO()
	{
		ShopDAOImpl shopDAO = null;
		shopDAO = new ShopDAOImpl();
		configureDAO(shopDAO);		
		return shopDAO;
	}	
	
	
}
