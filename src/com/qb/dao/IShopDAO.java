package com.qb.dao;

import java.util.List;

import com.qb.dto.CCTransactionDTO;
import com.qb.dto.CreditCustomerDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ShopItemDTO;
import com.qb.dto.ShopTransactionDTO;
import com.qb.dto.ShopUserDTO;
import com.qb.exception.DataAccessException;

public interface IShopDAO {

	public ResponseDTO<ShopItemDTO>  getShopItems(int userId, ResponseDTO<ShopItemDTO> response) throws DataAccessException;
	
	public int updateShopItemCosts(List<ShopItemDTO> shopItemDTOs) throws DataAccessException;
	
	//public int updateShopItemCostsWithImages(ShopItemDTO shopItemDTOs) throws DataAccessException;
	
	public int updateTransaction(List<ShopTransactionDTO> shopItemDTOs, int customerTransactionId) throws DataAccessException;
	
	public ShopUserDTO shopLogin(int userId) throws DataAccessException;

	public ResponseDTO<ShopTransactionDTO> getInventoryTransactions(int userId, String startDate, String endDate, List<String> transactionType, List<Integer> itemIds, ResponseDTO<ShopTransactionDTO> response) throws DataAccessException;
	
	public ResponseDTO<CreditCustomerDTO>  getCCDetails(int userId, ResponseDTO<CreditCustomerDTO> response) throws DataAccessException;
	
	public int  createCreditCustomer(CreditCustomerDTO creditCustomerDTO) throws DataAccessException;
	
	public int  updateCreditCustomerBalance(int customerId, Double amount) throws DataAccessException;
	
	public int  insertCustomerTransaction(CreditCustomerDTO creditCustomerDTO) throws DataAccessException;
	
	public int  updateCustomerTransaction(CreditCustomerDTO creditCustomerDTO) throws DataAccessException;
	
	public ResponseDTO<CCTransactionDTO>  getCCTransactions(int userId, int transactionId,  ResponseDTO<CCTransactionDTO> response) throws DataAccessException;
	
	public int  clearCreditCustomerBalance(int customerId, double credit) throws DataAccessException;

	public ResponseDTO<ShopItemDTO> addNewItem(ResponseDTO<ShopItemDTO> response, ShopItemDTO shopItemDTO) throws  DataAccessException;
	
	public CreditCustomerDTO  getCCDetail(int customerId) throws DataAccessException;
	
	public ResponseDTO<CCTransactionDTO> generatePDF(int userId, int customerId, int transactionId, ResponseDTO<CCTransactionDTO> response) throws DataAccessException;

	public int updateShopItemCostsWithImage(ShopItemDTO shopItemDTOs) throws DataAccessException;

	public ResponseDTO<ShopItemDTO> deleteItem(ResponseDTO<ShopItemDTO> response, ShopItemDTO shopItemDTO) throws DataAccessException;

	public ResponseDTO<CreditCustomerDTO> deleteCustomer(ResponseDTO<CreditCustomerDTO> response, CreditCustomerDTO creditCustomerDTO) throws DataAccessException;

	//public int updateCreditCustomer(CreditCustomerDTO creditCustomerDTO) throws DataAccessException;

	public ResponseDTO<CreditCustomerDTO> updateCreditCustomer(ResponseDTO<CreditCustomerDTO> response,
			CreditCustomerDTO creditCustomerDTO) throws DataAccessException; 
	
	
	public int deleteInventoryTransaction(int customer_transaction_id) throws DataAccessException;
	
	public int deleteCustomerTransaction(int customer_transaction_id) throws DataAccessException;

	public ResponseDTO<CCTransactionDTO> getCCTransactionsWithFilters(int customerId, int transactionId,
			String startDate, String endDate, ResponseDTO<CCTransactionDTO> response) throws DataAccessException;;

	
	
	
}

