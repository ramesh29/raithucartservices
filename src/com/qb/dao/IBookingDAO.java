package com.qb.dao;

import java.util.List;
import java.util.Map;

import com.qb.dto.AtomPaymentDTO;
import com.qb.dto.BookingDTO;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.OrgBookingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ScheduleDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.dto.TransactionDTO;
import com.qb.exception.DataAccessException;

public interface IBookingDAO 
{
	
	public int reserveQuantity(List<Integer> scheduleIds) throws DataAccessException;
	
	public int createTransaction(TransactionDTO transactionDTO) throws DataAccessException;
	
	public int createBooking(int transactionId , List<BookingDTO> bookingDTOs) throws DataAccessException;
	
	public int bookConfirm(List<Integer> scheduleIds) throws DataAccessException;
	
	public int bookingFailed(List<Integer> scheduleIds) throws DataAccessException;
	
	public int updateTransaction(TransactionDTO transactionDTO) throws DataAccessException;
	
	public int updateBooking(TransactionDTO transactionDTO) throws DataAccessException;
	
	public ResponseDTO<OrgBookingDTO> getOrgBookingInfo(SearchParametersDTO search, ResponseDTO<OrgBookingDTO> response) throws DataAccessException;
	
	public ResponseDTO<ScheduleDTO> getScheduleDropList(SearchParametersDTO search, ResponseDTO<ScheduleDTO> response) throws DataAccessException;

	public ResponseDTO<OrgBookingDTO> getUserBookingInfo(SearchParametersDTO search, ResponseDTO<OrgBookingDTO> response) throws DataAccessException;
	
	public int createAtomTransactionEntry(AtomPaymentDTO atom) throws DataAccessException;
	
	public int updateAtomTransaction(AtomPaymentDTO atom) throws DataAccessException;
	
	public Map<Integer, List<Integer>> getSchedulesForTransaction(int transactionId) throws DataAccessException;
	
	public boolean isTransactionUpdated(int transactionId) throws DataAccessException;

	public int createShopPaymentTransaction(TransactionDTO transactionDTO) throws DataAccessException;
	
	public ResponseDTO<CourseBookingDTO> getAtomDetails(SearchParametersDTO search, ResponseDTO<CourseBookingDTO> response) throws DataAccessException;
	
	
}
 