package com.qb.dao;

import java.util.List;

import com.qb.dto.CourseBookingDTO;
import com.qb.dto.CourseMappingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.SearchDropListsDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.exception.DataAccessException;

public interface ICourseDAO {

	public ResponseDTO<SearchDropListsDTO> getSearchDropDowns(ResponseDTO<SearchDropListsDTO> response) throws DataAccessException;
	
	public ResponseDTO<CourseMappingDTO> getNewSearchDropDowns(ResponseDTO<CourseMappingDTO> response) throws DataAccessException;

	public ResponseDTO<CourseBookingDTO> getCourseSchdules(SearchParametersDTO search, int categoryId,
			int subCategoryId,
			List<Integer> courseId,
			List<Integer> instituteId,
			List<String> cities,
			List<Integer> schdules,
			String startDate,
			String endDate,
			String availablility, 
			List<Integer> courseCode,
			ResponseDTO<CourseBookingDTO> response) throws DataAccessException;


}
