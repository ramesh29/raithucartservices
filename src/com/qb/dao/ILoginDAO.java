package com.qb.dao;

import com.qb.dto.PasswordDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ShopUserDTO;
import com.qb.dto.UserDTO;
import com.qb.exception.DataAccessException;

public interface ILoginDAO {
	
	public UserDTO checkUserNameExists(String userName,int orgId) throws DataAccessException;
	
	public UserDTO registerUser(UserDTO userdto,String userType) throws DataAccessException;
	
	public UserDTO CreateorUpdatePassword(PasswordDTO passwordDTO) throws DataAccessException;
	
	public UserDTO authenticate(String userName,String password) throws DataAccessException;
	
	public UserDTO updateUser(UserDTO userdto,String userType) throws DataAccessException;
	
	public UserDTO getUser(int userId, String indosNo) throws DataAccessException;
	
	public UserDTO getAdminUser(int orgId) throws DataAccessException;
	
	public UserDTO updateAdmin(UserDTO userdto) throws DataAccessException;
	
	public ResponseDTO<UserDTO> getUsers(ResponseDTO<UserDTO> response) throws DataAccessException;
	
	
	//Shop Services
	
	public ShopUserDTO checkShopUserExists(String mobileNumber) throws DataAccessException;
	
	public ShopUserDTO registerShopUser(ShopUserDTO userdto) throws DataAccessException;
	
	public ShopUserDTO shopLogin(String mobileNumber,String password) throws DataAccessException;

	public ShopUserDTO updateShop(ShopUserDTO userdto) throws DataAccessException;
	
	public ShopUserDTO renewShopUser(ShopUserDTO userdto) throws DataAccessException;

	public ShopUserDTO updatePassword(PasswordDTO passwordDTO) throws DataAccessException;

	public ShopUserDTO confirmPassword(PasswordDTO passwordDTO) throws DataAccessException;

	public ShopUserDTO updateShopDeviceUUID(ShopUserDTO userdto) throws DataAccessException;
	
	

}
 