package com.qb.dao;

import com.qb.dto.OrgAccountDTO;
import com.qb.dto.OrgCourseDTO;
import com.qb.dto.OrgDTO;
import com.qb.dto.ResponseDTO;
import com.qb.exception.DataAccessException;

public interface IOrganizationDAO {
	
	public ResponseDTO<OrgDTO> getOrgs(ResponseDTO<OrgDTO> response) throws DataAccessException;
	
	public ResponseDTO<OrgDTO> getOrg(ResponseDTO<OrgDTO> response, int orgId) throws DataAccessException;
	
	public ResponseDTO<OrgAccountDTO> getOrgAccounts(ResponseDTO<OrgAccountDTO> response,int orgId) throws DataAccessException;
	
	public ResponseDTO<OrgDTO> addOrg(ResponseDTO<OrgDTO> response,OrgDTO orgDTO) throws DataAccessException;
	
	public ResponseDTO<OrgAccountDTO> addOrgAccount(ResponseDTO<OrgAccountDTO> response,OrgAccountDTO orgDTO) throws DataAccessException;
	
	public ResponseDTO<OrgDTO> updateOrg(ResponseDTO<OrgDTO> response,OrgDTO orgDTO) throws DataAccessException;
	
	public ResponseDTO<OrgAccountDTO> updateOrgAccount(ResponseDTO<OrgAccountDTO> response,OrgAccountDTO orgaccountDTO) throws DataAccessException;
	
	public ResponseDTO<OrgDTO> inactivateOrg(ResponseDTO<OrgDTO> response,OrgDTO orgDTO) throws DataAccessException;
	
	public ResponseDTO<OrgDTO> deleteOrg(ResponseDTO<OrgDTO> response,OrgDTO orgDTO) throws DataAccessException;
	
	public ResponseDTO<OrgCourseDTO> getOrgCourses(ResponseDTO<OrgCourseDTO> response, int orgId) throws DataAccessException;
	
	public ResponseDTO<OrgCourseDTO> addOrgCourse(ResponseDTO<OrgCourseDTO> response,OrgCourseDTO orgDTO) throws DataAccessException;
	
	public ResponseDTO<OrgCourseDTO> updateOrgCourse(ResponseDTO<OrgCourseDTO> response,OrgCourseDTO orgDTO) throws DataAccessException;

}
