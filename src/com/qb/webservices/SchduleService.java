package com.qb.webservices;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.ICourseDAO;
import com.qb.dao.IScheduleDAO;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ScheduleDTO;
import com.qb.dto.SearchParametersDTO;


@Path("/scheduleService")
public class SchduleService extends ValidateUserRequests{
	
	ICourseDAO courseDAO = DataAccessFactory.instance().getCourseDAO();
	
	IScheduleDAO scheduleDAO = DataAccessFactory.instance().getScheduleDAO();
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/add-schedule")
	public ResponseDTO<ScheduleDTO> addSchedule(ScheduleDTO scheduleDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<ScheduleDTO> response = new ResponseDTO<ScheduleDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return scheduleDAO.addSchdule(response, scheduleDTO);
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/edit-schedule")
	public ResponseDTO<ScheduleDTO> editSchedule(ScheduleDTO scheduleDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<ScheduleDTO> response = new ResponseDTO<ScheduleDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return scheduleDAO.updateSchdule(response, scheduleDTO);
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/delete-schedule")
	public ResponseDTO<ScheduleDTO> deleteSchedule(ScheduleDTO scheduleDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<ScheduleDTO> response = new ResponseDTO<ScheduleDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return scheduleDAO.deleteSchdule(response, scheduleDTO);
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getCourseSchdules")
	public ResponseDTO<CourseBookingDTO> getOrgCourseData( String json,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<CourseBookingDTO> response = new ResponseDTO<CourseBookingDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		return scheduleDAO.getOrgCourseSchdules(search, response);
	}

}
