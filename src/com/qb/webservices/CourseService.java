package com.qb.webservices;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.ICourseDAO;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.CourseMappingDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.SearchDropListsDTO;
import com.qb.dto.SearchParametersDTO;

@Path("/courseService")
public class CourseService extends ValidateUserRequests{
	
	ICourseDAO courseDAO = DataAccessFactory.instance().getCourseDAO();
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getNewSearchDropDowns")
	public ResponseDTO<CourseMappingDTO> getCoursesFromMapper(@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<CourseMappingDTO> response = new ResponseDTO<CourseMappingDTO>();
		/*if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}*/
		//System.out.println("Called Search Drop Down");
		return courseDAO.getNewSearchDropDowns(response);
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getSearchDropDowns")
	public ResponseDTO<SearchDropListsDTO> getOrgs(@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<SearchDropListsDTO> response = new ResponseDTO<SearchDropListsDTO>();
		/*if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}*/
		//System.out.println("Called Search Drop Down");
		return courseDAO.getSearchDropDowns(response);
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getCourseSchdules")
	public ResponseDTO<CourseBookingDTO> getCourseData( String json,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<CourseBookingDTO> response = new ResponseDTO<CourseBookingDTO>();
		/*if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}*/
		
		//System.out.println("Called Course List");
		return courseDAO.getCourseSchdules(search, search.getCategoryId(), search.getSubCategoryId(), search.getCourseId(), search.getInstituteId(), search.getCities()
				,search.getSchdules(), search.getStartDate(), search.getEndDate(),search.getAvailability(), search.getCourseCode(), response);
	}
	
	
	
	
}
