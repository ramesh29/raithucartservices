package com.qb.webservices;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.IOrganizationDAO;
import com.qb.dto.OrgAccountDTO;
import com.qb.dto.OrgCourseDTO;
import com.qb.dto.OrgDTO;
import com.qb.dto.ResponseDTO;

@Path("/orgService")
public class OrganizationService extends ValidateUserRequests{
	
	IOrganizationDAO orgDAO = DataAccessFactory.instance().getOrgDAO();
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getOrgs")
	public ResponseDTO<OrgDTO> getOrgs(@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgDTO> response = new ResponseDTO<OrgDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.getOrgs(response);
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getOrg/{orgId}")
	public ResponseDTO<OrgDTO> getOrg(@PathParam("orgId") int orgId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgDTO> response = new ResponseDTO<OrgDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.getOrg(response,orgId);
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/add-org")
	public ResponseDTO<OrgDTO> addOrg(OrgDTO orgDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgDTO> response = new ResponseDTO<OrgDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.addOrg(response, orgDTO);
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/add-org-account")
	public ResponseDTO<OrgAccountDTO> addOrgAccount(OrgAccountDTO orgDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgAccountDTO> response = new ResponseDTO<OrgAccountDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.addOrgAccount(response, orgDTO);
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/upadate-org")
	public ResponseDTO<OrgDTO> upadateOrg(OrgDTO orgDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgDTO> response = new ResponseDTO<OrgDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.updateOrg(response, orgDTO);
		
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getOrgAccount/{orgId}")
	public ResponseDTO<OrgAccountDTO> getOrgAccount(@PathParam("orgId") int orgId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgAccountDTO> response = new ResponseDTO<OrgAccountDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.getOrgAccounts(response,orgId);
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/upadate-org-account")
	public ResponseDTO<OrgAccountDTO> upadateOrgAccount(OrgAccountDTO orgDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgAccountDTO> response = new ResponseDTO<OrgAccountDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.updateOrgAccount(response, orgDTO);
		
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/invalidateOrg")
	public ResponseDTO<OrgDTO> invalidateOrg(OrgDTO orgDTO,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgDTO> response = new ResponseDTO<OrgDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.inactivateOrg(response,orgDTO);
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/deleteOrg")
	public ResponseDTO<OrgDTO> deleteOrg(OrgDTO orgDTO,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgDTO> response = new ResponseDTO<OrgDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.deleteOrg(response,orgDTO);
	}
	
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getOrgCourses/{orgId}")
	public ResponseDTO<OrgCourseDTO> getOrgCourses(@PathParam("orgId") int orgId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgCourseDTO> response = new ResponseDTO<OrgCourseDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.getOrgCourses(response,orgId);
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/add-org-course")
	public ResponseDTO<OrgCourseDTO> addOrgCourse(OrgCourseDTO orgDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgCourseDTO> response = new ResponseDTO<OrgCourseDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.addOrgCourse(response, orgDTO);
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/update-org-course")
	public ResponseDTO<OrgCourseDTO> updateOrgCourse(OrgCourseDTO orgDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<OrgCourseDTO> response = new ResponseDTO<OrgCourseDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return orgDAO.addOrgCourse(response, orgDTO);
		
	}
	

}
