package com.qb.webservices;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.IDashboardDAO;
import com.qb.dto.DashboardDTO;
import com.qb.dto.DashboardFinalDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.SearchParametersDTO;

@Path("/dashboardService")
public class DashboardService extends ValidateUserRequests{
	
	IDashboardDAO dashboardDAO = DataAccessFactory.instance().getDashboardDAO();
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getDashboardInfo")
	public ResponseDTO<DashboardFinalDTO> getSchedulesDropList( String json,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<DashboardFinalDTO> response = new ResponseDTO<DashboardFinalDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		
		List<DashboardDTO> onlineBookings = dashboardDAO.getBookingInfo(search.getOrgId(), search.getStartDate(), search.getEndDate(), "ONLINE");
		
		List<DashboardDTO> directBookings = dashboardDAO.getBookingInfo(search.getOrgId(), search.getStartDate(), search.getEndDate(), "DIRECT");
		
		List<DashboardDTO> upcoming = dashboardDAO.getScheduleInfo(search.getOrgId(), "upcoming");
		
		List<DashboardDTO> ongoing = dashboardDAO.getScheduleInfo(search.getOrgId(), "ongoing");
		
		DashboardFinalDTO dashboard = new DashboardFinalDTO();
		
		dashboard.setDirectBookings(directBookings);
		dashboard.setOngoing(ongoing);
		dashboard.setOnlineBookings(onlineBookings);
		dashboard.setUpcoming(upcoming);
		
		List<DashboardFinalDTO> finalDashboard =  new ArrayList<>();
		finalDashboard.add(dashboard);
		response.setData(finalDashboard);
		response.setResponseCode(200);
		response.setSuccess(true);	
		response.setResponseMessage("Data");
		
		return response;
	}

}
