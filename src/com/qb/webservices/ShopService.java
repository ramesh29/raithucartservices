package com.qb.webservices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.IShopDAO;
import com.qb.dao.jdbc.ShopDAOImpl;
import com.qb.dto.CCTransactionDTO;
import com.qb.dto.CreditCustomerDTO;
import com.qb.dto.PasswordDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.dto.ShopItemDTO;
import com.qb.dto.ShopTransactionDTO;
import com.qb.dto.ShopUserDTO;
import com.qb.util.IPaymentGatewayURLS;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;


@Path("/shopService")
public class ShopService extends ValidateUserRequests{
	
	private static final Logger log = Logger.getLogger(ShopService.class.getName());
	
	IShopDAO shopDAO = DataAccessFactory.instance().getShopDAO();
	
	  @Context 
		private ServletContext context;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getShopItems/{id}")
	public ResponseDTO<ShopItemDTO> getShopItems(@PathParam("id") int id,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<ShopItemDTO> response = new ResponseDTO<ShopItemDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		return shopDAO.getShopItems(id, response);
		
	}
	
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/addNewItem")
	public ResponseDTO<ShopItemDTO> addNewItem(ShopItemDTO ShopItemDTO, @Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<ShopItemDTO> response = new ResponseDTO<ShopItemDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return shopDAO.addNewItem(response, ShopItemDTO);
	}

	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/showImage/{userId}/{imageName}")
	public byte[]  showImage(@PathParam("userId") int userId,@PathParam("imageName") String name, @Context HttpServletRequest request, @Context HttpServletResponse response,@Context HttpHeaders httpHeaders) throws Exception{

		String fileName = IPaymentGatewayURLS.extDir +userId+"/"+name;
		File file = new File(fileName);
		if(file.exists())
		{
			 	String contentType = "application/octet-stream";
		        response.setContentType(contentType);
		        FileInputStream in = new FileInputStream(file);
		        return IOUtils.toByteArray(in);
		}else if(new File(IPaymentGatewayURLS.extDir+"/"+name).exists())
		{
			file = new File(IPaymentGatewayURLS.extDir+"/"+name);
			String contentType = "application/octet-stream";
	        response.setContentType(contentType);
	        FileInputStream in = new FileInputStream(file);
	        return IOUtils.toByteArray(in);
		}
		else
		{
			file = new File(request.getSession().getServletContext().getRealPath("/CustomImages/no-image-available.jpg"));
			String contentType = "application/octet-stream";
	        response.setContentType(contentType);
	        FileInputStream in = new FileInputStream(file);
	        return IOUtils.toByteArray(in);
		}
	}
	

	
	
	@POST
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/addNewItemWithUpload")
	public ResponseDTO<ShopItemDTO> addNewItemUpload(@FormDataParam("file") InputStream uploadedInputStream,  
			@FormDataParam("file") FormDataContentDisposition fileDetail,@FormDataParam("shopItemDTO") String shop,@FormDataParam("userId") String userId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{
		
		ResponseDTO<ShopItemDTO> response = new ResponseDTO<ShopItemDTO>();
		ShopItemDTO shopItemDTO = new ObjectMapper().readValue(shop, ShopItemDTO.class);
		String fileLocation = IPaymentGatewayURLS.extDir +shopItemDTO.getUserId();

		File directory = new File(fileLocation);
		if (! directory.exists()){
			directory.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
		}

		String fileName = shopItemDTO.getItemName().replaceAll(" ", "_")+"_"+fileDetail.getFileName().replaceAll(" ", "_");
		shopItemDTO.setImageName(fileName);
		//fileLocation = req.getServletContext().getRealPath(fileLocation)+"\\"+fileName;
		 
		 try {  
             FileOutputStream out = new FileOutputStream(new File(fileLocation+"/"+fileName));  
             int read = 0;  
             byte[] bytes = new byte[1024];  
             out = new FileOutputStream(new File(fileLocation+"/"+fileName));  
             while ((read = uploadedInputStream.read(bytes)) != -1) {  
                 out.write(bytes, 0, read);  
             }  
             out.flush();  
             out.close();  
         } catch (IOException e) {e.printStackTrace();}  
         //String output = "File successfully uploaded to : " + fileLocation;  
         //System.out.println(output);
         
		
         return shopDAO.addNewItem(response, shopItemDTO);
	}
	
	
	
	
	
	@POST
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/updateCostWithImage")
	public ResponseDTO<ShopItemDTO> updateShopItemWithImage( @FormDataParam("file") InputStream uploadedInputStream,  
			@FormDataParam("file") FormDataContentDisposition fileDetail,@FormDataParam("shopItemDTO") String shop,@FormDataParam("userId") String userId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<ShopItemDTO> response = new ResponseDTO<ShopItemDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		ShopItemDTO shopItemDTO = new ObjectMapper().readValue(shop, ShopItemDTO.class);
		String fileLocation = IPaymentGatewayURLS.extDir +shopItemDTO.getUserId();
		//String fileLocation = IPaymentGatewayURLS.extDir +shopItemDTO.getUserId();

		File directory = new File(fileLocation);
		if (! directory.exists()){
			directory.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
		}

		String fileName = shopItemDTO.getItemName().replaceAll(" ", "_")+"_"+fileDetail.getFileName().replaceAll(" ", "_");
		shopItemDTO.setImageName(fileName);
		//fileLocation = req.getServletContext().getRealPath(fileLocation)+"\\"+fileName;
		 
		 try {  
             FileOutputStream out = new FileOutputStream(new File(fileLocation+"/"+fileName));  
             int read = 0;  
             byte[] bytes = new byte[1024];  
             out = new FileOutputStream(new File(fileLocation+"/"+fileName));  
             while ((read = uploadedInputStream.read(bytes)) != -1) {  
                 out.write(bytes, 0, read);  
             }  
             out.flush();  
             out.close();  
         } catch (IOException e) {e.printStackTrace();}  
         //String output = "File successfully uploaded to : " + fileLocation;  
         //System.out.println(output);
		
		
		
		
		//Update History and 
		
		int count = shopDAO.updateShopItemCostsWithImage(shopItemDTO);
		if(count > 0)
		{
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage("Cost updated successfully");
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Cost updated failed");
		}
		
		return response;
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/updateCost")
	public ResponseDTO<ShopItemDTO> updateShopItem( List<ShopItemDTO> shopItemDTOs,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<ShopItemDTO> response = new ResponseDTO<ShopItemDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		//Update History and 
		
		int count = shopDAO.updateShopItemCosts(shopItemDTOs);
		if(count > 0)
		{
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage("Cost updated successfully");
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Cost updated failed");
		}
		
		return response;
		
	}
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/updateTransaction")
	public ResponseDTO<ShopTransactionDTO> updateTransaction( List<ShopTransactionDTO> shopTransactionDTOs,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<ShopTransactionDTO> response = new ResponseDTO<ShopTransactionDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		//Update History and 
		
		Double transactionAmount = 0.0;
		Double paidAmount = 0.0;
		Double discount = 0.0;
		Double previousCredit = 0.0;
		int customerTransactionId =  0;
		int id = 0;
		int isCustomer = 0;
		String status = null;
		List<ShopItemDTO> shopItems = new ArrayList<>();
		int userId=0;
		for(ShopTransactionDTO shopTransactionDTO : shopTransactionDTOs)
		{
			if(!shopTransactionDTO.getItemPurchaseCost().equals(shopTransactionDTO.getNewPurchaseCost()) || !shopTransactionDTO.getItemSoldCost().equals(shopTransactionDTO.getNewSoldCost()))
			{
				ShopItemDTO shopItem = new ShopItemDTO();
				shopItem.setId(shopTransactionDTO.getId());
				shopItem.setItemId(shopTransactionDTO.getItemId());
				shopItem.setItemName(shopTransactionDTO.getItemName());
				shopItem.setItemDesc(shopTransactionDTO.getItemDesc());
				shopItem.setItemUOM(shopTransactionDTO.getItemUOM());
				shopItem.setUserId(shopTransactionDTO.getUserId());
				shopItem.setItemPurchaseCost(shopTransactionDTO.getItemPurchaseCost());
				shopItem.setItemSoldCost(shopTransactionDTO.getItemSoldCost());
				shopItem.setNewPurchaseCost(shopTransactionDTO.getNewPurchaseCost());
				shopItem.setNewSoldCost(shopTransactionDTO.getNewSoldCost());
				shopItem.setGstValue(shopTransactionDTO.getGstValue());
				shopItem.setUpcCode(shopTransactionDTO.getUpcCode());
				shopItems.add(shopItem);
			}
			
			if(shopTransactionDTO.getCustomerId() > 0)
			{
				isCustomer = shopTransactionDTO.getCustomerId();
				//transactionAmount+=(shopTransactionDTO.getItemSoldCost()*shopTransactionDTO.getNewQty());
				status = shopTransactionDTO.getStatus();
				transactionAmount = shopTransactionDTO.getTransactionAmount();
				paidAmount = shopTransactionDTO.getPaidAmount();
				discount = shopTransactionDTO.getDiscount();
				//discount = 0.00;
				id = shopTransactionDTO.getId();
				previousCredit = shopTransactionDTO.getOldCredit();
				userId = shopTransactionDTO.getUserId();
				customerTransactionId = shopTransactionDTO.getCustomerTransactionId();
			}
		}
		
		
		
		if(isCustomer > 0)
		{
			double prevPaidAmount=0.0;
			if(customerTransactionId>0)
			{
				// get Customer Transaction Details
				ResponseDTO<CCTransactionDTO> response1 = new ResponseDTO<CCTransactionDTO>();
				
				
				ResponseDTO<CCTransactionDTO> response2 = shopDAO.getCCTransactions(isCustomer, customerTransactionId, response1);
				List<CCTransactionDTO> transactionDetails = response2.getData();
				if(transactionDetails != null && transactionDetails.size()>0)
				{
					CCTransactionDTO ccDTO =  transactionDetails.get(0);
					prevPaidAmount =  ccDTO.getCc_paid_amount();
					//paidAmount = ccDTO.getCc_paid_amount()-paidAmount;
				}
				
				//Delete Old Customer Transaction and Inventory Transaction
				shopDAO.deleteInventoryTransaction(customerTransactionId);
				shopDAO.deleteCustomerTransaction(customerTransactionId);
			}
			
			CreditCustomerDTO ccd = new CreditCustomerDTO();
			
			ccd.setCc_id(isCustomer);
			ccd.setStatus(status);
			ccd.setCc_total_amount(transactionAmount);
			ccd.setCc_paid_amount(paidAmount);
			ccd.setCc_credit_amount(transactionAmount-paidAmount-discount);
			ccd.setCc_discount(discount);
			ccd.setCc_prev_credit_amount(previousCredit);
			ccd.setUserId(userId);
			ccd.setId(id);
			//if(customerTransactionId>0)
			/*{
				ccd.setCustomerTransactionId(customerTransactionId);
				shopDAO.updateCustomerTransaction(ccd);
			}else*/
			customerTransactionId = shopDAO.insertCustomerTransaction(ccd); 
			
			
			
			if(customerTransactionId>0)
			{
				shopDAO.updateCreditCustomerBalance(isCustomer, transactionAmount-prevPaidAmount);
			}
		}
		
		if(!shopItems.isEmpty() && isCustomer <=0)
		{
			int count = shopDAO.updateShopItemCosts(shopItems);
			if(count > 0)
			{
				log.info("Cost Update Successful");
			}else
			{
				log.info("Cost Update Failed");
			}
		}
		
		
		
		int count = shopDAO.updateTransaction(shopTransactionDTOs, customerTransactionId);
		if(isCustomer>0)
		{
			ResponseDTO<CCTransactionDTO> response1 = new ResponseDTO<>(); 
			shopDAO.generatePDF(userId, isCustomer, customerTransactionId, response1);	
		}
		
		if(count > 0)
		{
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage(userId+"/"+isCustomer+"/"+customerTransactionId);
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Transaction update failed");
		}
		
		return response;
		
	}
	
	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/shopLogin/{id}")
	public ShopUserDTO shopLogin(@PathParam("id") int userId,@Context HttpServletRequest req, @Context HttpHeaders httpHeaders) throws Exception{

		
		/*if(!validateAppKey(req,httpHeaders))
		{
			userDTO.setSuccess(false);
			userDTO.setMessege("Session invalid.Please try again");
			return userDTO;
			
		}else
		{}*/

		return shopDAO.shopLogin(userId);
	
		
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getInventoryTransactions")
	public ResponseDTO<ShopTransactionDTO> getInventoryTransactions( String json,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<ShopTransactionDTO> response = new ResponseDTO<ShopTransactionDTO>();
		/*if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}*/
		
		//System.out.println("Called Course List");
		return shopDAO.getInventoryTransactions(search.getUserId(), search.getStartDate(), search.getEndDate(), search.getTransactionType(), search.getItemIds(), response);
	}
	
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getCreditCustomers/{id}")
	public ResponseDTO<CreditCustomerDTO> getCreditCustomers( @PathParam("id") int userId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<CreditCustomerDTO> response = new ResponseDTO<CreditCustomerDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return shopDAO.getCCDetails(userId, response);
		
	}
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/createCreditCustomer")
	public ResponseDTO<CreditCustomerDTO> createCreditCustomer( CreditCustomerDTO creditCustomerDTO,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<CreditCustomerDTO> response = new ResponseDTO<CreditCustomerDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		int count = shopDAO.createCreditCustomer(creditCustomerDTO);
		if(count > 0)
		{
			List<CreditCustomerDTO> ccDetails = new ArrayList<>();
			CreditCustomerDTO ccDTO = new CreditCustomerDTO();
			ccDTO.setCc_id(count);
			ccDetails.add(ccDTO);
			response.setData(ccDetails);
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage("Customer created successfully");
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Customer already exists");
		}
		
		return response;
		
	}
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/clearCredit/{id}/{credit}/{previousCredit}")
	public ResponseDTO<CreditCustomerDTO> clearCredit(  @PathParam("id") int customerId, @PathParam("credit") double credit,@PathParam("previousCredit") double previousCredit,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<CreditCustomerDTO> response = new ResponseDTO<CreditCustomerDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		if(customerId > 0)
		{
			CreditCustomerDTO ccd = new CreditCustomerDTO();
			ccd.setCc_id(customerId);
			ccd.setStatus("Credit");
			ccd.setCc_total_amount(0);
			ccd.setCc_paid_amount(credit);
			ccd.setCc_credit_amount(-1 * credit);
			ccd.setCc_prev_credit_amount(previousCredit);
			
			int count = shopDAO.insertCustomerTransaction(ccd); 
			if(count > 0)
			{
				response.setResponseCode(400);
				response.setSuccess(true);	
				response.setResponseMessage("Credit cleared successfully");
			}else
			{
				response.setResponseCode(100);
				response.setSuccess(false);	
				response.setResponseMessage("Credit clear failed");
			}
		}
		
		
		
	
		return response;
		
	}
	
	/*@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/clearCredit/{id}/{credit}/{creditState}")
	public ResponseDTO<CreditCustomerDTO> clearCredit(  @PathParam("id") int customerId, @PathParam("creditState") String creditState,@PathParam("credit") double credit,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		
		ResponseDTO<CreditCustomerDTO> response = new ResponseDTO<CreditCustomerDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		if(customerId > 0)
		{
			CreditCustomerDTO ccd = new CreditCustomerDTO();
			ccd.setCc_id(customerId);
			ccd.setStatus("Credit");
			ccd.setCc_total_amount(0);
			ccd.setCc_paid_amount(credit);
			ccd.setCc_credit_amount(-1 * credit);
			ccd.setCreditState(creditState);
			
			
			
				int count = shopDAO.insertCustomerTransaction(ccd); 
				if(count > 0)
				{
					response.setResponseCode(400);
					response.setSuccess(true);	
					response.setResponseMessage("Credit cleared successfully");
				}else
				{
					response.setResponseCode(100);
					response.setSuccess(false);	
					response.setResponseMessage("Credit clear failed");
				}
			
			int count = 0;
			if(creditState.equalsIgnoreCase("creditCancel")){
				 count = shopDAO.insertCustomerTransaction(ccd,creditState); 
			}
			else
			{
				 count = shopDAO.insertCustomerTransaction(ccd); 
			}
			if(count > 0)
			{
				response.setResponseCode(400);
				response.setSuccess(true);	
				response.setResponseMessage("Credit cleared successfully");
			}else
			{
				response.setResponseCode(100);
				response.setSuccess(false);	
				response.setResponseMessage("Credit clear failed");
			}
			
		}
	
		return response;
		
	}*/
	
	/*@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getCreditCustomerTransactions/{id}")
	public ResponseDTO<CCTransactionDTO> getCreditCustomerTransactionDetails( @PathParam("id") int customerId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception
	{
		ResponseDTO<CCTransactionDTO> response = new ResponseDTO<CCTransactionDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		//return shopDAO.getCCTransactionsWithFilters(search.getCustomerId(), 0,search.getStartDate(), search.getEndDate(), response);

		return shopDAO.getCCTransactions(customerId,0, response);
	}*/
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getCreditCustomerTransactions")
	public ResponseDTO<CCTransactionDTO> getCreditCustomerTransactionDetails( String json,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception
	{
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<CCTransactionDTO> response = new ResponseDTO<CCTransactionDTO>();
		
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		return shopDAO.getCCTransactionsWithFilters(search.getCustomerId(), 0,search.getStartDate(), search.getEndDate(), response);
		
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/generatePDF/{userId}/{customerId}/{id}")
	public ResponseDTO<CCTransactionDTO> generatePDF(@PathParam("userId") int userId,@PathParam("customerId") int customerId, @PathParam("id") int transactionId,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception
	{
		ResponseDTO<CCTransactionDTO> response = new ResponseDTO<CCTransactionDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		return shopDAO.generatePDF(userId, customerId, transactionId, response);
		
	}
	
	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces("application/pdf")
	@Path("/showPDF/{userId}/{customerId}/{id}")
	public Response   showPDF(@PathParam("userId") int userId,@PathParam("customerId") int customerId, @PathParam("id") int transactionId, @Context HttpServletRequest request, @Context HttpServletResponse response1,@Context HttpHeaders httpHeaders) throws Exception{

		String fileName = IPaymentGatewayURLS.extDir +userId+"/PDF/"+customerId+"/"+transactionId+".pdf";;
		File file = new File(fileName);
		if(file.exists())
		{

			ResponseBuilder response = Response.ok((Object) file);
	        response.header("Content-Disposition",
	            "attachment; filename=\"" +transactionId+ ".pdf\"");
	        return response.build();
		}
		return null;
		
	}	
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/deleteItem")
	public ResponseDTO<ShopItemDTO> deleteItem(ShopItemDTO shopItemDTO,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<ShopItemDTO> response = new ResponseDTO<ShopItemDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		return shopDAO.deleteItem(response,shopItemDTO);
	}
	
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/deleteCustomer")
	public ResponseDTO<CreditCustomerDTO> deleteCustomer(CreditCustomerDTO creditCustomerDTO,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<CreditCustomerDTO> response = new ResponseDTO<CreditCustomerDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		return shopDAO.deleteCustomer(response,creditCustomerDTO);
	}
	
	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/updateCreditCustomer")
	public ResponseDTO<CreditCustomerDTO> updateCreditCustomer(CreditCustomerDTO creditCustomerDTO,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception {

		
		ResponseDTO<CreditCustomerDTO> response = new ResponseDTO<CreditCustomerDTO>();
		
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		
		return shopDAO.updateCreditCustomer(response,creditCustomerDTO);
		//int count = shopDAO.updateCreditCustomer(response,creditCustomerDTO);
		/*if(count > 0)
		{
			List<CreditCustomerDTO> ccDetails = new ArrayList<>();
			CreditCustomerDTO ccDTO = new CreditCustomerDTO();
			ccDTO.setCc_id(count);
			ccDetails.add(ccDTO);
			response.setData(ccDetails);
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage("Customer updated successfully");
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Customer update failed");
		}
		
		return response;*/
	}
	
	

}
