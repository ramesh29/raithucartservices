package com.qb.webservices;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import com.qb.dto.ResponseDTO;
import com.qb.dto.SupportDTO;
import com.qb.util.SendMailSSL;

@Path("/supportService")
public class SupportService {
	
	

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/sendSupportEmail")
	public ResponseDTO<SupportDTO> sendSupportEmail(SupportDTO support,@Context HttpServletRequest req,@Context HttpHeaders httpHeaders) throws Exception{

		ResponseDTO<SupportDTO> response = new ResponseDTO<SupportDTO>();
		
		StringBuilder contentBuilder = new StringBuilder();
		try {
			;
			
		    BufferedReader in = new BufferedReader(new FileReader(req.getServletContext().getRealPath("EmailTemplates/Support.html")));
		    String str;
		    while ((str = in.readLine()) != null) {
		    	str = str.replace(":name", support.getName());
		    	str = str.replace(":phone", support.getPhone());
		    	str = str.replace(":email", support.getEmail());
		    	str = str.replace(":content", support.getComment());
		    	
		        contentBuilder.append(str);
		    }
		    in.close();
		    String content = contentBuilder.toString();
			
			
			SendMailSSL.sendEmail("support@mymarinecourse.com","Support Request", content);
			
			
			

		} catch (IOException e) {
			
		}
		
		
		contentBuilder = new StringBuilder();
		try {
			;
			
		    BufferedReader in = new BufferedReader(new FileReader(req.getServletContext().getRealPath("EmailTemplates/ReceivedSupport.html")));
		    String str;
		    while ((str = in.readLine()) != null) {
		    	str = str.replace(":name", support.getName());
		    	
		    	
		        contentBuilder.append(str);
		    }
		    in.close();
		    String content = contentBuilder.toString();
			
			
			SendMailSSL.sendEmail(support.getEmail(),"Received Your Support Request", content);
			
			
			

		} catch (IOException e) {
			
		}
		
		return response;
	}
	

}
