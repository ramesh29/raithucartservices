package com.qb.webservices;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;

import com.qb.dao.ABaseDAO;
import com.qb.dao.DataAccessFactory;
import com.qb.dao.IBookingDAO;
import com.qb.dao.ICourseDAO;
import com.qb.dao.ILoginDAO;
import com.qb.dto.AtomPaymentDTO;
import com.qb.dto.BookingDTO;
import com.qb.dto.CourseBookingDTO;
import com.qb.dto.DirectPaymentDTO;
import com.qb.dto.OrgBookingDTO;
import com.qb.dto.PaymentTransactionDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ScheduleDTO;
import com.qb.dto.SearchParametersDTO;
import com.qb.dto.ShopUserDTO;
import com.qb.dto.TransactionDTO;
import com.qb.dto.UserDTO;
import com.qb.util.AtomPostingThread;
import com.qb.util.GenerateXML;
import com.qb.util.IPaymentGatewayURLS;
import com.qb.util.PaymentGatewayIntegration;
import com.qb.util.QuickBookBase64;
import com.qb.util.QuickBookUtil;
import com.qb.util.SignatureGenerate;

@Path("/bookingService")
public class BookingService  extends ValidateUserRequests{
	
	IBookingDAO bookingDAO = DataAccessFactory.instance().getBookingDAO();
	ICourseDAO courseDAO = DataAccessFactory.instance().getCourseDAO();
	ILoginDAO loginDAO = DataAccessFactory.instance().getLoginDAO();
	private CourseBookingDTO credentials;
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/payNow")
	public ResponseDTO<PaymentTransactionDTO> payNow( String json,
														@Context HttpServletRequest req,@Context HttpServletResponse resp,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		

		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

	
		ResponseDTO<PaymentTransactionDTO> response = new ResponseDTO<PaymentTransactionDTO>();
		List<PaymentTransactionDTO> ptos =new ArrayList<>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		
		ResponseDTO<CourseBookingDTO> response1 = new ResponseDTO<CourseBookingDTO>();
		
		String redirectURL;
		
		ResponseDTO<CourseBookingDTO> courses = courseDAO.getCourseSchdules(search, search.getCategoryId(), search.getSubCategoryId(), search.getCourseId(), search.getInstituteId(), search.getCities()
				,search.getSchdules(), search.getStartDate(), search.getEndDate(),"Available",search.getCourseCode(), response1);
		
		UserDTO users = loginDAO.getUser(search.getUserId(), null);
		
		List<CourseBookingDTO>  courseBookingDTOs = courses.getData();
		List<Integer> scheduleIds = new ArrayList<>();
		String atomUserId = null;
		String atomPassowrd = null;
		String requestHashKey = null;
		if(courseBookingDTOs.size() > 0)
		{
			
			List<BookingDTO> bookings = new ArrayList<>();
			Double totalPrice = 0.0;
			DecimalFormat df = new DecimalFormat("#.##");     
			for(CourseBookingDTO course : courseBookingDTOs)
			{
				BookingDTO bookingDTO = new BookingDTO();
				bookingDTO.setOrgId(course.getOrgId());
				bookingDTO.setAtomProdId(course.getAtomProdId());
				bookingDTO.setAtomProdName(course.getAtomProdName());
				bookingDTO.setScheduleId(course.getScheduleId());
				bookingDTO.setUserId(search.getUserId());
				bookingDTO.setOrgPrice(course.getPrice());
				bookingDTO.setGatewayTransactionStatus("INPROGRESS");
				bookingDTO.setBookingSource("ONLINE");
				bookingDTO.setBookingNumber(QuickBookUtil.getBookingNumber());
				totalPrice = totalPrice+ Double.parseDouble(course.getPrice());
				scheduleIds.add(course.getScheduleId());
				bookingDTO.setApiBatchId(course.getApiBatchId());
				bookingDTO.setApiCourseId(course.getApiCourseId());
				bookingDTO.setPaymentAgreementType(course.getPaymentAgreementType());
				bookingDTO.setPaymentAgreementValue(course.getPaymentAgreementValue());
				bookings.add(bookingDTO);
				
				atomUserId = course.getAtomUsername();
				atomPassowrd = course.getAtomPassword();
				requestHashKey = course.getRequestHashKey();
			}
			
			
			
			
			
			Double serviceTax = (IPaymentGatewayURLS.serviceTax/100)*totalPrice;
			serviceTax = Double.valueOf(df.format(serviceTax));
			Double convienceCharge = (IPaymentGatewayURLS.convienceCost/100)*totalPrice;
			convienceCharge = Double.valueOf(df.format(convienceCharge));
			/*Double paymentGateway = (IConstants.paymentGateway/100)*totalPrice;
			paymentGateway = Double.valueOf(df.format(paymentGateway));
			totalPrice = totalPrice+serviceTax+convienceCharge+paymentGateway;*/
//			Double tractilePrice = convienceCharge;
//			tractilePrice = Double.valueOf(df.format(tractilePrice));
//			
			
			
			totalPrice = totalPrice+convienceCharge;
			totalPrice = Double.valueOf(df.format(totalPrice));
			
			TransactionDTO transactionDTO =  new TransactionDTO();
			transactionDTO.setUserId(search.getUserId());
			transactionDTO.setTotalPrice(totalPrice);
			transactionDTO.setServiceTax(serviceTax);
			transactionDTO.setInternetHandlingFees(convienceCharge);
			//transactionDTO.setPaymentGateway(paymentGateway);
			transactionDTO.setTransactionStatus("INPROGRESS");
			transactionDTO.setTransactionSource("ONLINE");
			boolean success = false;
			
			int reserveSeats = bookingDAO.reserveQuantity(search.getSchdules());
			
			if(reserveSeats > 0)
			{
				success = true;
				int transactionId = bookingDAO.createTransaction(transactionDTO);
				
				if(transactionId > 0)
				{
					int count = bookingDAO.createBooking(transactionId, bookings);
					
					if(count > 0)
					{
						
						/*BookingDTO bookingDTO = new BookingDTO();
						bookingDTO.setAtomProdId("1");
						bookingDTO.setAtomProdName("Tractile");
						bookingDTO.setOrgPrice(tractilePrice+"");
						bookings.add(bookingDTO);*/
						
						success = true;
						
						// Redirect to payment gateway site 
						
						
						String commaSeperatedIds = "";
						for(Integer scheduleId : scheduleIds)
						{
							if(commaSeperatedIds.length()>0)
							{
								commaSeperatedIds=commaSeperatedIds+","+scheduleId;
							}else
							{
								commaSeperatedIds=scheduleId+"";
							}
							
						}
						//commaSeperatedIds = commaSeperatedIds.substring(0, commaSeperatedIds.length()-1);
						
						
						AtomPaymentDTO atom = new AtomPaymentDTO();
						atom.setLogin(Integer.parseInt(atomUserId));
						atom.setPassword(atomPassowrd);
						atom.setTtype("NBFundTransfer");
						//atom.setProdId("NSE");
						// Enable multi for Split Pay
						atom.setProdId("Multi"); 
						atom.setAmount(totalPrice+"");
						atom.setCurrency("INR");
						atom.setTransactionServiceCharge("0");
						atom.setClientCode(QuickBookBase64.encode((""+transactionDTO.getUserId()).getBytes()));
						atom.setTransactionId(transactionId);
						atom.setDate(ABaseDAO.getDateAsString(new Date(),"dd/MM/YYYY HH:mm:ss"));
						atom.setTrackingDate(ABaseDAO.getDateAsString(new Date(),"YYYY-MM-dd"));
						atom.setCustAcc(users.getUserId());
						atom.setReturnUrl(IPaymentGatewayURLS.returnURL);
						atom.setUdf1(users.getUserName());
						atom.setUdf2(search.getUserId()+"");
						atom.setUdf3(commaSeperatedIds);
						atom.setUdf4(users.getUserName());
						atom.setContentXML(GenerateXML.generateXML(bookings,convienceCharge,transactionId));
						//atom.setContentXML("<products><product><id>1</id><name>ONE</name><amount>2000.00</amount></product><product><id>2</id><name>TWO</name><amount>1000.00</amount></product></products>");
						atom.setRequestHashKey(requestHashKey);
						atom.setTransactionStatus("SUCCESS");
						//MDD in request parameters will enable the specific payment options in the end user page
						
						PaymentGatewayIntegration pgi = new PaymentGatewayIntegration();
						//atom = pgi.firstPaymentRequest(atom);
						
						if(!"FAILED".equalsIgnoreCase(atom.getTransactionStatus()))
						{
							
							// Insert into db with atom transaction details
							
							int atomTransactionId = bookingDAO.createAtomTransactionEntry(atom);
							
							//Second Call
							if(atomTransactionId > 0)
							{
								
								//Launch Final Transaction Job Thread
								
								Timer timer = new Timer();

								AtomPostingThread thread = new AtomPostingThread(atom);

							    timer.schedule(thread, 900 * 1000);
								
							    
							   String requestParams = "login=" +atom.getLogin()+
										"&pass="+atom.getPassword()+
										"&ttype="+atom.getTtype()+
										"&prodid="+atom.getProdId()+
										"&amt="+atom.getAmount()+
										//"&amt=3000.0"+
										"&txncurr="+atom.getCurrency()+
										"&txnscamt=" +atom.getTransactionServiceCharge()+
										"&clientcode="+atom.getClientCode()+
										"&txnid="+atom.getTransactionId()+
										"&date="+atom.getDate()+
										"&custacc="+atom.getCustAcc()+
										"&udf1="+atom.getUdf1()+
										"&udf2="+atom.getUdf2()+
										"&udf3="+atom.getUdf3()+
										"&udf4="+atom.getUdf4()+
										"&ru="+atom.getReturnUrl()+
										"&mprod="+atom.getContentXML()+
										"&signature="+SignatureGenerate.getEncodedValueWithSha2(atom.getRequestHashKey(), atom.getLogin()+"",atom.getPassword(),atom.getTtype(),atom.getProdId(),atom.getTransactionId()+"",atom.getAmount(),atom.getCurrency());
							    
								//redirectURL = IPaymentGatewayURLS.paymentGatewayURL+"ttype="+atom.getTtype()+"&tempTxnId="+atom.getTempTxnId()+"&token="+atom.getToken()+"&txnStage="+atom.getTxnStage();
							    redirectURL = IPaymentGatewayURLS.paymentGatewayURL+requestParams;
								PaymentTransactionDTO pto = new PaymentTransactionDTO();
								pto.setTransactionId(transactionId);
								pto.setSchedules(scheduleIds);
								pto.setUserId(search.getUserId());
								pto.setRedirectURL(redirectURL);
								ptos.add(pto);	
							}
							
							
							
						}else
						{
							// revert transaction status in Trasacion DTO, Booking DTO and inform user as failed
							success = false;
							
							
						}
						

						
					}else
					{
						success = false;
					}
				}else
				{
					success = false;
				}
			}else
			{
				response.setResponseCode(100);
				response.setSuccess(false);	
				response.setResponseMessage("Courses are not available.Please try again");
				return response;
			}
			
			if(success == true)
			{
				response.setData(ptos);
				response.setResponseCode(400);
				response.setSuccess(true);	
				response.setResponseMessage("Redirect to Payment Gateway");
				return response;
				
			}else
			{
				response.setResponseCode(100);
				response.setSuccess(false);	
				response.setResponseMessage("Courses are not available.Please try again");
				return response;
			}
			
			
		}else{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Courses are not available.Please try again");
			return response;
		}
	
	}
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/updateTransactionStatus")
	public ResponseDTO<PaymentTransactionDTO> updateTransactionStatus( PaymentTransactionDTO pto,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		
		ResponseDTO<PaymentTransactionDTO> response = new ResponseDTO<PaymentTransactionDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		boolean status = false;
		
		if("success".equalsIgnoreCase(pto.getAction()))
		{
			int confirmBooking = bookingDAO.bookConfirm(pto.getSchedules());
			if(confirmBooking > 0)
			{
				TransactionDTO transactionDTO = new TransactionDTO();
				transactionDTO.setUserId(pto.getUserId());
				transactionDTO.setTransactionStatus("SUCCESS");
				transactionDTO.setTransactionId(pto.getTransactionId());
				int updateTransaction = bookingDAO.updateTransaction(transactionDTO);
						
				if(updateTransaction > 0)
				{
					int updateBooking = bookingDAO.updateBooking(transactionDTO);
					if(updateBooking > 0)
					{
						status = true;
					}
				}
			}
		}else{

			
			
			if(bookingDAO.isTransactionUpdated(pto.getTransactionId()))
			{
				int failBooking = bookingDAO.bookingFailed(pto.getSchedules());
				if(failBooking > 0)
				{
					TransactionDTO transactionDTO = new TransactionDTO();
					transactionDTO.setUserId(pto.getUserId());
					transactionDTO.setTransactionStatus("ERROR");
					transactionDTO.setTransactionId(pto.getTransactionId());
					int updateTransaction = bookingDAO.updateTransaction(transactionDTO);
							
					if(updateTransaction > 0)
					{
						int updateBooking = bookingDAO.updateBooking(transactionDTO);
						if(updateBooking > 0)
						{
							status = true;
						}
					}
				}	
			}
			
		
		}
		
		if(status == true)
		{
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage("Transaction status updated successfully");
			return response;
			
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Transaction status update failed");
			return response;
		}
	}
	

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getOrgBookingInfo")
	public ResponseDTO<OrgBookingDTO> getOrgBookingInfo( String json,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<OrgBookingDTO> response = new ResponseDTO<OrgBookingDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		return bookingDAO.getOrgBookingInfo(search, response);
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getSchedulesDropList")
	public ResponseDTO<ScheduleDTO> getSchedulesDropList( String json,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<ScheduleDTO> response = new ResponseDTO<ScheduleDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		return bookingDAO.getScheduleDropList(search, response);
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/bookByOrg")
	public ResponseDTO<DirectPaymentDTO> bookByOrg( DirectPaymentDTO pto,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ResponseDTO<DirectPaymentDTO> response = new ResponseDTO<DirectPaymentDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		boolean success = false;
		
		int transactionId = bookingDAO.createTransaction(pto.getTransactionDTO());
		
		if(transactionId > 0)
		{
			List<BookingDTO> bookings = new ArrayList<>();
			bookings.add(pto.getBookingDTO());
			int count = bookingDAO.createBooking(transactionId, bookings);
			if(count > 0)
			{
				success = true;
				
			}else
			{
				success = false;
			}
		}else
		{
			success = false;
		}
	
		if(success == true)
		{
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage("Transaction updated successfully");
			return response;
			
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Transaction status update failed");
			return response;
		}
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getUserBookingInfo")
	public ResponseDTO<OrgBookingDTO> getUserBookingInfo( String json,
														@Context HttpServletRequest req,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

		ResponseDTO<OrgBookingDTO> response = new ResponseDTO<OrgBookingDTO>();
		if(!validateAppKey(req,httpHeaders))
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Session Invalid.Please Relaunch Application");
			return response;
		}
		return bookingDAO.getUserBookingInfo(search, response);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/payShopNow")
	public ResponseDTO<PaymentTransactionDTO> payShopNow(String json, @Context HttpServletRequest req,
			@Context HttpServletResponse resp, @Context HttpHeaders httpHeaders) throws Exception {

		ObjectMapper objectMapper = new ObjectMapper();
		credentials = null;
		
		/*
		 * SearchParametersDTO search = objectMapper.readValue(json,
		 * SearchParametersDTO.class);
		 */
		
		ShopUserDTO  planDetails = objectMapper.readValue(json, ShopUserDTO.class);
		ResponseDTO<PaymentTransactionDTO> response = new ResponseDTO<PaymentTransactionDTO>();
		List<PaymentTransactionDTO> ptos = new ArrayList<>();
	

		ResponseDTO<CourseBookingDTO> response1 = new ResponseDTO<CourseBookingDTO>();
		
		
		/* List<BookingDTO> bookings = null; */
		
		String atomUserId = credentials.getAtomUsername();
		String atomPassowrd = credentials.getAtomPassword();
		String requestHashKey = credentials.getRequestHashKey();
		/*String atomUserId = "7";
		String atomPassowrd = "Test@123";
		String requestHashKey = "KEY123657234";*/
		String redirectURL;
		Double totalPrice = planDetails.getPlanPrice();
		Double serviceTax = 0.0;
		Double convienceCharge = 0.0;
		TransactionDTO transactionDTO =  new TransactionDTO();
		/* transactionDTO.setUserId(search.getUserId()); */
		transactionDTO.setMobileNumber(planDetails.getMobileNumber());
		transactionDTO.setTotalPrice(totalPrice);
		transactionDTO.setServiceTax(serviceTax);
		transactionDTO.setInternetHandlingFees(convienceCharge);
		//transactionDTO.setPaymentGateway(paymentGateway);
		transactionDTO.setTransactionStatus("INPROGRESS");
		transactionDTO.setTransactionSource("ONLINE");
		boolean success = false;
		
	
		int transactionId = bookingDAO.createShopPaymentTransaction(transactionDTO);
		 
		/* int transactionId = 2; */
		
		if(transactionId > 0) {
		AtomPaymentDTO atom = new AtomPaymentDTO();
		atom.setLogin(Integer.parseInt(atomUserId));
		atom.setPassword(atomPassowrd);
		atom.setTtype("NBFundTransfer");
		//atom.setProdId("NSE");
		// Enable multi for Split Pay
		atom.setProdId("TRACTILE"); 
		atom.setAmount(totalPrice+"");
		atom.setCurrency("INR");
		atom.setTransactionServiceCharge("0");
			/*
			 * atom.setClientCode(QuickBookBase64.encode((""+transactionDTO.getUserId()).
			 * getBytes()));
			 */
		atom.setClientCode(QuickBookBase64.encode((""+transactionDTO.getMobileNumber()).getBytes()));
		atom.setTransactionId(transactionId);
		atom.setDate(ABaseDAO.getDateAsString(new Date(),"dd/MM/YYYY HH:mm:ss"));
		atom.setTrackingDate(ABaseDAO.getDateAsString(new Date(),"YYYY-MM-dd"));
		/* atom.setCustAcc(users.getUserId()); */
		atom.setReturnUrl(IPaymentGatewayURLS.returnURL);
		/*
		 * atom.setUdf1(users.getUserName()); atom.setUdf2(search.getUserId()+"");
		 * atom.setUdf3(commaSeperatedIds); atom.setUdf4(users.getUserName());
		 */
			/*
			 * atom.setContentXML(GenerateXML.generateXML(bookings,convienceCharge,
			 * transactionId));
			 */
		//atom.setContentXML("<products><product><id>1</id><name>ONE</name><amount>2000.00</amount></product><product><id>2</id><name>TWO</name><amount>1000.00</amount></product></products>");
		atom.setRequestHashKey(requestHashKey);
		atom.setTransactionStatus("SUCCESS");
		//MDD in request parameters will enable the specific payment options in the end user page
		
		PaymentGatewayIntegration pgi = new PaymentGatewayIntegration();
		//atom = pgi.firstPaymentRequest(atom);
		
		if(!"FAILED".equalsIgnoreCase(atom.getTransactionStatus()))
		{
			
			// Insert into db with atom transaction details
			
			int atomTransactionId = bookingDAO.createAtomTransactionEntry(atom);
			
			//Second Call
			if(atomTransactionId > 0)
			{	success = true;
				
				//Launch Final Transaction Job Thread
				
				Timer timer = new Timer();

				AtomPostingThread thread = new AtomPostingThread(atom);

			    timer.schedule(thread, 900 * 1000);
				
			    
			   String requestParams = "login=" +atom.getLogin()+
						"&pass="+atom.getPassword()+
						"&ttype="+atom.getTtype()+
						"&prodid="+atom.getProdId()+
						"&amt="+atom.getAmount()+
						//"&amt=3000.0"+
						"&txncurr="+atom.getCurrency()+
						"&txnscamt=" +atom.getTransactionServiceCharge()+
						"&clientcode="+atom.getClientCode()+
						"&txnid="+atom.getTransactionId()+
						"&date="+atom.getDate()+
						"&custacc="+atom.getCustAcc()+
						"&udf1="+atom.getUdf1()+
						"&udf2="+atom.getUdf2()+
						"&udf3="+atom.getUdf3()+
						"&udf4="+atom.getUdf4()+
						"&ru="+atom.getReturnUrl()+
						"&mprod="+atom.getContentXML()+
						"&signature="+SignatureGenerate.getEncodedValueWithSha2(atom.getRequestHashKey(), atom.getLogin()+"",atom.getPassword(),atom.getTtype(),atom.getProdId(),atom.getTransactionId()+"",atom.getAmount(),atom.getCurrency());
			    
				//redirectURL = IPaymentGatewayURLS.paymentGatewayURL+"ttype="+atom.getTtype()+"&tempTxnId="+atom.getTempTxnId()+"&token="+atom.getToken()+"&txnStage="+atom.getTxnStage();
			    redirectURL = IPaymentGatewayURLS.paymentGatewayURL+requestParams;
				PaymentTransactionDTO pto = new PaymentTransactionDTO();
				pto.setTransactionId(transactionId);
				/*
				 * pto.setSchedules(scheduleIds); pto.setUserId(search.getUserId());
				 */
				pto.setRedirectURL(redirectURL);
				ptos.add(pto);	
			}
			
			
			
		}else
		{
			// revert transaction status in Trasacion DTO, Booking DTO and inform user as failed
			success = false;
			
			
		}
		
		
		
	}
		if(success == true)
		{
			response.setData(ptos);
			response.setResponseCode(400);
			response.setSuccess(true);	
			response.setResponseMessage("Redirect to Payment Gateway");
			return response;
			
		}else
		{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Shop registration payment failed");
			return response;
		}
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/payNowForSubscription")
	public ResponseDTO<PaymentTransactionDTO> payNowForSubscription( String json,
														@Context HttpServletRequest req,@Context HttpServletResponse resp,
														@Context HttpHeaders httpHeaders) throws Exception{
		
		

		ObjectMapper objectMapper = new ObjectMapper();
		
		SearchParametersDTO search = objectMapper.readValue(json, SearchParametersDTO.class);

	
		ResponseDTO<PaymentTransactionDTO> response = new ResponseDTO<PaymentTransactionDTO>();
		List<PaymentTransactionDTO> ptos =new ArrayList<>();
		
		ResponseDTO<CourseBookingDTO> response1 = new ResponseDTO<CourseBookingDTO>();
		
		String redirectURL;
		
		ResponseDTO<CourseBookingDTO> courses = bookingDAO.getAtomDetails(search, response1);
		
		List<CourseBookingDTO>  courseBookingDTOs = courses.getData();
		List<Integer> scheduleIds = new ArrayList<>();
		String atomUserId = null;
		String atomPassowrd = null;
		String requestHashKey = null;
		if(courseBookingDTOs.size() > 0)
		{
			Double totalPrice = 0.0;
			DecimalFormat df = new DecimalFormat("#.##");     
			for(CourseBookingDTO course : courseBookingDTOs)
			{
				atomUserId = course.getAtomUsername();
				requestHashKey = course.getRequestHashKey();
				atomPassowrd = course.getAtomPassword();
			}
			
			totalPrice = Double.valueOf(df.format(search.getAmount()));
			TransactionDTO transactionDTO =  new TransactionDTO();
			transactionDTO.setUserId(1);
			transactionDTO.setTotalPrice(totalPrice);
			transactionDTO.setServiceTax(0.0);
			transactionDTO.setInternetHandlingFees(0.0);
			transactionDTO.setMobileNumber(search.getMobile());
			transactionDTO.setPlan(search.getPlan());
			transactionDTO.setTransactionStatus("INPROGRESS");
			transactionDTO.setTransactionSource("ONLINE");
			boolean success = false;
			
			int reserveSeats = 1;
			
			if(reserveSeats > 0)
			{
				success = true;
				int transactionId = bookingDAO.createTransaction(transactionDTO);
				
				if(transactionId > 0)
				{
					int count = 1;
					
					if(count > 0)
					{
						
						success = true;
						
						AtomPaymentDTO atom = new AtomPaymentDTO();
						atom.setLogin(Integer.parseInt(atomUserId));
						atom.setPassword(atomPassowrd);
						atom.setTtype("NBFundTransfer");
						atom.setProdId("TRACTILE");
						// Enable multi for Split Pay
						//atom.setProdId("Multi"); 
						atom.setAmount(totalPrice+"");
						atom.setCurrency("INR");
						atom.setTransactionServiceCharge("0");
						atom.setClientCode(QuickBookBase64.encode((""+search.getMobile()).getBytes()));
						atom.setTransactionId(transactionId);
						atom.setDate(ABaseDAO.getDateAsString(new Date(),"dd/MM/YYYY HH:mm:ss"));
						atom.setTrackingDate(ABaseDAO.getDateAsString(new Date(),"YYYY-MM-dd"));
						atom.setCustAcc(Integer.parseInt(search.getMobile().substring(0, 8)));
						atom.setReturnUrl(IPaymentGatewayURLS.returnURLMerchant);
						atom.setUdf1(search.getMobile()+"");
						atom.setUdf2(search.getAmount()+"");
						atom.setUdf3(search.getPlan());
						atom.setUdf4(search.getPaymentFor());
						//atom.setContentXML(GenerateXML.generateXML(bookings,0.0,transactionId));
						//atom.setContentXML("<products><product><id>1</id><name>ONE</name><amount>2000.00</amount></product><product><id>2</id><name>TWO</name><amount>1000.00</amount></product></products>");
						atom.setRequestHashKey(requestHashKey);
						atom.setTransactionStatus("SUCCESS");
						//MDD in request parameters will enable the specific payment options in the end user page
						
						//PaymentGatewayIntegration pgi = new PaymentGatewayIntegration();
						//atom = pgi.firstPaymentRequest(atom);
						
						if(!"FAILED".equalsIgnoreCase(atom.getTransactionStatus()))
						{
							
							// Insert into db with atom transaction details
							
							int atomTransactionId = bookingDAO.createAtomTransactionEntry(atom);
							
							//Second Call
							if(atomTransactionId > 0)
							{
								
								//Launch Final Transaction Job Thread
								
								Timer timer = new Timer();

								AtomPostingThread thread = new AtomPostingThread(atom);

							    timer.schedule(thread, 900 * 1000);
								
							    
							   String requestParams = "login=" +atom.getLogin()+
										"&pass="+atom.getPassword()+
										"&ttype="+atom.getTtype()+
										"&prodid="+atom.getProdId()+
										"&amt="+atom.getAmount()+
										//"&amt=3000.0"+
										"&txncurr="+atom.getCurrency()+
										"&txnscamt=" +atom.getTransactionServiceCharge()+
										"&clientcode="+atom.getClientCode()+
										"&txnid="+atom.getTransactionId()+
										"&date="+atom.getDate()+
										"&custacc="+atom.getCustAcc()+
										"&udf1="+atom.getUdf1()+
										"&udf2="+atom.getUdf2()+
										"&udf3="+atom.getUdf3()+
										"&udf4="+atom.getUdf4()+
										"&ru="+atom.getReturnUrl()+
									//	"&mprod="+atom.getContentXML()+
										"&signature="+SignatureGenerate.getEncodedValueWithSha2(atom.getRequestHashKey(), atom.getLogin()+"",atom.getPassword(),atom.getTtype(),atom.getProdId(),atom.getTransactionId()+"",atom.getAmount(),atom.getCurrency());
							    
								//redirectURL = IPaymentGatewayURLS.paymentGatewayURL+"ttype="+atom.getTtype()+"&tempTxnId="+atom.getTempTxnId()+"&token="+atom.getToken()+"&txnStage="+atom.getTxnStage();
							    redirectURL = IPaymentGatewayURLS.paymentGatewayURL+requestParams;
								PaymentTransactionDTO pto = new PaymentTransactionDTO();
								pto.setTransactionId(transactionId);
								pto.setSchedules(scheduleIds);
								pto.setUserId(search.getUserId());
								pto.setRedirectURL(redirectURL);
								ptos.add(pto);	
							}
							
							
							
						}else
						{
							// revert transaction status in Trasacion DTO, Booking DTO and inform user as failed
							success = false;
							
							
						}
						

						
					}else
					{
						success = false;
					}
				}else
				{
					success = false;
				}
			}else
			{
				response.setResponseCode(100);
				response.setSuccess(false);	
				response.setResponseMessage("Courses are not available.Please try again");
				return response;
			}
			
			if(success == true)
			{
				response.setData(ptos);
				response.setResponseCode(400);
				response.setSuccess(true);	
				response.setResponseMessage("Redirect to Payment Gateway");
				return response;
				
			}else
			{
				response.setResponseCode(100);
				response.setSuccess(false);	
				response.setResponseMessage("Courses are not available.Please try again");
				return response;
			}
			
			
		}else{
			response.setResponseCode(100);
			response.setSuccess(false);	
			response.setResponseMessage("Courses are not available.Please try again");
			return response;
		}
	
	}
	
		
}
