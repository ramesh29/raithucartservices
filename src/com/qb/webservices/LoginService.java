package com.qb.webservices;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;

import com.qb.dao.DataAccessFactory;
import com.qb.dao.ILoginDAO;
import com.qb.dto.PasswordDTO;
import com.qb.dto.ResponseDTO;
import com.qb.dto.ShopItemDTO;
import com.qb.dto.ShopUserDTO;
import com.qb.dto.UserDTO;
import com.qb.util.GenerateOTP;
import com.qb.util.IPaymentGatewayURLS;
/*import com.qb.util.SMSGateway;*/
import com.qb.util.SMSService;
import com.qb.util.SendMailSSL;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/loginService")
public class LoginService extends ValidateUserRequests {

	ILoginDAO loginDAO = DataAccessFactory.instance().getLoginDAO();
	public HttpSession httpSession;

	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/apikey")
	public String getAppKey(@Context HttpServletRequest req) throws Exception {
		return getApiKey(req);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/register")
	public UserDTO registerUser(UserDTO user, @Context HttpServletRequest req) throws Exception {

		UserDTO userDTO = loginDAO.checkUserNameExists(user.getUserName(), 0);

		if (userDTO.isSuccess()) {
			userDTO = loginDAO.registerUser(user, user.getUserType());
			if (userDTO.isSuccess()) {
				StringBuilder contentBuilder = new StringBuilder();
				try {
					;

					BufferedReader in = new BufferedReader(
							new FileReader(req.getServletContext().getRealPath("EmailTemplates/NewUser.html")));
					String str;
					while ((str = in.readLine()) != null) {
						str = str.replace(":link", IPaymentGatewayURLS.ServerURL + "pages/changePassword.html?userId="
								+ userDTO.getUserId());
						contentBuilder.append(str);
					}
					in.close();
				} catch (IOException e) {
				}
				String content = contentBuilder.toString();

				SendMailSSL.sendEmail(userDTO.getUserName(), "User registered successfully", content);
			}
		}

		return userDTO;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/createPassword")
	public UserDTO createPassword(PasswordDTO passwordDTO) throws Exception {

		UserDTO userDTO = loginDAO.CreateorUpdatePassword(passwordDTO);
		return userDTO;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/forgotPassword")
	public UserDTO forgotPassword(UserDTO passwordDTO, @Context HttpServletRequest req) throws Exception {

		UserDTO userDTO = loginDAO.checkUserNameExists(passwordDTO.getUserName(), 0);

		if (!userDTO.isSuccess()) {

			StringBuilder contentBuilder = new StringBuilder();
			try {
				;

				BufferedReader in = new BufferedReader(
						new FileReader(req.getServletContext().getRealPath("EmailTemplates/NewUser.html")));
				String str;
				while ((str = in.readLine()) != null) {
					str = str.replace(":link", IPaymentGatewayURLS.ServerURL
							+ "MyMarineCourse/pages/changePassword.html?userId=" + userDTO.getUserId());
					str = str.replace("Your have successfully registered.", "");
					contentBuilder.append(str);
				}
				in.close();
			} catch (IOException e) {
			}
			String content = contentBuilder.toString();

			SendMailSSL.sendEmail(passwordDTO.getUserName(), "Reset Password", content);
			userDTO.setSuccess(true);
		}

		return userDTO;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/authenticate")
	public UserDTO authenticate(UserDTO userDTO, @Context HttpServletRequest req, @Context HttpHeaders httpHeaders)
			throws Exception {

		/*
		 * if(!validateAppKey(req,httpHeaders)) { userDTO.setSuccess(false);
		 * userDTO.setMessege("Session invalid.Please try again"); return userDTO;
		 * 
		 * }else {}
		 */

		return loginDAO.authenticate(userDTO.getUserName(), userDTO.getPassword());

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/updateUser")
	public ResponseDTO<UserDTO> updateUser(UserDTO userDTO, @Context HttpServletRequest req,
			@Context HttpHeaders httpHeaders) throws Exception {

		ResponseDTO<UserDTO> response = new ResponseDTO<UserDTO>();

		if (!validateAppKey(req, httpHeaders)) {
			response.setResponseCode(100);
			response.setSuccess(false);
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}

		userDTO = loginDAO.updateUser(userDTO, userDTO.getUserType());
		if (userDTO.isSuccess()) {
			response.setResponseCode(200);

		} else {
			response.setResponseCode(402);
		}
		response.setSuccess(userDTO.isSuccess());
		response.setResponseMessage(userDTO.getMessege());
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getUserId/{userName}")
	public ResponseDTO<UserDTO> checkUserExists(@PathParam("userName") String userName, @Context HttpServletRequest req,
			@Context HttpHeaders httpHeaders) throws Exception {

		ResponseDTO<UserDTO> response = new ResponseDTO<UserDTO>();
		UserDTO userDTO = loginDAO.checkUserNameExists(userName, 0);
		if (!validateAppKey(req, httpHeaders)) {
			response.setResponseCode(100);
			response.setSuccess(false);
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		List<UserDTO> users = new ArrayList<>();
		if (userDTO.getUserId() > 0) {
			userDTO = loginDAO.getUser(userDTO.getUserId(), null);
			if (userDTO.isSuccess()) {
				response.setResponseCode(200);
				users.add(userDTO);

			} else {
				response.setResponseCode(402);
			}

		} else {
			userDTO.setSuccess(false);
			userDTO.setMessege("User does not exist.Please create new user");
		}

		response.setSuccess(userDTO.isSuccess());
		response.setResponseMessage(userDTO.getMessege());
		response.setData(users);
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getUser/{userId}")
	public ResponseDTO<UserDTO> getUser(@PathParam("userId") int userId, @Context HttpServletRequest req,
			@Context HttpHeaders httpHeaders) throws Exception {

		ResponseDTO<UserDTO> response = new ResponseDTO<UserDTO>();

		if (!validateAppKey(req, httpHeaders)) {
			response.setResponseCode(100);
			response.setSuccess(false);
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		List<UserDTO> users = new ArrayList<>();
		UserDTO userDTO = new UserDTO();
		userDTO = loginDAO.getUser(userId, null);
		if (userDTO.isSuccess()) {
			response.setResponseCode(200);
			users.add(userDTO);

		} else {
			response.setResponseCode(402);
		}
		response.setSuccess(userDTO.isSuccess());
		response.setResponseMessage(userDTO.getMessege());

		response.setData(users);
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getUserWithIndos/{indos}")
	public ResponseDTO<UserDTO> getUserWithIndos(@PathParam("indos") String indos, @Context HttpServletRequest req,
			@Context HttpHeaders httpHeaders) throws Exception {

		ResponseDTO<UserDTO> response = new ResponseDTO<UserDTO>();

		if (!validateAppKey(req, httpHeaders)) {
			response.setResponseCode(100);
			response.setSuccess(false);
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		List<UserDTO> users = new ArrayList<>();
		UserDTO userDTO = new UserDTO();
		userDTO = loginDAO.getUser(0, indos);
		if (userDTO.isSuccess()) {
			response.setResponseCode(200);
			users.add(userDTO);

		} else {
			response.setResponseCode(402);
		}
		response.setSuccess(userDTO.isSuccess());
		response.setResponseMessage(userDTO.getMessege());

		response.setData(users);
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getAdminUser/{orgId}")
	public ResponseDTO<UserDTO> getaAdminUser(@PathParam("orgId") int orgId, @Context HttpServletRequest req,
			@Context HttpHeaders httpHeaders) throws Exception {

		ResponseDTO<UserDTO> response = new ResponseDTO<UserDTO>();

		if (!validateAppKey(req, httpHeaders)) {
			response.setResponseCode(100);
			response.setSuccess(false);
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		List<UserDTO> users = new ArrayList<>();
		UserDTO userDTO = new UserDTO();
		userDTO = loginDAO.getAdminUser(orgId);
		if (userDTO.isSuccess()) {
			response.setResponseCode(200);
			users.add(userDTO);

		} else {
			response.setResponseCode(402);
		}
		response.setSuccess(userDTO.isSuccess());
		response.setResponseMessage(userDTO.getMessege());

		response.setData(users);
		return response;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/updateAdminUser")
	public ResponseDTO<UserDTO> updateAdminUser(UserDTO userDTO, @Context HttpServletRequest req,
			@Context HttpHeaders httpHeaders) throws Exception {

		ResponseDTO<UserDTO> response = new ResponseDTO<UserDTO>();

		if (!validateAppKey(req, httpHeaders)) {
			response.setResponseCode(100);
			response.setSuccess(false);
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}

		userDTO = loginDAO.updateAdmin(userDTO);
		if (userDTO.isSuccess()) {
			response.setResponseCode(200);

		} else {
			response.setResponseCode(402);
		}
		response.setSuccess(userDTO.isSuccess());
		response.setResponseMessage(userDTO.getMessege());
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getUsers")
	public ResponseDTO<UserDTO> getUsers(@Context HttpServletRequest req, @Context HttpHeaders httpHeaders)
			throws Exception {

		ResponseDTO<UserDTO> response = new ResponseDTO<UserDTO>();

		if (!validateAppKey(req, httpHeaders)) {
			response.setResponseCode(100);
			response.setSuccess(false);
			response.setResponseMessage("Session Invalid.Please Relogin");
			return response;
		}
		return loginDAO.getUsers(response);

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/checkUsers")
	public ShopUserDTO checkShopUserExists(ShopUserDTO user, @Context HttpServletRequest req) throws Exception {

		ShopUserDTO userDTO = loginDAO.checkShopUserExists(user.getMobileNumber());

		/*
		 * if(userDTO.isSuccess()) { userDTO = loginDAO.registerShopUser(user);
		 * if(userDTO.isSuccess()) { StringBuilder contentBuilder = new StringBuilder();
		 * try { ;
		 * 
		 * BufferedReader in = new BufferedReader(new
		 * FileReader(req.getServletContext().getRealPath("EmailTemplates/NewUser.html")
		 * )); String str; while ((str = in.readLine()) != null) { str =
		 * str.replace(":link",
		 * IPaymentGatewayURLS.ServerURL+"pages/changePassword.html?userId="+userDTO.
		 * getUserId()); contentBuilder.append(str); } in.close(); } catch (IOException
		 * e) { } String content = contentBuilder.toString();
		 * 
		 * 
		 * SendMailSSL.sendEmail(userDTO.getUserName(),"User registered successfully",
		 * content); } }
		 */

		return userDTO;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/registerShop")
	public ShopUserDTO registerShopUser(ShopUserDTO user, @Context HttpServletRequest req) throws Exception {

		ShopUserDTO userDTO = loginDAO.checkShopUserExists(user.getMobileNumber());
		
		/*if (userDTO.isSuccess()) {*/
			userDTO = loginDAO.registerShopUser(user);
			/*
			 * if(userDTO.isSuccess()) { StringBuilder contentBuilder = new StringBuilder();
			 * try { ;
			 * 
			 * BufferedReader in = new BufferedReader(new
			 * FileReader(req.getServletContext().getRealPath("EmailTemplates/NewUser.html")
			 * )); String str; while ((str = in.readLine()) != null) { str =
			 * str.replace(":link",
			 * IPaymentGatewayURLS.ServerURL+"pages/changePassword.html?userId="+userDTO.
			 * getUserId()); contentBuilder.append(str); } in.close(); } catch (IOException
			 * e) { } String content = contentBuilder.toString();
			 * 
			 * 
			 * SendMailSSL.sendEmail(userDTO.getUserName(),"User registered successfully",
			 * content); }
			 */
		/* } */

		return userDTO;
	}
	
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/renewShop")
	public ShopUserDTO renewShop(ShopUserDTO user, @Context HttpServletRequest req) throws Exception {

		ShopUserDTO userDTO = loginDAO.checkShopUserExists(user.getMobileNumber());
		
		/*if (userDTO.isSuccess()) {*/
				/*userDTO = loginDAO.registerShopUser(user);*/
					userDTO = loginDAO.renewShopUser(user);
			 /* userDTO = loginDAO.renewShopUser(user);*/
			/*
			 * if(userDTO.isSuccess()) { StringBuilder contentBuilder = new StringBuilder();
			 * try { ;
			 * 
			 * BufferedReader in = new BufferedReader(new
			 * FileReader(req.getServletContext().getRealPath("EmailTemplates/NewUser.html")
			 * )); String str; while ((str = in.readLine()) != null) { str =
			 * str.replace(":link",
			 * IPaymentGatewayURLS.ServerURL+"pages/changePassword.html?userId="+userDTO.
			 * getUserId()); contentBuilder.append(str); } in.close(); } catch (IOException
			 * e) { } String content = contentBuilder.toString();
			 * 
			 * 
			 * SendMailSSL.sendEmail(userDTO.getUserName(),"User registered successfully",
			 * content); }
			 */
		/* } */

		return userDTO;
	}
	
	
	/*
	 * @POST
	 * 
	 * @Consumes({ MediaType.APPLICATION_JSON })
	 * 
	 * @Produces({ MediaType.APPLICATION_JSON })
	 * 
	 * @Path("/registerShop") public ShopUserDTO registerShopUser(ShopUserDTO
	 * user, @Context HttpServletRequest req) throws Exception {
	 * 
	 * ShopUserDTO userDTO = loginDAO.checkShopUserExists(user.getMobileNumber());
	 * 
	 * if (userDTO.isSuccess()) { userDTO = loginDAO.registerShopUser(user);
	 * 
	 * if(userDTO.isSuccess()) { StringBuilder contentBuilder = new StringBuilder();
	 * try { ;
	 * 
	 * BufferedReader in = new BufferedReader(new
	 * FileReader(req.getServletContext().getRealPath("EmailTemplates/NewUser.html")
	 * )); String str; while ((str = in.readLine()) != null) { str =
	 * str.replace(":link",
	 * IPaymentGatewayURLS.ServerURL+"pages/changePassword.html?userId="+userDTO.
	 * getUserId()); contentBuilder.append(str); } in.close(); } catch (IOException
	 * e) { } String content = contentBuilder.toString();
	 * 
	 * 
	 * SendMailSSL.sendEmail(userDTO.getUserName(),"User registered successfully",
	 * content); }
	 * 
	 * }
	 * 
	 * return userDTO; }
	 */

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/updateShopInvoice")
	public ShopUserDTO updateShopUser(ShopUserDTO user, @Context HttpServletRequest req) throws Exception {

		return loginDAO.updateShop(user);
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/updateUUID")
	public ShopUserDTO updateShopUUID(ShopUserDTO user, @Context HttpServletRequest req) throws Exception {

		return loginDAO.updateShopDeviceUUID(user);
	}
	
	
	@POST
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	@Produces({MediaType.APPLICATION_JSON })
	@Path("/updateShop")
	public ShopUserDTO updateShopUsers(@FormDataParam("file") InputStream uploadedInputStream, @FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormDataParam("shopItemDTO") String shopDetails,@FormDataParam("userId") String userId, @Context HttpServletRequest req) throws Exception {

		ShopUserDTO shopUserDTO = new ObjectMapper().readValue(shopDetails, ShopUserDTO.class);
		String fileLocation = IPaymentGatewayURLS.extDir +shopUserDTO.getUserId();
		
		

		File directory = new File(fileLocation);
		if (! directory.exists()){
			directory.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
		}

		String fileName = shopUserDTO.getShopName().replaceAll(" ", "_")+"_"+fileDetail.getFileName().replaceAll(" ", "_");
		shopUserDTO.setImageName(fileName);
		//fileLocation = req.getServletContext().getRealPath(fileLocation)+"\\"+fileName;
		 
		 try {  
             FileOutputStream out = new FileOutputStream(new File(fileLocation+"/"+fileName));  
             int read = 0;  
             byte[] bytes = new byte[1024];  
             out = new FileOutputStream(new File(fileLocation+"/"+fileName));  
             while ((read = uploadedInputStream.read(bytes)) != -1) {  
                 out.write(bytes, 0, read);  
             }  
             out.flush();  
             out.close();  
         } catch (IOException e) {e.printStackTrace();}  
        
		
		
		
		
		
		
		return loginDAO.updateShop(shopUserDTO);
	}
	
	
	
	
	
	
	
	
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/shopLogin")
	public ShopUserDTO shopLogin(ShopUserDTO userDTO, @Context HttpServletRequest req, @Context HttpHeaders httpHeaders)
			throws Exception {

		/*
		 * if(!validateAppKey(req,httpHeaders)) { userDTO.setSuccess(false);
		 * userDTO.setMessege("Session invalid.Please try again"); return userDTO;
		 * 
		 * }else {}
		 */

		userDTO = loginDAO.shopLogin(userDTO.getMobileNumber(), userDTO.getPassword());

		if (userDTO.isSuccess()) {
			httpSession = req.getSession(true);
			httpSession.setAttribute("userId", "" + userDTO.getUserId());
			userDTO.setApiKey(httpSession.getId());
		}

		return userDTO;

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/generateOTP")
	public ShopUserDTO generateOTP(ShopUserDTO userDTO, @Context HttpServletRequest req,
			@Context HttpHeaders httpHeaders) throws Exception {

		String otp = GenerateOTP.generatorOTP(4);
		userDTO.setOtp(otp);
		System.out.println("final otp:" + otp);
		SMSService sms = new SMSService();
	    sms.sendSms(IPaymentGatewayURLS.smsUserName,IPaymentGatewayURLS.smsPassword, IPaymentGatewayURLS.smsSender, userDTO.getMobileNumber(),"Your verification code " +otp);
		return userDTO;

	}
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/updateShopPassword")
	public ShopUserDTO forgetShopPassword(PasswordDTO passwordDTO) throws Exception {

		
		ShopUserDTO userDTO = loginDAO.confirmPassword(passwordDTO);
		if(userDTO.isSuccess()) {
			userDTO = loginDAO.updatePassword(passwordDTO);
		}
		return userDTO;
	}
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/forgetShopPassword")
	public ShopUserDTO upadateShopPassword(PasswordDTO passwordDTO) throws Exception {

		ShopUserDTO userDTO = loginDAO.updatePassword(passwordDTO);
		return userDTO;
	}
}
