package com.qb.dto;

import java.io.Serializable;

public class BookingDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int transactionId = 0;
	
	private int bookingId = 0;
	
	private int orgId = 0;
	
	private int scheduleId = 0;
	
	private int userId = 0;
	
	private String orgPrice = null;
	
	private String orgPaymentStatus = null;
	
	private String orgPaymentDate = null;
	
	private String gatewayTransactionStatus = null;
	
	private String gatewayPaymentStatus = null;
	
	private String gatewayPaymentDate = null;
	
	private String bookingSource = null;
	
	private String bookingStatus  = null;
	
	private String bookingNumber = null;
	
	private String atomProdId = null;
	
	private String atomProdName = null;
	
	private int apiCourseId = 0;
	
	private int apiBatchId = 0;
	
	private String paymentAgreementType = null;
	
	private String paymentAgreementValue = null;
	
	public String getPaymentAgreementType() {
		return paymentAgreementType;
	}

	public void setPaymentAgreementType(String paymentAgreementType) {
		this.paymentAgreementType = paymentAgreementType;
	}

	public String getPaymentAgreementValue() {
		return paymentAgreementValue;
	}

	public void setPaymentAgreementValue(String paymentAgreementValue) {
		this.paymentAgreementValue = paymentAgreementValue;
	}
	
	
	public String getAtomProdId() {
		return atomProdId;
	}

	public void setAtomProdId(String atomProdId) {
		this.atomProdId = atomProdId;
	}

	public String getAtomProdName() {
		return atomProdName;
	}

	public void setAtomProdName(String atomProdName) {
		this.atomProdName = atomProdName;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getOrgPrice() {
		return orgPrice;
	}

	public void setOrgPrice(String orgPrice) {
		this.orgPrice = orgPrice;
	}

	public String getOrgPaymentStatus() {
		return orgPaymentStatus;
	}

	public void setOrgPaymentStatus(String orgPaymentStatus) {
		this.orgPaymentStatus = orgPaymentStatus;
	}

	public String getOrgPaymentDate() {
		return orgPaymentDate;
	}

	public void setOrgPaymentDate(String orgPaymentDate) {
		this.orgPaymentDate = orgPaymentDate;
	}

	public String getGatewayTransactionStatus() {
		return gatewayTransactionStatus;
	}

	public void setGatewayTransactionStatus(String gatewayTransactionStatus) {
		this.gatewayTransactionStatus = gatewayTransactionStatus;
	}

	public String getGatewayPaymentStatus() {
		return gatewayPaymentStatus;
	}

	public void setGatewayPaymentStatus(String gatewayPaymentStatus) {
		this.gatewayPaymentStatus = gatewayPaymentStatus;
	}

	public String getGatewayPaymentDate() {
		return gatewayPaymentDate;
	}

	public void setGatewayPaymentDate(String gatewayPaymentDate) {
		this.gatewayPaymentDate = gatewayPaymentDate;
	}

	public String getBookingSource() {
		return bookingSource;
	}

	public void setBookingSource(String bookingSource) {
		this.bookingSource = bookingSource;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getBookingNumber() {
		return bookingNumber;
	}

	public void setBookingNumber(String bookingNumber) {
		this.bookingNumber = bookingNumber;
	}

	public int getApiCourseId() {
		return apiCourseId;
	}

	public void setApiCourseId(int apiCourseId) {
		this.apiCourseId = apiCourseId;
	}

	public int getApiBatchId() {
		return apiBatchId;
	}

	public void setApiBatchId(int apiBatchId) {
		this.apiBatchId = apiBatchId;
	}
	
	
	
}
