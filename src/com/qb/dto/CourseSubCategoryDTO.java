package com.qb.dto;

import java.io.Serializable;
import java.util.List;

public class CourseSubCategoryDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int couseCategoryId = 0;
	
	private int couseSubCategoryId = 0;
	
	private String courseSubCategoryName = null;
	
	private List<CourseDTO> courses = null;

	public int getCouseCategoryId() {
		return couseCategoryId;
	}

	public void setCouseCategoryId(int couseCategoryId) {
		this.couseCategoryId = couseCategoryId;
	}

	public int getCouseSubCategoryId() {
		return couseSubCategoryId;
	}

	public void setCouseSubCategoryId(int couseSubCategoryId) {
		this.couseSubCategoryId = couseSubCategoryId;
	}

	public String getCourseSubCategoryName() {
		return courseSubCategoryName;
	}

	public void setCourseSubCategoryName(String courseSubCategoryName) {
		this.courseSubCategoryName = courseSubCategoryName;
	}

	public List<CourseDTO> getCourses() {
		return courses;
	}

	public void setCourses(List<CourseDTO> courses) {
		this.courses = courses;
	}

}
