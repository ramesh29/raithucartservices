package com.qb.dto;

import java.io.Serializable;
import java.util.List;

public class CCTransactionDTO extends CreditCustomerDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8328712368716716104L;

	private int id;
	
	private int cc_id;

	private int customer_id;
	
	private String transactionDate;
	
	private Double amount;
	
	private String pdf;
	
	private String invoiceNumber;
	
	private List<ShopTransactionDTO> transactions;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCc_id() {
		return cc_id;
	}

	public void setCc_id(int cc_id) {
		this.cc_id = cc_id;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public List<ShopTransactionDTO> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<ShopTransactionDTO> transactions) {
		this.transactions = transactions;
	}

	public String getPdf() {
		return pdf;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	
	
}
