package com.qb.dto;

import java.io.Serializable;

public class OrgDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int orgId = 0;
	
	private String orgName = null;
	
	private String phone= null;
	
	private String email = null;
	
	private String address1 = null;
	
	private String address2 = null;
	
	private String city = null;
	
	private String state = null;
	
	private String zip = null;
	
	private String country = null;
	
	private String active = "N";
	
	private String creationDate = null;
	
	private OrgAccountDTO orgAccountDetails = null;

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public OrgAccountDTO getOrgAccountDetails() {
		return orgAccountDetails;
	}

	public void setOrgAccountDetails(OrgAccountDTO orgAccountDetails) {
		this.orgAccountDetails = orgAccountDetails;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	

}
