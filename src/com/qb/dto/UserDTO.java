package com.qb.dto;

import java.io.Serializable;

public class UserDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int userId = 0;
	
	private String userName = null;
	
	private String firstName = null;
	
	private String lastName = null;
	
	private String password = null;
	
	private int roleId = 0;
	
	private String roleName = null;
	
	private String userType = null;
	
	private int orgId = 0;
	
	private String dateOfBirth = null;
	
	private String rank = null;
	
	private String indosNo = null;
	
	private String cdcNo = null;
	
	private String cdcIssueDate = null;
	
	private String cocNo = null;
	
	private String grade = null;
	
	private String cocIssuingAuthority = null;
	
	private String cocIssuingDate = null;
	
	private String passportNo = null;
	
	private String passportPlaceOfIssue = null;
	
	private String passportIssueDate = null;
	
	private String address = null;
	
	private String city = null;
	
	private String state = null;
	
	private String zip = null;
	
	private String country = null;
	
	private String nationality = null;
	
	private String email = null;
	
	private String phone = null;
	
	private String active = null;
	
	private int createdBy = 0;
	
	private int updatedBy = 0;
	
	private boolean success = false;
	
	private String messege = null;
	
	private String apiKey = null;
	
	private int bookings = 0;
	
	private String lastBookingDate = null;

	public int getBookings() {
		return bookings;
	}

	public void setBookings(int bookings) {
		this.bookings = bookings;
	}

	public String getLastBookingDate() {
		return lastBookingDate;
	}

	public void setLastBookingDate(String lastBookingDate) {
		this.lastBookingDate = lastBookingDate;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessege() {
		return messege;
	}

	public void setMessege(String messege) {
		this.messege = messege;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getIndosNo() {
		return indosNo;
	}

	public void setIndosNo(String indosNo) {
		this.indosNo = indosNo;
	}

	public String getCdcNo() {
		return cdcNo;
	}

	public void setCdcNo(String cdcNo) {
		this.cdcNo = cdcNo;
	}

	public String getCdcIssueDate() {
		return cdcIssueDate;
	}

	public void setCdcIssueDate(String cdcIssueDate) {
		this.cdcIssueDate = cdcIssueDate;
	}

	public String getCocNo() {
		return cocNo;
	}

	public void setCocNo(String cocNo) {
		this.cocNo = cocNo;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getCocIssuingAuthority() {
		return cocIssuingAuthority;
	}

	public void setCocIssuingAuthority(String cocIssuingAuthority) {
		this.cocIssuingAuthority = cocIssuingAuthority;
	}

	public String getCocIssuingDate() {
		return cocIssuingDate;
	}

	public void setCocIssuingDate(String cocIssuingDate) {
		this.cocIssuingDate = cocIssuingDate;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportPlaceOfIssue() {
		return passportPlaceOfIssue;
	}

	public void setPassportPlaceOfIssue(String passportPlaceOfIssue) {
		this.passportPlaceOfIssue = passportPlaceOfIssue;
	}

	public String getPassportIssueDate() {
		return passportIssueDate;
	}

	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
	
	
	
	
	

}
