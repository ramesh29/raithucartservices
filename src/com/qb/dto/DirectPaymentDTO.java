package com.qb.dto;

import java.io.Serializable;

public class DirectPaymentDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private TransactionDTO transactionDTO = null;
	
	private BookingDTO bookingDTO = null;

	public TransactionDTO getTransactionDTO() {
		return transactionDTO;
	}

	public void setTransactionDTO(TransactionDTO transactionDTO) {
		this.transactionDTO = transactionDTO;
	}

	public BookingDTO getBookingDTO() {
		return bookingDTO;
	}

	public void setBookingDTO(BookingDTO bookingDTO) {
		this.bookingDTO = bookingDTO;
	}

}
