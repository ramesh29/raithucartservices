package com.qb.dto;

import java.io.Serializable;

public class AtomPaymentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int login = 0;
	
	private String password = null;
	
	private String 	ttype = null;
	
	private String prodId = null;
	
	private String amount = null;
	
	private String currency = null;
	
	private String transactionServiceCharge = null;
	
	private String clientCode = null;
	
	private int transactionId = 0;
	
	private String date = null;
	
	private String trackingDate = null;
	
	private String requestHashKey = null;
	
	private String responseHashKey = null;
	
	private int custAcc = 0;
	
	private String returnUrl = null;
	
	private String udf1 = null;
	
	private String udf2 = null;
	
	private String udf3 = null;
	
	private String udf4 = null;
	
	private String contentXML = null;
	
	private String tempTxnId = null;
	
	private String token = null;
	
	private int txnStage = 0;
	
	//Atom Response attributes after transaction
	
	private String mmp_txn = null;
	
	private String mer_txn = null;
	
	private String respAmount = null;
	
	private String surcharge = null;
	
	private String respTxndate = null;
	
	private String bankTransactionId = null;
	
	private String transactionStatus = null;
	
	private String bankName = null;
	
	private String discriminator = null;
	
	private String cardNumber = null;
	
	private String transactionMsg = null;
	
	//Confirmation Request Attributes
	
	private String verified = null;
	
	private String BID = null;
	
	private String requestParameter = null;
	
	private String paymentGatewayURL = null;

	public int getLogin() {
		return login;
	}

	public void setLogin(int login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTtype() {
		return ttype;
	}

	public void setTtype(String ttype) {
		this.ttype = ttype;
	}

	public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTransactionServiceCharge() {
		return transactionServiceCharge;
	}

	public void setTransactionServiceCharge(String transactionServiceCharge) {
		this.transactionServiceCharge = transactionServiceCharge;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getCustAcc() {
		return custAcc;
	}

	public void setCustAcc(int custAcc) {
		this.custAcc = custAcc;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getTempTxnId() {
		return tempTxnId;
	}

	public void setTempTxnId(String tempTxnId) {
		this.tempTxnId = tempTxnId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getTxnStage() {
		return txnStage;
	}

	public void setTxnStage(int txnStage) {
		this.txnStage = txnStage;
	}

	public String getMmp_txn() {
		return mmp_txn;
	}

	public void setMmp_txn(String mmp_txn) {
		this.mmp_txn = mmp_txn;
	}

	public String getMer_txn() {
		return mer_txn;
	}

	public void setMer_txn(String mer_txn) {
		this.mer_txn = mer_txn;
	}

	public String getRespAmount() {
		return respAmount;
	}

	public void setRespAmount(String respAmount) {
		this.respAmount = respAmount;
	}

	public String getSurcharge() {
		return surcharge;
	}

	public void setSurcharge(String surcharge) {
		this.surcharge = surcharge;
	}

	public String getRespTxndate() {
		return respTxndate;
	}

	public void setRespTxndate(String respTxndate) {
		this.respTxndate = respTxndate;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getDiscriminator() {
		return discriminator;
	}

	public void setDiscriminator(String discriminator) {
		this.discriminator = discriminator;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getTransactionMsg() {
		return transactionMsg;
	}

	public void setTransactionMsg(String transactionMsg) {
		this.transactionMsg = transactionMsg;
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getBID() {
		return BID;
	}

	public void setBID(String bID) {
		BID = bID;
	}

	public String getContentXML() {
		return contentXML;
	}

	public void setContentXML(String contentXML) {
		this.contentXML = contentXML;
	}

	public String getTrackingDate() {
		return trackingDate;
	}

	public void setTrackingDate(String trackingDate) {
		this.trackingDate = trackingDate;
	}

	
	public String getRequestHashKey() {
		return requestHashKey;
	}

	public void setRequestHashKey(String requestHashKey) {
		this.requestHashKey = requestHashKey;
	}

	public String getResponseHashKey() {
		return responseHashKey;
	}

	public void setResponseHashKey(String responseHashKey) {
		this.responseHashKey = responseHashKey;
	}

	
	public String getRequestParameter() {
		return requestParameter;
	}

	public void setRequestParameter(String requestParameter) {
		this.requestParameter = requestParameter;
	}

	public String getPaymentGatewayURL() {
		return paymentGatewayURL;
	}

	public void setPaymentGatewayURL(String paymentGatewayURL) {
		this.paymentGatewayURL = paymentGatewayURL;
	}
	
	
	
	
}
