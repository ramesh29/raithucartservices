package com.qb.dto;

import java.io.Serializable;
import java.util.List;

public class SearchDropListsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CourseCategoryDTO> courseCategoryDTO = null;
	
	private List<String> cities = null;
	
	private List<OrgDTO> institutes = null;

	public List<CourseCategoryDTO> getCourseCategoryDTO() {
		return courseCategoryDTO;
	}

	public void setCourseCategoryDTO(List<CourseCategoryDTO> courseCategoryDTO) {
		this.courseCategoryDTO = courseCategoryDTO;
	}

	public List<String> getCities() {
		return cities;
	}

	public void setCities(List<String> cities) {
		this.cities = cities;
	}

	public  List<OrgDTO>  getInstitutes() {
		return institutes;
	}

	public void setInstitutes( List<OrgDTO>  institutes) {
		this.institutes = institutes;
	}

}
