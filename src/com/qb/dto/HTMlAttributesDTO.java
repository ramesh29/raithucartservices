package com.qb.dto;

import java.util.HashMap;

public class HTMlAttributesDTO {
	
	private String htmlFileName = null;
	
	private String qr = null;
	
	private HashMap<String, String> replaceValues = null;

	public String getHtmlFileName() {
		return htmlFileName;
	}

	public void setHtmlFileName(String htmlFileName) {
		this.htmlFileName = htmlFileName;
	}

	public String getQr() {
		return qr;
	}

	public void setQr(String qr) {
		this.qr = qr;
	}

	public HashMap<String, String> getReplaceValues() {
		return replaceValues;
	}

	public void setReplaceValues(HashMap<String, String> replaceValues) {
		this.replaceValues = replaceValues;
	}

}
