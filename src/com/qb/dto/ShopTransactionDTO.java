package com.qb.dto;

import java.io.Serializable;

public class ShopTransactionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int transactionId;
	
	private String transactionType;
	
	private double transactionQty;
	
	private String transactionDate;
	
	private int itemId;
	
	private int userId;
	
	private double existingQty;
	
	private double newQty;
	
	private Double itemPurchaseCost;
	
	private Double itemSoldCost;
	
	private Double newPurchaseCost;
	
	private Double newSoldCost;
	
	private Double totalAmount;
	
	private String uom;

	private String itemName;
	
	private String itemDesc;
	
	private String itemUOM;
	
	private int customerId;
	
	private int customerTransactionId;
	
	private String status;
	
	private Double paidAmount;

	private Double oldCredit;
	
	private Double discountAmount;
	
	private Double transactionAmount;
	
	private String gstValue;
	
	private String upcCode;
	
	private String customerName;
	
	private int id;
	
	private double previousQuantity;
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUpcCode() {
		return upcCode;
	}

	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	
	public Double getOldCredit() {
		return oldCredit;
	}

	public void setOldCredit(Double oldCredit) {
		this.oldCredit = oldCredit;
	}

	private String imageName;
	
	private Double discount;
	
	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public double getTransactionQty() {
		return transactionQty;
	}

	public void setTransactionQty(double transactionQty) {
		this.transactionQty = transactionQty;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public double getExistingQty() {
		return existingQty;
	}

	public void setExistingQty(double existingQty) {
		this.existingQty = existingQty;
	}

	public double getNewQty() {
		return newQty;
	}

	public void setNewQty(double newQty) {
		this.newQty = newQty;
	}

	public Double getItemPurchaseCost() {
		return itemPurchaseCost;
	}

	public void setItemPurchaseCost(Double itemPurchaseCost) {
		this.itemPurchaseCost = itemPurchaseCost;
	}

	public Double getItemSoldCost() {
		return itemSoldCost;
	}

	public void setItemSoldCost(Double itemSoldCost) {
		this.itemSoldCost = itemSoldCost;
	}

	public Double getNewPurchaseCost() {
		return newPurchaseCost;
	}

	public void setNewPurchaseCost(Double newPurchaseCost) {
		this.newPurchaseCost = newPurchaseCost;
	}

	public Double getNewSoldCost() {
		return newSoldCost;
	}

	public void setNewSoldCost(Double newSoldCost) {
		this.newSoldCost = newSoldCost;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemUOM() {
		return itemUOM;
	}

	public void setItemUOM(String itemUOM) {
		this.itemUOM = itemUOM;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getCustomerTransactionId() {
		return customerTransactionId;
	}

	public void setCustomerTransactionId(int customerTransactionId) {
		this.customerTransactionId = customerTransactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getGstValue() {
		return gstValue;
	}

	public void setGstValue(String gstValue) {
		this.gstValue = gstValue;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPreviousQuantity() {
		return previousQuantity;
	}

	public void setPreviousQuantity(double previousQuantity) {
		this.previousQuantity = previousQuantity;
	}

	

}
