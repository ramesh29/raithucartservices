package com.qb.dto;

import java.io.Serializable;

public class ShopItemDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int itemId;
	
	private String itemName;
	
	private String itemDesc;
	
	private String itemUOM;
	
	private String imageName;
	
	private int userId;
	
	private Double totalQuantity;
	
	private Double totalAmount;
	
	private Double itemPurchaseCost;
	
	private Double itemSoldCost;

	private int newQuantity;
	
	private Double newPurchaseCost;
	
	private Double newSoldCost;
	
	private int lowQuantityValue;
	
	private String gstValue;
	
	private String upcCode;
	
	private int id;
	
	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemUOM() {
		return itemUOM;
	}

	public void setItemUOM(String itemUOM) {
		this.itemUOM = itemUOM;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getItemPurchaseCost() {
		return itemPurchaseCost;
	}

	public void setItemPurchaseCost(Double itemPurchaseCost) {
		this.itemPurchaseCost = itemPurchaseCost;
	}

	public Double getItemSoldCost() {
		return itemSoldCost;
	}

	public void setItemSoldCost(Double itemSoldCost) {
		this.itemSoldCost = itemSoldCost;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getNewQuantity() {
		return newQuantity;
	}

	public void setNewQuantity(int newQuantity) {
		this.newQuantity = newQuantity;
	}

	public Double getNewPurchaseCost() {
		return newPurchaseCost;
	}

	public void setNewPurchaseCost(Double newPurchaseCost) {
		this.newPurchaseCost = newPurchaseCost;
	}

	public Double getNewSoldCost() {
		return newSoldCost;
	}

	public void setNewSoldCost(Double newSoldCost) {
		this.newSoldCost = newSoldCost;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public int getLowQuantityValue() {
		return lowQuantityValue;
	}

	public void setLowQuantityValue(int lowQuantityValue) {
		this.lowQuantityValue = lowQuantityValue;
	}

	public String getGstValue() {
		return gstValue;
	}

	public void setGstValue(String gstValue) {
		this.gstValue = gstValue;
	}

	public String getUpcCode() {
		return upcCode;
	}

	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	
}
