package com.qb.dto;

import java.io.Serializable;
import java.util.List;

public class CourseCategoryDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int couseCategoryId = 0;
	
	private String couseCategoryName = null;
	
	private List<CourseSubCategoryDTO> couseSubCategoryId = null;

	public int getCouseCategoryId() {
		return couseCategoryId;
	}
	
	public void setCouseCategoryId(int couseCategoryId) {
		this.couseCategoryId = couseCategoryId;
	}

	public String getCouseCategoryName() {
		return couseCategoryName;
	}

	public void setCouseCategoryName(String couseCategoryName) {
		this.couseCategoryName = couseCategoryName;
	}

	public List<CourseSubCategoryDTO> getCouseSubCategoryId() {
		return couseSubCategoryId;
	}

	public void setCouseSubCategoryId(List<CourseSubCategoryDTO> couseSubCategoryId) {
		this.couseSubCategoryId = couseSubCategoryId;
	}

	

}
