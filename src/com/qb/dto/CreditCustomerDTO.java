package com.qb.dto;

import java.io.Serializable;

public class CreditCustomerDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int cc_id;
	
	private String cc_mobile;
	
	private String cc_name;
	
	private double cc_paid_amount;
	
	private double cc_credit_amount;
	
	private double cc_total_amount;

	private double cc_discount;
	
	private String status;
	
	private String creditState;
	
	private int userId;
	
	private double cc_prev_credit_amount;
	
	private int id;
	
	private int customerTransactionId;

	public int getCustomerTransactionId() {
		return customerTransactionId;
	}

	public void setCustomerTransactionId(int customerTransactionId) {
		this.customerTransactionId = customerTransactionId;
	}

	public int getCc_id() {
		return cc_id;
	}

	public String getCreditState() {
		return creditState;
	}

	public void setCreditState(String creditState) {
		this.creditState = creditState;
	}

	public void setCc_id(int cc_id) {
		this.cc_id = cc_id;
	}

	public String getCc_mobile() {
		return cc_mobile;
	}

	public void setCc_mobile(String cc_mobile) {
		this.cc_mobile = cc_mobile;
	}

	public String getCc_name() {
		return cc_name;
	}

	public void setCc_name(String cc_name) {
		this.cc_name = cc_name;
	}

	public double getCc_paid_amount() {
		return cc_paid_amount;
	}

	public void setCc_paid_amount(double cc_paid_amount) {
		this.cc_paid_amount = cc_paid_amount;
	}

	public double getCc_credit_amount() {
		return cc_credit_amount;
	}

	public void setCc_credit_amount(double cc_credit_amount) {
		this.cc_credit_amount = cc_credit_amount;
	}

	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public double getCc_total_amount() {
		return cc_total_amount;
	}

	public void setCc_total_amount(double cc_total_amount) {
		this.cc_total_amount = cc_total_amount;
	}

	public double getCc_discount() {
		return cc_discount;
	}

	public void setCc_discount(double cc_discount) {
		this.cc_discount = cc_discount;
	}

	public double getCc_prev_credit_amount() {
		return cc_prev_credit_amount;
	}

	public void setCc_prev_credit_amount(double cc_prev_credit_amount) {
		this.cc_prev_credit_amount = cc_prev_credit_amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
