package com.qb.dto;

import java.io.Serializable;
import java.util.List;

public class SearchParametersDTO implements Serializable{
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	private int categoryId = 0;
	private int subCategoryId=0;
	private List<Integer> courseId = null;
	private List<Integer> instituteId = null;
	private List<String> cities=null;
	private List<Integer> schdules=null;
	private String startDate;
	private String endDate;
	private int orgId =0;
	private int scheduleId = 0;
	private List<String> status=null;
	private List<String> schdulestatus=null;
	private int userId = 0;
	private int transactionId = 0;
	private String transactionStatus = null;
	private String availability = null;
	private List<PaymentTransactionDTO> pto = null;
	private List<String> bookingSource = null;
	private int bookingId = 0;
	private List<Integer> courseCode = null;
	
	private List<String> transactionType=null;
	private List<Integer> itemIds = null;
	
	
	private String phone = null;
	
	private String plan = null;
	
	private Double amount = 0.0;
	
	private String mobile = null;
	
	private String paymentFor = null;
	
	private int customerId;
	
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public List<Integer> getCourseId() {
		return courseId;
	}
	public void setCourseId(List<Integer> courseId) {
		this.courseId = courseId;
	}
	public List<Integer> getInstituteId() {
		return instituteId;
	}
	public void setInstituteId(List<Integer> instituteId) {
		this.instituteId = instituteId;
	}
	public List<String> getCities() {
		return cities;
	}
	public void setCities(List<String> cities) {
		this.cities = cities;
	}
	public List<Integer> getSchdules() {
		return schdules;
	}
	public void setSchdules(List<Integer> schdules) {
		this.schdules = schdules;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getOrgId() {
		return orgId;
	}
	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}
	public int getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}
	public List<String> getStatus() {
		return status;
	}
	public void setStatus(List<String> status) {
		this.status = status;
	}
	public List<String> getSchdulestatus() {
		return schdulestatus;
	}
	public void setSchdulestatus(List<String> schdulestatus) {
		this.schdulestatus = schdulestatus;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public List<PaymentTransactionDTO> getPto() {
		return pto;
	}
	public void setPto(List<PaymentTransactionDTO> pto) {
		this.pto = pto;
	}
	public List<String> getBookingSource() {
		return bookingSource;
	}
	public void setBookingSource(List<String> bookingSource) {
		this.bookingSource = bookingSource;
	}
	public int getBookingId() {
		return bookingId;
	}
	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}
	public List<Integer> getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(List<Integer> courseCode) {
		this.courseCode = courseCode;
	}
	public List<String> getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(List<String> transactionType) {
		this.transactionType = transactionType;
	}
	public List<Integer> getItemIds() {
		return itemIds;
	}
	public void setItemIds(List<Integer> itemIds) {
		this.itemIds = itemIds;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPaymentFor() {
		return paymentFor;
	}
	public void setPaymentFor(String paymentFor) {
		this.paymentFor = paymentFor;
	}
}
