package com.qb.dto;

import java.io.Serializable;

public class DashboardDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String courseName = null;
	
	private String username = null;
	
	private String trasactionDate = null;
	
	private String startDate = null;
	
	private String endDate = null;
	
	private String startTime = null;
	
	private String endTime = null;
	
	private String price = null;
	

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTrasactionDate() {
		return trasactionDate;
	}

	public void setTrasactionDate(String trasactionDate) {
		this.trasactionDate = trasactionDate;
	}

}
