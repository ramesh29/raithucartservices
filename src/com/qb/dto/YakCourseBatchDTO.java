package com.qb.dto;

import java.io.Serializable;

public class YakCourseBatchDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String Address = null;
	
	public String BatchEndDate = null;
	
	public int BatchId = 0;
	
	public int BatchNo = 0;
	
	public String BatchStartDate = null;
	
	public int courseId = 0;
	
	public String InstituteName = null;
	
	public int Seats = 0;
	
	public CourseDTO courseDTO = null;

	
}
