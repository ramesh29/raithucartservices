package com.qb.dto;

import java.io.Serializable;

public class CourseBookingDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String college = null;
	
	private String categoryName = null;
	
	private String subCatName = null;
	
	private String courseName = null;
	
	private int scheduleId = 0;
	
	private String startDate = null;
	
	private String endDate = null;
	
	private String seats = null;
	
	private String price = null;
	
	private String timings = null;
	
	private String available = null;
	
	private String startTime = null;
	
	private String endTime = null;
	
	private String totalSeats = null;
	
	private String bookedSeats = null;
	
	private String display = null;
	
	private int orgId = 0;
	
	private int transactionId = 0;
	
	private String atomUsername = null;
	
	private String atomPassword = null;
	
	private String atomProdId = null;
	
	private String atomProdName = null;
	
	private int apiCourseId = 0;
	
	private int apiBatchId = 0;
	
	private String paymentAgreementType = null;
	
	private String paymentAgreementValue = null;
	
	private int courseCode = 0;
	
	private String requestHashKey = null;
	
	private String responseHashKey = null;
	
	public String getRequestHashKey() {
		return requestHashKey;
	}

	public void setRequestHashKey(String requestHashKey) {
		this.requestHashKey = requestHashKey;
	}

	public String getResponseHashKey() {
		return responseHashKey;
	}

	public void setResponseHashKey(String responseHashKey) {
		this.responseHashKey = responseHashKey;
	}

	public String getPaymentAgreementType() {
		return paymentAgreementType;
	}

	public void setPaymentAgreementType(String paymentAgreementType) {
		this.paymentAgreementType = paymentAgreementType;
	}

	public String getPaymentAgreementValue() {
		return paymentAgreementValue;
	}

	public void setPaymentAgreementValue(String paymentAgreementValue) {
		this.paymentAgreementValue = paymentAgreementValue;
	}

	public int getApiCourseId() {
		return apiCourseId;
	}

	public void setApiCourseId(int apiCourseId) {
		this.apiCourseId = apiCourseId;
	}

	public int getApiBatchId() {
		return apiBatchId;
	}

	public void setApiBatchId(int apiBatchId) {
		this.apiBatchId = apiBatchId;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSubCatName() {
		return subCatName;
	}

	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTimings() {
		return timings;
	}

	public void setTimings(String timings) {
		this.timings = timings;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(String totalSeats) {
		this.totalSeats = totalSeats;
	}

	public String getBookedSeats() {
		return bookedSeats;
	}

	public void setBookedSeats(String bookedSeats) {
		this.bookedSeats = bookedSeats;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public String getAtomProdId() {
		return atomProdId;
	}

	public void setAtomProdId(String atomProdId) {
		this.atomProdId = atomProdId;
	}

	public String getAtomProdName() {
		return atomProdName;
	}

	public void setAtomProdName(String atomProdName) {
		this.atomProdName = atomProdName;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getAtomUsername() {
		return atomUsername;
	}

	public void setAtomUsername(String atomUsername) {
		this.atomUsername = atomUsername;
	}

	public String getAtomPassword() {
		return atomPassword;
	}

	public void setAtomPassword(String atomPassword) {
		this.atomPassword = atomPassword;
	}

	public int getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(int courseCode) {
		this.courseCode = courseCode;
	}

}
