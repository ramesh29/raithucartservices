package com.qb.dto;

import java.io.Serializable;

public class OrgCourseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int orgCourseId=0;
	
	private String courseName = null;
	
	private int orgId = 0;
	
	private int courseId = 0;
	
	private int duration = 0;
	
	private int seats = 0;
	
	private String paymentAgreementType = null;
	
	private String paymentAgreementValue = null;
	
	private String campus = null;
	
	private String repeat = null;
	
	private String repeatEvery = null;
	
	private String repeatDetail = null;
	
	private String repeatOn = null;
	
	private String price = null;

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public String getRepeatEvery() {
		return repeatEvery;
	}

	public void setRepeatEvery(String repeatEvery) {
		this.repeatEvery = repeatEvery;
	}

	public String getRepeatDetail() {
		return repeatDetail;
	}

	public void setRepeatDetail(String repeatDetail) {
		this.repeatDetail = repeatDetail;
	}

	public String getRepeatOn() {
		return repeatOn;
	}

	public void setRepeatOn(String repeatOn) {
		this.repeatOn = repeatOn;
	}

	public int getOrgCourseId() {
		return orgCourseId;
	}

	public void setOrgCourseId(int orgCourseId) {
		this.orgCourseId = orgCourseId;
	}

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}



	public String getPaymentAgreementType() {
		return paymentAgreementType;
	}

	public void setPaymentAgreementType(String paymentAgreementType) {
		this.paymentAgreementType = paymentAgreementType;
	}

	public String getPaymentAgreementValue() {
		return paymentAgreementValue;
	}

	public void setPaymentAgreementValue(String paymentAgreementValue) {
		this.paymentAgreementValue = paymentAgreementValue;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	
}
