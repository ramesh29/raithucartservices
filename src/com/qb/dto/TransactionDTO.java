package com.qb.dto;

import java.io.Serializable;

public class TransactionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int transactionId = 0;
	
	private int userId = 0;
	
	private String mobileNumber = null;
	
	private Double totalPrice = null;
	
	private Double serviceTax = null;
	
	private Double internetHandlingFees = null;
	
	private Double paymentGateway = null;
	
	private String transactionStatus  = null;
	
	private String transactionDate = null;
	
	private String transactionSource = null;
	
	private String plan;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}



	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(Double serviceTax) {
		this.serviceTax = serviceTax;
	}

	public Double getInternetHandlingFees() {
		return internetHandlingFees;
	}

	public void setInternetHandlingFees(Double internetHandlingFees) {
		this.internetHandlingFees = internetHandlingFees;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Double getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(Double paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String string) {
		this.mobileNumber = string;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	
}
