package com.qb.dto;

import java.io.Serializable;
import java.util.List;

public class PaymentTransactionDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int transactionId = 0;
	
	private List<Integer> schedules = null;
	
	private int userId = 0;
	
	private String action= null;
	
	private String redirectURL = null;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public List<Integer> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<Integer> schedules) {
		this.schedules = schedules;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

}
