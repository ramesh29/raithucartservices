package com.qb.dto;

import java.io.Serializable;
import java.util.List;


public class YakCourseDTO implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String ErrorMessage = null;
	
	public String Status = null;
	
	public List<CourseDTO> Courses = null;

	public List<YakCourseBatchDTO> CourseBatches = null;
	
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}


	public List<CourseDTO> getCourses() {
		return Courses;
	}

	public void setCourses(List<CourseDTO> courses) {
		Courses = courses;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

}
