package com.qb.dto;

import java.io.Serializable;

public class ShopUserDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int userId;
	
	private Double planPrice;
	
	private String shopName;
	
	private String mobileNumber;
	
	private String planName;
	
	private String password;
	
	private String currentStockValue;
	
	private String currentSoldValue;
	
	private String profit;
	
	private String address;
	
	private String city;
	
	private String state;
	
	private String zip;
	
	private String country;
	
	private boolean success = false;
	
	private String messege = null;
	
	private String apiKey = null;
	
	private String otp = null;

	private String plan = null;
	
	private String subscriptionFor = null;
	
	private String discount = null;
	
	private String shopLogo = null;
	
	private String showDetails = null;
	
	private String gst = null;
	
	private String uuid = null;
	
	private String totalSoldValue;
	
	private String totalCreditValue;
	
	private String todaySoldValue;
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	private String imageName = null;
	
	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getSubscriptionFor() {
		return subscriptionFor;
	}

	public void setSubscriptionFor(String subscriptionFor) {
		this.subscriptionFor = subscriptionFor;
	}

	public String getShopLogo() {
		return shopLogo;
	}

	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}

	public String getShowDetails() {
		return showDetails;
	}

	public void setShowDetails(String showDetails) {
		this.showDetails = showDetails;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
			
	
	public Double getPlanPrice() {
		return planPrice;
	}

	public void setPlanPrice(Double planPrice) {
		this.planPrice = planPrice;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	
	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessege() {
		return messege;
	}

	public void setMessege(String messege) {
		this.messege = messege;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getCurrentStockValue() {
		return currentStockValue;
	}

	public void setCurrentStockValue(String currentStockValue) {
		this.currentStockValue = currentStockValue;
	}

	public String getCurrentSoldValue() {
		return currentSoldValue;
	}

	public void setCurrentSoldValue(String currentSoldValue) {
		this.currentSoldValue = currentSoldValue;
	}

	public String getProfit() {
		return profit;
	}

	public void setProfit(String profit) {
		this.profit = profit;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getTotalSoldValue() {
		return totalSoldValue;
	}

	public void setTotalSoldValue(String totalSoldValue) {
		this.totalSoldValue = totalSoldValue;
	}

	public String getTotalCreditValue() {
		return totalCreditValue;
	}

	public void setTotalCreditValue(String totalCreditValue) {
		this.totalCreditValue = totalCreditValue;
	}

	public String getTodaySoldValue() {
		return todaySoldValue;
	}

	public void setTodaySoldValue(String todaySoldValue) {
		this.todaySoldValue = todaySoldValue;
	}
	
}
