package com.qb.dto;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseDTO implements Serializable{
	
	/**
	 * 
	 */
	
	public CourseDTO()
	{
		
	}
	
	private static final long serialVersionUID = 1L;

	private int couseCategoryId = 0;
	
	private int couseSubCategoryId = 0;
	
	private int courseId = 0;
	
	private String courseName = null;
	
	private String courseCode = null;
	
	private String courseFee = null;
	
	private String duration = null;
	
	private List<YakCourseBatchDTO> courseBatchDTO = null;
	
	

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseFee() {
		return courseFee;
	}

	public void setCourseFee(String courseFee) {
		this.courseFee = courseFee;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public int getCouseCategoryId() {
		return couseCategoryId;
	}

	public void setCouseCategoryId(int couseCategoryId) {
		this.couseCategoryId = couseCategoryId;
	}

	public int getCouseSubCategoryId() {
		return couseSubCategoryId;
	}

	public void setCouseSubCategoryId(int couseSubCategoryId) {
		this.couseSubCategoryId = couseSubCategoryId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public List<YakCourseBatchDTO> getCourseBatchDTO() {
		return courseBatchDTO;
	}

	public void setCourseBatchDTO(List<YakCourseBatchDTO> courseBatchDTO) {
		this.courseBatchDTO = courseBatchDTO;
	}

}
