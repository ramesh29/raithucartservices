package com.qb.dto;

import java.io.Serializable;
import java.util.List;

public class DashboardFinalDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<DashboardDTO> directBookings = null;
	
	private List<DashboardDTO> onlineBookings = null;
	
	private List<DashboardDTO> upcoming = null;
	
	private List<DashboardDTO> ongoing = null;

	public List<DashboardDTO> getDirectBookings() {
		return directBookings;
	}

	public void setDirectBookings(List<DashboardDTO> directBookings) {
		this.directBookings = directBookings;
	}

	public List<DashboardDTO> getOnlineBookings() {
		return onlineBookings;
	}

	public void setOnlineBookings(List<DashboardDTO> onlineBookings) {
		this.onlineBookings = onlineBookings;
	}

	public List<DashboardDTO> getUpcoming() {
		return upcoming;
	}

	public void setUpcoming(List<DashboardDTO> upcoming) {
		this.upcoming = upcoming;
	}

	public List<DashboardDTO> getOngoing() {
		return ongoing;
	}

	public void setOngoing(List<DashboardDTO> ongoing) {
		this.ongoing = ongoing;
	}

	
	
	

}
